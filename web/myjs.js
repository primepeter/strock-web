 firebase.initializeApp({
      apiKey: "AIzaSyAYyPkQZawkdTr2OFl05_aai2iKiQE44Es",
  authDomain: "quickhooks-c4691.firebaseapp.com",
  databaseURL: "https://quickhooks-c4691.firebaseio.com",
  projectId: "quickhooks-c4691",
  storageBucket: "quickhooks-c4691.appspot.com",
  messagingSenderId: "662219331974",
  appId: "1:662219331974:web:ab81951a8892786dd77407",
  measurementId: "G-40M5SNNKQQ"
});
firebase.initializeApp(firebaseConfig);
firebase.analytics();

const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function (payload) {
    const promiseChain = clients
        .matchAll({
            type: "window",
            includeUncontrolled: true
        })
        .then(windowClients => {
            for (let i = 0; i < windowClients.length; i++) {
                const windowClient = windowClients[i];
                windowClient.postMessage(payload);
            }
        })
        .then(() => {
            return registration.showNotification("New Message");
        });
    return promiseChain;
});
self.addEventListener('notificationclick', function (event) {
    console.log('notification received: ', event)
});