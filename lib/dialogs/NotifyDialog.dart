import 'dart:ui';

import 'package:Strokes/assets.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../AppEngine.dart';
import '../app_config.dart';

class NotifyDialog extends StatefulWidget {
  @override
  _NotifyDialogState createState() => _NotifyDialogState();
}

class _NotifyDialogState extends State<NotifyDialog> {
  TextEditingController messageController = TextEditingController();
  double rating = 0.0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: transparent,
        body: Stack(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
                  child: Container(
                    color: black.withOpacity(.7),
                  )),
            ),
            page()
          ],
        ));
  }

  page() {
    return Center(
      child: Container(
        margin: EdgeInsets.fromLTRB(20, 40, 20, 20),
        width: kIsWeb ? 300 : null,
        child: Center(
          child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15))),
              color: white,
              clipBehavior: Clip.antiAlias,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  /*Container(
                   width: double.infinity,
                   color: white,
                   padding: EdgeInsets.fromLTRB(20,10,20,10),
                   child:Center(child: Column(
                     mainAxisSize: MainAxisSize.min,
                     children: [
                       Text("Make Offer",style: textStyle(true, 12, white),),

                     ],
                   )),
                 ),*/
                  AnimatedContainer(
                    duration: Duration(milliseconds: 500),
                    width: double.infinity,
                    height: errorText.isEmpty ? 0 : 40,
                    color: red0,
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: Center(
                        child: Text(
                      errorText,
                      style: textStyle(true, 16, white),
                    )),
                  ),
                  Flexible(
                    fit: FlexFit.loose,
                    child: Container(
                      color: white,
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(15, 20, 15, 10),
                          child: textFieldBox(messageController, "Message",
                              "Write a message", (v) => null,
                              maxLines: 4),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 50,
                    margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                    child: FlatButton(
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0)),
                        color: AppConfig.appColor,
                        onPressed: () {
                          String commentText = messageController.text;

                          if (commentText.isEmpty) {
                            showError("Add Message");
                            return;
                          }
                          Navigator.pop(context, commentText);
                        },
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              "Send",
                              style: textStyle(true, 16, white),
                            ),
                            addSpaceWidth(10),
                            Icon(
                              Icons.send,
                              color: white,
                              size: 15,
                            )
                          ],
                        )),
                  ),
                ],
              )),
        ),
      ),
    );
  }

  String errorText = "";
  showError(String text) {
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 1), () {
      errorText = "";
      setState(() {});
    });
  }

  textFieldBox(TextEditingController controller, String title, String hint,
      setState(String v),
      {focusNode, int maxLength, int maxLines, bool number = false}) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: textStyle(false, 14, black.withOpacity(.7)),
          ),
          addSpace(5),
          TextFormField(
            focusNode: focusNode,
            maxLength: maxLength,
            maxLines: maxLines,
            //maxLengthEnforced: false,
            controller: controller,
            cursorColor: black,
            style: textStyle(false, 16, black),
            decoration: InputDecoration(
                fillColor: black.withOpacity(.05),
                filled: true,
                labelText: hint,
                labelStyle: textStyle(false, 16, black.withOpacity(.4)),
                counter: Container(),
                border: InputBorder.none),
            onChanged: setState,
            keyboardType: number ? TextInputType.number : null,
          ),
        ],
      ),
    );
  }
}
