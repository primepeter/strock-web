import 'package:Strokes/auth/AddSupport.dart';
import 'package:Strokes/dialogs/inputDialog.dart';
import 'package:Strokes/dialogs/listDialog.dart';
import 'package:flutter/material.dart';

import '../AppEngine.dart';
import '../NewUpdate.dart';
import '../assets.dart';

class WebSettings extends StatefulWidget {
  @override
  _WebSettingsState createState() => _WebSettingsState();
}

class _WebSettingsState extends State<WebSettings> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: page(),
    );
  }

  page() {
    return Center(
      child: Container(
        //padding: EdgeInsets.only(left: 100, right: 100),
        child: Card(
          elevation: 5.0,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          child: ListView(
            padding: EdgeInsets.all(0),
            children: [
              ListTile(
                onTap: () {
                  pushAndResult(
                      context,
                      AddSupport(
                        theUser: userModel,
                      ));
                },
                title: Text("Change My Password"),
//            subtitle: Text(""),
                trailing: Icon(Icons.navigate_next),
              ),
              if (isAdmin) ...[
                ListTile(
                  onTap: () {
                    pushAndResult(
                        context,
                        inputDialog(
                          "Stripe Secret Key",
                          hint: "Enter Stripe Secret Key",
                          message: appSettingsModel.getString(STRIPE_SEC_KEY),
                        ), result: (_) {
                      if (null == _) return;
                      appSettingsModel
                        ..put(STRIPE_PUB_KEY, _)
                        ..updateItems();
                      setState(() {});
                    }, depend: false);
                  },
                  title: Text("Stripe Secret Key"),
                  subtitle: Text("Set and Update Stripe Secret key Here"),
                  trailing: Icon(Icons.navigate_next),
                ),
                ListTile(
                  onTap: () {
                    pushAndResult(
                        context,
                        inputDialog(
                          "Miles Limit Distance",
                          hint: "Enter Miles Limit Distance",
                          message: appSettingsModel
                              .getDouble(MILES_LIMIT)
                              .toString(),
                          inputType: TextInputType.number,
                        ),
                        depend: false, result: (_) {
                      if (null == _) return;
                      appSettingsModel
                        ..put(MILES_LIMIT, double.parse(_))
                        ..updateItems();
                      setState(() {});
                    });
                  },
                  title: Text(
                      "Miles Limit Distance -- (${appSettingsModel.getDouble(MILES_LIMIT)})"),
                  subtitle: Text("Set Miles Limit Distance here"),
                  trailing: Icon(Icons.navigate_next),
                ),
                ListTile(
                  onTap: () {
                    pushAndResult(
                        context,
                        inputDialog(
                          "Meetme Likes Max",
                          hint: "Enter Meetme Likes Max",
                          message: appSettingsModel
                              .getDouble(COUNTS_LIKE)
                              .toString(),
                          inputType: TextInputType.number,
                        ),
                        depend: false, result: (_) {
                      if (null == _) return;
                      appSettingsModel
                        ..put(COUNTS_LIKE, double.parse(_))
                        ..updateItems();
                      setState(() {});
                    });
                  },
                  title: Text(
                      "MeetMe Likes Max -- (${appSettingsModel.getDouble(COUNTS_LIKE)})"),
                  subtitle: Text("Set MeetMe Likes Max here"),
                  trailing: Icon(Icons.navigate_next),
                ),

                ListTile(
                  onTap: () {
                    pushAndResult(
                        context,
                        inputDialog(
                          "Meetme Rewind Max",
                          hint: "Enter Meetme Rewind Max",
                          message: appSettingsModel
                              .getDouble(COUNTS_REWIND)
                              .toString(),
                          inputType: TextInputType.number,
                        ),
                        depend: false, result: (_) {
                      if (null == _) return;
                      appSettingsModel
                        ..put(COUNTS_REWIND, double.parse(_))
                        ..updateItems();
                      setState(() {});
                    });
                  },
                  title: Text(
                      "MeetMe Rewind Max -- (${appSettingsModel.getDouble(COUNTS_REWIND)})"),
                  subtitle: Text("Set MeetMe Rewind Max here"),
                  trailing: Icon(Icons.navigate_next),
                ),

                ListTile(
                  onTap: () {
                    pushAndResult(
                        context,
                        inputDialog(
                          "Meetme Superlike Max",
                          hint: "Enter Meetme Superlike Max",
                          message: appSettingsModel
                              .getDouble(COUNTS_SUPER_LIKE)
                              .toString(),
                          inputType: TextInputType.number,
                        ),
                        depend: false, result: (_) {
                      if (null == _) return;
                      appSettingsModel
                        ..put(COUNTS_SUPER_LIKE, double.parse(_))
                        ..updateItems();
                      setState(() {});
                    });
                  },
                  title: Text(
                      "MeetMe Superlike Max -- (${appSettingsModel.getDouble(COUNTS_SUPER_LIKE)})"),
                  subtitle: Text("Set MeetMe Superlike Max here"),
                  trailing: Icon(Icons.navigate_next),
                ),

//                ListTile(
//                  onTap: () {
//                    pushAndResult(
//                        context,
//                        Subscriptions(
//                          type: 0,
//                        ),
//                        depend: false);
//                  },
//                  title: Text("Regular Subscription"),
//                  subtitle: Text("Set Superlike,Swipes,Features"),
//                  trailing: Icon(Icons.navigate_next),
//                ),
//                ListTile(
//                  onTap: () {
//                    pushAndResult(
//                        context,
//                        Subscriptions(
//                          type: 1,
//                        ),
//                        depend: false);
//                  },
//                  title: Text("Premium Subscription"),
//                  subtitle: Text("Set Superlike,Swipes,Prices and Features"),
//                  trailing: Icon(Icons.navigate_next),
//                ),
                ListTile(
                  onTap: () {
                    pushAndResult(
                        context,
                        inputDialog(
                          "About Us",
                          hint: "Enter About Us url link",
                          message: appSettingsModel.getString(ABOUT_LINK),
                        ), result: (_) {
                      if (null == _) return;
                      appSettingsModel
                        ..put(ABOUT_LINK, _)
                        ..updateItems();
                      setState(() {});
                    }, depend: false);
                  },
                  title: Text("About Us"),
                  subtitle: Text("Update About the app here"),
                  trailing: Icon(Icons.navigate_next),
                ),
                ListTile(
                  onTap: () {
                    pushAndResult(
                        context,
                        inputDialog(
                          "Privacy Policy",
                          hint: "Enter Privacy Policy url link",
                          message: appSettingsModel.getString(PRIVACY_LINK),
                        ), result: (_) {
                      if (null == _) return;
                      appSettingsModel
                        ..put(PRIVACY_LINK, _)
                        ..updateItems();
                      setState(() {});
                    }, depend: false);
                  },
                  title: Text("Privacy Policy"),
                  subtitle: Text("Update Privacy Policy of the app here"),
                  trailing: Icon(Icons.navigate_next),
                ),
                ListTile(
                  onTap: () {
                    pushAndResult(
                        context,
                        inputDialog(
                          "Terms of Service",
                          hint: "Enter Terms of Service url link",
                          message: appSettingsModel.getString(TERMS_LINK),
                        ), result: (_) {
                      if (null == _) return;
                      appSettingsModel
                        ..put(TERMS_LINK, _)
                        ..updateItems();
                      setState(() {});
                    }, depend: false);
                  },
                  title: Text("Terms of Service"),
                  subtitle: Text("Update Terms of Service of the app here"),
                  trailing: Icon(Icons.navigate_next),
                ),
                ListTile(
                  onTap: () {
                    pushAndResult(
                        context,
                        inputDialog(
                          "Stripe Public Key",
                          hint: "Enter Stripe Public Key",
                          message: appSettingsModel.getString(STRIPE_PUB_KEY),
                        ), result: (_) {
                      if (null == _) return;
                      appSettingsModel
                        ..put(STRIPE_PUB_KEY, _)
                        ..updateItems();
                      setState(() {});
                    }, depend: false);
                  },
                  title: Text("Stripe Public Key"),
                  subtitle: Text("Set and Update Stripe Public key Here"),
                  trailing: Icon(Icons.navigate_next),
                ),
                ListTile(
                  onTap: () {
                    pushAndResult(
                        context,
                        inputDialog(
                          "Stripe Secret Key",
                          hint: "Enter Stripe Secret Key",
                          message: appSettingsModel.getString(STRIPE_SEC_KEY),
                        ), result: (_) {
                      if (null == _) return;
                      appSettingsModel
                        ..put(STRIPE_PUB_KEY, _)
                        ..updateItems();
                      setState(() {});
                    }, depend: false);
                  },
                  title: Text("Stripe Secret Key"),
                  subtitle: Text("Set and Update Stripe Secret key Here"),
                  trailing: Icon(Icons.navigate_next),
                ),
                ListTile(
                  onTap: () {
                    pushAndResult(context, listDialog(["True", "False"]),
                        result: (_) {
                      if (null == _) return;
                      appSettingsModel
                        ..put(STRIPE_IS_LIVE, _ == "True")
                        ..updateItems();
                      setState(() {});
                    }, depend: false);
                  },
                  title: Text(
                      "Stripe in Production Mode ${appSettingsModel.getBoolean(STRIPE_IS_LIVE)}"),
                  subtitle: Text("Set Stripe Production Mode"),
                  trailing: Icon(Icons.navigate_next),
                ),
                ListTile(
                  onTap: () {
                    pushAndResult(
                        context,
                        inputDialog(
                          "Download Android Link",
                          hint: "Enter App Android Download Link",
                          message: appSettingsModel.getString(APP_LINK_ANDROID),
                        ), result: (_) {
                      if (null == _) return;
                      appSettingsModel
                        ..put(APP_LINK_ANDROID, _)
                        ..updateItems();
                      setState(() {});
                    }, depend: false);
                  },
                  title: Text("Stripe Secret Key"),
                  subtitle: Text("Set and Update Stripe Secret key Here"),
                  trailing: Icon(Icons.navigate_next),
                ),
                ListTile(
                  onTap: () {
                    pushAndResult(
                        context,
                        inputDialog(
                          "Download IPhone Link",
                          hint: "Enter App IPhone Download Link",
                          message: appSettingsModel.getString(APP_LINK_IOS),
                        ), result: (_) {
                      if (null == _) return;
                      appSettingsModel
                        ..put(APP_LINK_IOS, _)
                        ..updateItems();
                      setState(() {});
                    }, depend: false);
                  },
                  title: Text("Stripe Secret Key"),
                  subtitle: Text("Set and Update Stripe Secret key Here"),
                  trailing: Icon(Icons.navigate_next),
                ),
                ListTile(
                  onTap: () {
                    pushAndResult(
                        context,
                        inputDialog(
                          "Support Email",
                          hint: "Enter Support Email",
                          message: appSettingsModel.getString(SUPPORT_EMAIL),
                        ), result: (_) {
                      if (null == _) return;
                      appSettingsModel
                        ..put(SUPPORT_EMAIL, _)
                        ..updateItems();
                      setState(() {});
                    }, depend: false);
                  },
                  title: Text("Support Email"),
                  subtitle: Text("Set Support Email Here"),
                  trailing: Icon(Icons.navigate_next),
                ),
                ListTile(
                  onTap: () {
                    pushAndResult(context, NewUpdate());
                  },
                  title: Text("Release Update"),
//            subtitle: Text(""),
                  trailing: Icon(Icons.navigate_next),
                ),
              ],
            ],
          ),
        ),
      ),
    );
  }
}
