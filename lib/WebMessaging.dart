/*
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'AppEngine.dart';
import 'ChatMainWeb.dart';
import 'MainWebHome.dart';
import 'app_config.dart';
import 'assets.dart';
import 'basemodel.dart';
import 'dialogs/listDialog.dart';

class WebMessaging extends StatefulWidget {
  @override
  _WebMessagingState createState() => _WebMessagingState();
}

class _WebMessagingState extends State<WebMessaging> {
  @override
  Widget build(BuildContext context) {
    return page();
  }

  Container page() {
    return Container(
      margin: EdgeInsets.all(10),
      child: Material(
        color: white,
        elevation: 2,
        shadowColor: black.withOpacity(.2),
        borderRadius: BorderRadius.circular(5),
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  BackButton(),
                  Text(
                    "Messages",
                    style: textStyle(true, 13, black),
                  ),
                  Spacer(),
                ],
              ),
              addSpace(5),
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        // width: getScreenWidth(context) / 3,
                        child: Builder(builder: (ctx) {
                          if (!chatSetup) return loadingLayout();
                          if (lastMessages.isEmpty)
                            return Center(
                              child: Padding(
                                padding: const EdgeInsets.all(10),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Image.asset(
                                      ic_chat,
                                      width: 50,
                                      height: 50,
                                      color: AppConfig.appColor,
                                    ),
                                    Text(
                                      "No Chat Yet",
                                      style: textStyle(true, 20, black),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                              ),
                            );

                          return Container(
                              child: ListView.builder(
                            key: const ValueKey('cpssdList'),
                            itemBuilder: (c, p) {
                              BaseModel model = lastMessages[p];

                              String chatId = model.getString(CHAT_ID);
                              String otherPersonId = getOtherPersonId(model);
                              List partyModels = model.getList(PARTIES_MODEL);
                              Map otherPersonItem = partyModels.singleWhere(
                                  (e) => e[OBJECT_ID] == otherPersonId,
                                  orElse: () => {});
                              BaseModel otherPerson =
                                  BaseModel(items: otherPersonItem);

                              String name = otherPerson.getString(NAME);
                              String image = otherPerson.getString(IMAGE_URL);
                              if (name.isEmpty) name = "No Name";

                              return chatItem(image, name, model,
                                  p == lastMessages.length - 1, otherPerson);
                            },
                            shrinkWrap: true,
                            itemCount: lastMessages.length,
                            padding: EdgeInsets.all(0),
                          ));
                        }),
                      ),
                    ),
                    Expanded(
                      child: Builder(builder: (context) {
                        if (null == chatId && null == otherPerson)
                          return Container(
                            width: 500,
                          );
                        return ChatMainWeb(
                          chatId,
                          otherPerson: otherPerson,
                        );
                      }),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  String chatId;
  BaseModel otherPerson;

  chatItem(String image, String name, BaseModel chatModel, bool last,
      BaseModel otherPerson) {
    int type = chatModel.getInt(TYPE);
    String chatId = chatModel.getString(CHAT_ID);
    bool myItem = chatModel.myItem();

    String hisId = chatId.replaceAll(userModel.getObjectId(), "");
    bool read = chatModel.getList(READ_BY).contains(hisId);
    bool myRead = chatModel.getList(READ_BY).contains(userModel.getObjectId());
    List mutedList = userModel.getList(MUTED);
    int unread =
        unreadCounter[chatId] == null ? 0 : unreadCounter[chatId].length;

    return new InkWell(
      onLongPress: () {
        pushAndResult(
            context,
            listDialog([
              mutedList.contains(chatId) ? "Unmute Chat" : "Mute Chat",
              "Delete Chat"
            ]), result: (_) {
          if (_ == "Mute Chat" || _ == "Unmute Chat") {
            if (mutedList.contains(chatId)) {
              mutedList.remove(chatId);
            } else {
              mutedList.add(chatId);
            }
            userModel.put(MUTED, mutedList);
            userModel.updateItems();
            setState(() {});
          }
          if (_ == "Delete Chat") {
            yesNoDialog(context, "Delete Chat?",
                "Are you sure you want to delete this chat?", () {
              deleteChat(chatId);
            });
          }
        }, depend: false);
      },
      onTap: () {
        BaseModel chat = BaseModel();
        chat.put(PARTIES, [userModel.getObjectId(), otherPerson.getObjectId()]);
        chat.put(PARTIES_MODEL,
            [chatPartyModel(userModel), chatPartyModel(otherPerson)]);
        chat.saveItem(CHAT_IDS_BASE, false, document: chatId, merged: true);

        chatModel.putInList(READ_BY, userModel.getObjectId(), true);
        chatModel.updateItems();
        unreadCounter.remove(chatId);
        showNewMessageDot.removeWhere((id) => id == chatId);

        this.otherPerson = null;
        this.chatId = null;
        setState(() {});

        Future.delayed(Duration(seconds: 2), () {
          this.otherPerson = otherPerson;
          this.chatId = chatId;
          setState(() {});
        });

        pushAndResult(
            context,
            ChatMainWeb(
              chatId,
              otherPerson: otherPerson,
            ), result: (_) {
          setState(() {});
        });
      },
      //margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: Container(
        decoration: BoxDecoration(
            color: white,
            boxShadow: [BoxShadow(color: black.withOpacity(.1), blurRadius: 5)],
            borderRadius: BorderRadius.circular(5)),
        padding: EdgeInsets.fromLTRB(10, 5, 10, 0),
        margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
//                  margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                  width: 80,
                  height: 80,
                  child: Card(
                    color: black.withOpacity(.1),
                    elevation: 0,

                    clipBehavior: Clip.antiAlias,
                    shape: CircleBorder(
                        side: BorderSide(
                            color: black.withOpacity(.2), width: .9)),
                    // shape: RoundedRectangleBorder(
                    //     borderRadius: BorderRadius.all(Radius.circular(10)),
                    //     side: BorderSide(
                    //         color: black.withOpacity(.1), width: .8)),
                    child: (otherPerson.getBoolean(IS_ADMIN))
                        ? Image.asset(
                            ic_launcher,
                            width: 40,
                            height: 40,
                            fit: BoxFit.cover,
                          )
                        : Image.network(
                            image,
                            fit: BoxFit.cover,
                            width: 40,
                            height: 40,
                          ),
                  ),
                ),
                addSpaceWidth(10),
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: new Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Text(
                              //"Emeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",
                              (otherPerson.getBoolean(IS_ADMIN))
                                  ? "Strock Support"
                                  : name,
                              maxLines: 1, overflow: TextOverflow.ellipsis,
                              style: textStyle(true, 20, black),
                            ),
                          ),
                          addSpaceWidth(5),
                          !myItem && !myRead && unread > 0
                              ? */
/*Icon(
                                  Icons.new_releases,
                                  size: 20,
                                  color: white,
                                )*/ /*

                              (Container(
                                  width: 25,
                                  height: 25,
                                  decoration: BoxDecoration(
                                    color: AppConfig.appColor,
                                    shape: BoxShape.circle,
//                                      border:
//                                          Border.all(color: white, width: 2)
                                  ),
                                  child: Center(
                                      child: Text(
                                    "${unread > 9 ? "9+" : unread}",
                                    style: textStyle(true, 12, white),
                                  )),
                                ))
                              : Container(),
                          addSpaceWidth(5),
                          Text(
                            getChatTime(chatModel.getTime()),
                            style: textStyle(false, 12, black.withOpacity(.8)),
                            textAlign: TextAlign.end,
                          ),

                          //addSpaceWidth(5),
                        ],
                      ),
                      addSpace(5),
                      Row(
                        children: <Widget>[
                          Flexible(
                            fit: FlexFit.tight,
                            child: Row(
                              children: <Widget>[
                                Flexible(
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      if (myItem && read)
                                        Container(
                                            margin: EdgeInsets.only(right: 5),
                                            child: Icon(
                                              Icons.remove_red_eye,
                                              size: 12,
                                              color: blue0,
                                            )),
                                      Icon(
                                        type == CHAT_TYPE_TEXT
                                            ? Icons.message
                                            : type == CHAT_TYPE_IMAGE
                                                ? Icons.camera_alt
                                                : type == CHAT_TYPE_VIDEO
                                                    ? Icons.videocam
                                                    : type == CHAT_TYPE_REC
                                                        ? Icons.mic
                                                        : Icons.library_books,
                                        color: black.withOpacity(.8),
                                        size: 12,
                                      ),
                                      addSpaceWidth(5),
                                      Flexible(
                                        flex: 1,
                                        child: Text(
                                          chatRemoved(chatModel)
                                              ? "This message has been removed"
                                              : type == CHAT_TYPE_TEXT
                                                  ? chatModel.getString(MESSAGE)
                                                  : type == CHAT_TYPE_IMAGE
                                                      ? "Photo"
                                                      : type == CHAT_TYPE_VIDEO
                                                          ? "Video"
                                                          : type ==
                                                                  CHAT_TYPE_REC
                                                              ? "Voice Note (${chatModel.getString(AUDIO_LENGTH)})"
                                                              : "Document",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: textStyle(
                                              false, 14, black.withOpacity(.8)),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          if (mutedList.contains(chatId))
                            Image.asset(
                              ic_mute,
                              width: 18,
                              height: 18,
                              color: white.withOpacity(.5),
                            )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
            addSpace(5),
            addLine(.5, white.withOpacity(.3), 0, 0, 0, 0)
          ],
        ),
      ),
    );
  }

  deleteChat(String chatId) {
    lastMessages.removeWhere((bm) => bm.getString(CHAT_ID) == chatId);
    stopListening.add(chatId);
    userModel.putInList(DELETED_CHATS, chatId, true);
    userModel.updateItems();
    if (mounted) setState(() {});

    FirebaseFirestore.instance
        .collection(CHAT_BASE)
        .where(PARTIES, arrayContains: userModel.getUserId())
        .where(CHAT_ID, isEqualTo: chatId)
        .orderBy(TIME, descending: false)
        .getDocuments()
        .then((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel chat = BaseModel(doc: doc);
        if (chat.myItem()) {
          chat.put(DELETED, true);
          chat.updateItems();
        } else {
          List hidden = List.from(chat.getList(HIDDEN));
          hidden.add(userModel.getObjectId());
          chat.put(HIDDEN, hidden);
          chat.updateItems();
        }
      }
    });
  }
}
*/
