import 'dart:html';

import 'package:firebase_cloud_messaging_interop/firebase_cloud_messaging_interop.dart';

import 'assets.dart';

class FCMService {
  FCM fcm;

  String currentToken;

  FCMService() {
    fcm = FCM(
        publicVapidKey:
            "BKe0oiaST5GzebhuqrkrGko48rRWfeL7BdAKi68nL7KA_vtWYKCbEXcPDMiVN06Y9ukcrUBrwNVUFsjiFw185Fk");

    fcm.onMessage((e) {
      /// You can access title, body and tag
    });

    fcm.onTokenRefresh(requestPermissionAndGetToken);
  }

  void requestPermissionAndGetToken() {
    Notification.requestPermission().then((permission) {
      if (permission == 'granted') {
        fcm.getToken().then((e) {
          currentToken = e;
          userModel
            ..put(TOKEN, e)
            ..updateItems();
          print('amqueenester Token: $e');

          /// SEND TOKEN TO THE BACKEND SERVER
        });
      } else {
        /// The user doesn't want notification :(
      }
    });
  }

  void deleteCurrentToken() => fcm.deleteToken(currentToken);
}

// c4Rx2jlUYDQ:APA91bHnKmkiq3GRzs8MgVoAX6AfyzH2pJVXxT9nKxYCVL7RsoJVPYWAF8nbtkBCtdA9PD_DP4UESZXL-yvRM6sT829M9PkppAsqylV9mIh2f-JtA-XMCdctKMYdhmdzKULYj0lAQ3rA
