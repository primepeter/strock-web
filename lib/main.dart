import 'package:Strokes/assets.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

import 'MainAppSetter.dart';

RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

/// Requires that a Firestore emulator is running locally.
/// See https://firebase.flutter.dev/docs/firestore/usage#emulator-usage
bool USE_FIRESTORE_EMULATOR = false;
bool webApp = false;
//bool splashScreenShown = false;

void main() async {
  webApp = kIsWeb;
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  if (USE_FIRESTORE_EMULATOR) {
    FirebaseFirestore.instance.settings = Settings(
        host: 'localhost:8080', sslEnabled: false, persistenceEnabled: false);
  }
  runApp(MyApp());
}

var routes = <String, WidgetBuilder>{
  '/admin': (context) => MainAppSetter(
        isAdmin: true,
      ),
};

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext c) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Strock",
        color: white,
        theme: ThemeData(
          fontFamily: 'Averta',
          accentColor: Colors.orange,
          primaryColor: Colors.orange,
          pageTransitionsTheme: PageTransitionsTheme(
            builders: <TargetPlatform, PageTransitionsBuilder>{
              TargetPlatform.iOS: createTransition(),
              TargetPlatform.android: createTransition(),
            },
          ),
        ),
        navigatorObservers: [],
        //routes: routes,
        /*  home: PaymentDetails(
          amount: double.parse("15000"),
          premiumIndex: 0,
        ));*/
        home: MainAppSetter());
  }

  PageTransitionsBuilder createTransition() {
    return ZoomPageTransitionsBuilder();
  }
}

//Admin
//firebase deploy --only hosting:strock
//flutter build web && firebase deploy --only hosting:strock

//Web App
//firebase deploy --only hosting:strockfun
//flutter build web && firebase deploy --only hosting:strockfun
