import 'package:Strokes/AppEngine.dart';
import 'package:flutter/material.dart';

import 'assets.dart';

class Test1 extends StatefulWidget {
  @override
  _Test1State createState() => _Test1State();
}

class _Test1State extends State<Test1> {
  int counter = 0;
  bool updateSecond = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Flexible(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "$counter",
                  style: textStyle(true, 60, black),
                ),
                Test2DontUpdate(
                    canUpdate: updateSecond,
                    child: Text(
                      "$counter",
                      style: textStyle(true, 40, black),
                    ))
              ],
            ),
          ),
          Row(
            children: [
              Flexible(
                child: FlatButton(
                  color: blue,
                  padding: EdgeInsets.all(20),
                  onPressed: () {
                    counter = counter + 1;
                    setState(() {});
                  },
                  child: Center(
                      child: Text(
                    "Update Top Text",
                    style: textStyle(false, 16, white),
                  )),
                ),
              ),
              Flexible(
                child: FlatButton(
                  color: brown,
                  padding: EdgeInsets.all(20),
                  onPressed: () {
                    setState(() {
                      updateSecond = !updateSecond;
                    });
                  },
                  child: Center(
                      child: Text(
                    "Update Bottom Text",
                    style: textStyle(false, 16, white),
                  )),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}

class Test2DontUpdate extends StatefulWidget {
  final Widget child;
  final bool canUpdate;

  const Test2DontUpdate({Key key, this.child, this.canUpdate})
      : super(key: key);
  @override
  _Test2DontUpdateState createState() => _Test2DontUpdateState();
}

class _Test2DontUpdateState extends State<Test2DontUpdate> {
  Widget child;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    child = widget.child;
  }

  @override
  void didUpdateWidget(Test2DontUpdate oldWidget) {
    // TODO: implement didUpdateWidget

    if (widget.canUpdate != oldWidget.canUpdate) {
      child = widget.child;
      setState(() {});
    }

    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: child,
    );
  }
}
