import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'AppEngine.dart';
import 'MainWebHome.dart';
import 'app_config.dart';
import 'assets.dart';
import 'basemodel.dart';
import 'main_pages/ShowProfile.dart';

class WebVisited extends StatefulWidget {
  @override
  _WebVisitedState createState() => _WebVisitedState();
}

class _WebVisitedState extends State<WebVisited> {
  @override
  initState() {
    super.initState();
    loadProfilesViewed();
  }

  loadProfilesViewed() async {
    //load persons i have seen there profile
    Firestore.instance
        .collection(USER_BASE)
        .where(PROFILE_VIEWS, arrayContains: userModel.getUserId())
        .limit(50)
        .getDocuments()
        .then((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
        if (!model.signUpCompleted) {
//          userModel
//            ..putInList(VIEWED_LIST, model.getUserId(), false)
//            ..updateItems();
          continue;
        }
        int index = seenList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (index == -1) {
          seenList.add(model);
        } else {
          seenList[index] = model;
        }
      }
      seenSetup = true;
      if (mounted) setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      child: Material(
        color: white,
        elevation: 2,
        shadowColor: black.withOpacity(.2),
        borderRadius: BorderRadius.circular(5),
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                    border: Border.all(color: black.withOpacity(.08)),
                    borderRadius: BorderRadius.circular(4),
                    color: black.withOpacity(.02)),
                child: Row(
                  children: [
                    addSpaceWidth(10),
                    Text(
                      "Profiles Visited".toUpperCase(),
                      style: textStyle(true, 13, black),
                    ),
                    Spacer(),
                  ],
                ),
              ),
              addSpace(10),
              Expanded(
                child: Builder(builder: (ctx) {
                  if (!seenSetup) return loadingLayout();

                  if (seenList.isEmpty)
                    return Center(
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Icon(
                              Icons.visibility,
                              color: AppConfig.appColor,
                              size: 50,
                            ),
                            Text(
                              "No Profile Viewed",
                              style: textStyle(true, 20, black),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    );

                  return StaggeredGridView.countBuilder(
                    key: const ValueKey('seenByList'),
                    physics: AlwaysScrollableScrollPhysics(),
                    crossAxisCount: 8,
                    itemCount: seenList.length,
                    itemBuilder: (BuildContext context, int p) {
                      BaseModel model = seenList[p];

                      bool isVideo = false;
                      int defPos = model.getInt(DEF_PROFILE_PHOTO);
                      isVideo = model.profilePhotos[defPos].isVideo;
                      String image = model.profilePhotos[defPos]
                          .getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);

                      return Container(
                        margin: EdgeInsets.only(top: p.isOdd ? 30 : 0),
                        width: double.infinity,
                        //alignment: Alignment.center,
                        child: Stack(
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    //clickChat(context, model, false);
                                    pushAndResult(
                                        context,
                                        ShowProfile(
                                          theUser: model,
                                          //fromMeetMe: widget.fromStrock,
                                        ));
                                  },
                                  child: Container(
                                    height: 110,
                                    width: double.infinity,
                                    alignment: Alignment.center,
                                    child: Stack(
                                      children: [
                                        Align(
                                          alignment: Alignment.topCenter,
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            child: Stack(
                                              children: [
                                                Container(
                                                  width: 100,
                                                  height: 100,
                                                  color: black.withOpacity(.05),
                                                ),
                                                if (image.contains("http"))
                                                  Image.network(
                                                    image,
                                                    width: 100,
                                                    height: 100,
                                                    fit: BoxFit.cover,
                                                  ),
                                                if (isOnline(model))
                                                  Container(
                                                    height: 15,
                                                    width: 15,
                                                    padding: EdgeInsets.all(2),
                                                    // child: Text(
                                                    //   "Online",
                                                    //   style:
                                                    //       textStyle(false, 10, white),
                                                    // ),
                                                    decoration: BoxDecoration(
                                                      color: green,
                                                      borderRadius:
                                                          BorderRadius.only(
                                                        topRight:
                                                            Radius.circular(10),
                                                        bottomRight:
                                                            Radius.circular(10),
                                                      ),

                                                      //shape: BoxShape.circle
                                                    ),
                                                  ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        /* Align(
                                          alignment: Alignment.bottomCenter,
                                          child: Container(
                                              //alignment: Alignment.bottomCenter,
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 3, 5, 3),
                                              decoration: BoxDecoration(
                                                  color: AppConfig.appColor,
                                                  borderRadius:
                                                      BorderRadius.circular(25),
                                                  border: Border.all(
                                                      color: white, width: 2)),
                                              child: Text(
                                                "${(model.getDouble(DISTANCE)).roundToDouble()} KM",
                                                style:
                                                    textStyle(false, 12, white),
                                              )),
                                        )*/
                                      ],
                                    ),
                                  ),
                                ),
                                Text(
                                  model.getString(NAME),
                                  style: textStyle(true, 12, black),
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                )
                              ],
                            ),
                            if (isVideo)
                              Center(
                                child: Container(
                                  height: 25,
                                  width: 25,
                                  child: Icon(
                                    Icons.play_arrow,
                                    color: Colors.white,
                                    size: 14,
                                  ),
                                  decoration: BoxDecoration(
                                      color: Colors.black.withOpacity(0.8),
                                      border: Border.all(
                                          color: Colors.white, width: 1.5),
                                      shape: BoxShape.circle),
                                ),
                              ),
                          ],
                        ),
                      );
                    },
                    padding: EdgeInsets.all(0),
                    staggeredTileBuilder: (int index) =>
                        new StaggeredTile.extent(1, (index == 1) ? 180 : 180),
                    shrinkWrap: true,
                    mainAxisSpacing: 4.0,
                    crossAxisSpacing: 4.0,
                  );
                }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
