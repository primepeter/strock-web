import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:geoflutterfire/geoflutterfire.dart';

import 'AppEngine.dart';
import 'FilterDialog.dart';
import 'MainWebHome.dart';
import 'app_config.dart';
import 'assets.dart';
import 'basemodel.dart';
import 'main_pages/ShowProfile.dart';

class WebNearBy extends StatefulWidget {
  @override
  _WebNearByState createState() => _WebNearByState();
}

class _WebNearByState extends State<WebNearBy>
    with AutomaticKeepAliveClientMixin {
  bool setup = false;
  //List nearByList = [];
  BaseModel filterLocation;
  int genderType = -1;
  int onlineType = -1;
  int interestType = -1;
  int minAge = 18;
  int maxAge = 80;
  bool filtering = false;
  List<StreamSubscription> subs = List();

  @override
  initState() {
    super.initState();

    var prefSub = preferenceController.stream.listen((b) {
      setup = false;
      nearByList.clear();

      filterLocation = null;
      genderType = userModel.getInt(PREFERENCE);
      onlineType = -1;
      interestType = -1;
      minAge = 18;
      maxAge = 80;
      filtering = false;

      setState(() {});
      loadItems();
    });
    subs.add(prefSub);
    //nearByList = nearByList;
    //setup = nearByList.isNotEmpty;
    genderType = userModel.getInt(PREFERENCE);
    loadItems();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  var loadingSub;
  loadItems() async {
    /*if (loadingSub != null) {
      loadingSub.cancel();
      nearByList.clear();
      setup = false;
      setState(() {});
    }*/

    //loadingSub.cancel();
    nearByList.clear();
    setup = false;
    setState(() {});

    Geoflutterfire geo = Geoflutterfire();
    Map myPosition = userModel.getMap(POSITION);
    if (myPosition.isEmpty) {
      showMessage(context, Icons.error, red0, "No Location",
          "Sorry we could no find your location", onClicked: (_) {
        // Navigator.pop(context);
        setup = true;
        setState(() {});
      });
      return;
    }

    print("myPosition $myPosition");

    GeoPoint myGeoPoint = myPosition["geopoint"];
    double lat = myGeoPoint.latitude;
    double lon = myGeoPoint.longitude;

    if (filterLocation != null) {
      lat = filterLocation.getDouble(LATITUDE);
      lon = filterLocation.getDouble(LONGITUDE);
    }

    GeoFirePoint center = geo.point(latitude: lat, longitude: lon);
    final collectionReference =
        FirebaseFirestore.instance.collection(USER_BASE);

    // double radius = 10000;
    double radius = appSettingsModel.getDouble(NEARBY_RADIUS);
    double limit = appSettingsModel.getDouble(MILES_LIMIT);
    bool useLimit = appSettingsModel.getBoolean(USE_RADIUS_LIMIT);
    bool useNearby = appSettingsModel.getBoolean(USE_NEARBY_LIMIT);

    /*loadingSub =*/ geo
        .collection(collectionRef: collectionReference)
        .within(center: center, radius: radius, field: POSITION)
        .first
        .then((event) async {
      print("Has result.... ${event.length}");

      for (DocumentSnapshot doc in event) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (!model.signUpCompleted) continue;
        if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
        if (genderType != -1) {
          if ((genderType == 0 || genderType == 1) &&
              model.getInt(GENDER) != genderType) continue;
        }
        /*else {
          if (model.getInt(GENDER) != userModel.getInt(PREFERENCE)) continue;
        }*/
        //if (model.getInt(ETHNICITY) != userModel.getInt(ETHNICITY)) continue;
        if (onlineType > 0) {
          if (onlineType == 1) if (!isOnline(model)) continue;
          int now = DateTime.now().millisecondsSinceEpoch;
          if (onlineType == 2) if ((now - (model.getInt(TIME))) >
              Duration.millisecondsPerSecond) continue;
        }

        if (minAge != -1) {
          int age = getAge(DateTime.parse(model.getString(BIRTH_DATE)));
          if (minAge > age) continue;
        }
        if (maxAge != -1) {
          int age = getAge(DateTime.parse(model.getString(BIRTH_DATE)));
          if (maxAge < age) continue;
        }
        if (interestType != -1) {
          if (model.getInt(RELATIONSHIP) != interestType) continue;
        }

        final geoPoint = model.getModel(POSITION).get("geopoint") as GeoPoint;
        final distance =
            (await calculateDistanceTo(geoPoint, myGeoPoint) * 0.001);
        //if (distance > 1000) continue;
        model.put(DISTANCE, distance);
        print('distance $distance');
        int index = nearByList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (index == -1)
          nearByList.add(model);
        else
          nearByList[index] = model;
      }

      //loadingSub.cancel();
      setup = true;
      if (mounted) setState(() {});
      print("Has result.... ${nearByList.length}  done....");
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      margin: EdgeInsets.all(10),
      child: Material(
        color: white,
        elevation: 2,
        shadowColor: black.withOpacity(.2),
        borderRadius: BorderRadius.circular(5),
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                //padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    border: Border.all(color: black.withOpacity(.08)),
                    borderRadius: BorderRadius.circular(4),
                    color: black.withOpacity(.02)),
                child: Row(
                  children: [
                    addSpaceWidth(10),
                    Text(
                      "Nearby Matches".toUpperCase(),
                      style: textStyle(true, 13, black),
                    ),
                    Spacer(),
                    Container(
                      color: appColor,
                      padding: EdgeInsets.all(8),
                      child: IconButton(
                          icon: Icon(Icons.sort),
                          onPressed: () {
                            pushAndResult(
                                context,
                                FilterDialog(
                                    filtering,
                                    filterLocation,
                                    genderType,
                                    onlineType,
                                    minAge,
                                    maxAge,
                                    interestType), result: (_) {
                              List items = _;
                              filterLocation = items[0];
                              genderType = items[1];
                              onlineType = items[2];
                              minAge = items[3];
                              maxAge = items[4];
                              interestType = items[5];
                              filtering = items[6];
                              loadItems();
                            }, depend: false);
                          }),
                    )
                  ],
                ),
              ),
              addSpace(10),
              Expanded(
                child: Builder(builder: (ctx) {
                  if (!setup) return loadingLayout();

                  if (nearByList.isEmpty)
                    return Center(
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Icon(
                              Icons.location_on,
                              color: AppConfig.appColor,
                              size: 50,
                            ),
                            Text(
                              "No Nearby Matches",
                              style: textStyle(true, 20, black),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    );

                  return StaggeredGridView.countBuilder(
                    key: const ValueKey('nfgfbList'),
                    physics: AlwaysScrollableScrollPhysics(),
                    crossAxisCount: 8,
                    itemCount: nearByList.length,
                    itemBuilder: (BuildContext context, int p) {
                      BaseModel model = nearByList[p];

                      bool isVideo = false;
                      int defPos = model.getInt(DEF_PROFILE_PHOTO);
                      isVideo = model.profilePhotos[defPos].isVideo;
                      String image = model.profilePhotos[defPos]
                          .getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);

                      return Container(
                        margin: EdgeInsets.only(top: p.isOdd ? 30 : 0),
                        width: double.infinity,
                        //alignment: Alignment.center,
                        child: Stack(
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    //clickChat(context, model, false);
                                    pushAndResult(
                                        context,
                                        ShowProfile(
                                          theUser: model,
                                          //fromMeetMe: widget.fromStrock,
                                        ));
                                  },
                                  child: Container(
                                    height: 110,
                                    width: double.infinity,
                                    alignment: Alignment.center,
                                    child: Stack(
                                      children: [
                                        Align(
                                          alignment: Alignment.topCenter,
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            child: Stack(
                                              children: [
                                                Container(
                                                  width: 100,
                                                  height: 100,
                                                  color: black.withOpacity(.05),
                                                ),
                                                if (image.contains("http"))
                                                  Image.network(
                                                    image,
                                                    width: 100,
                                                    height: 100,
                                                    fit: BoxFit.cover,
                                                  ),
                                                if (isOnline(model))
                                                  Container(
                                                    height: 15,
                                                    width: 15,
                                                    padding: EdgeInsets.all(2),
                                                    // child: Text(
                                                    //   "Online",
                                                    //   style:
                                                    //       textStyle(false, 10, white),
                                                    // ),
                                                    decoration: BoxDecoration(
                                                      color: green,
                                                      borderRadius:
                                                          BorderRadius.only(
                                                        topRight:
                                                            Radius.circular(10),
                                                        bottomRight:
                                                            Radius.circular(10),
                                                      ),

                                                      //shape: BoxShape.circle
                                                    ),
                                                  ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Align(
                                          alignment: Alignment.bottomCenter,
                                          child: Container(
                                              //alignment: Alignment.bottomCenter,
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 3, 5, 3),
                                              decoration: BoxDecoration(
                                                  color: AppConfig.appColor,
                                                  borderRadius:
                                                      BorderRadius.circular(25),
                                                  border: Border.all(
                                                      color: white, width: 2)),
                                              child: Text(
                                                "${(model.getDouble(DISTANCE)).roundToDouble()} KM",
                                                style:
                                                    textStyle(false, 12, white),
                                              )),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Text(
                                  model.getString(NAME),
                                  style: textStyle(true, 12, black),
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                )
                              ],
                            ),
                            if (isVideo)
                              Center(
                                child: Container(
                                  height: 25,
                                  width: 25,
                                  child: Icon(
                                    Icons.play_arrow,
                                    color: Colors.white,
                                    size: 14,
                                  ),
                                  decoration: BoxDecoration(
                                      color: Colors.black.withOpacity(0.8),
                                      border: Border.all(
                                          color: Colors.white, width: 1.5),
                                      shape: BoxShape.circle),
                                ),
                              ),
                          ],
                        ),
                      );
                    },
                    padding: EdgeInsets.all(0),
                    staggeredTileBuilder: (int index) =>
                        new StaggeredTile.extent(1, (index == 1) ? 180 : 180),
                    shrinkWrap: true,
                    mainAxisSpacing: 4.0,
                    crossAxisSpacing: 4.0,
                  );
                }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
