import 'dart:ui';

import 'package:flutter/material.dart';

import 'MainWebHome.dart';
import 'assets.dart';

class WebPusher extends StatefulWidget {
  final Widget child;

  const WebPusher({Key key, this.child}) : super(key: key);
  @override
  _WebPusherState createState() => _WebPusherState();
}

class _WebPusherState extends State<WebPusher> {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: black.withOpacity(.4),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: GestureDetector(
              onTap: () {
                overlayController.add(true);
                Navigator.pop(context, "");
              },
              child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                  child: Container(
                    color: black.withOpacity(.6),
                    height: MediaQuery.of(context).size.height,
                  )),
            ),
          ),
          Center(
            child: Container(
              width: 500,
              color: default_white,
              child: Center(
                child: widget.child,
              ),
            ),
          )
        ],
      ),
    );
  }
}
