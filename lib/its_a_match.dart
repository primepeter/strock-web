import 'dart:ui';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';
import 'package:loading_indicator/loading_indicator.dart';

import 'MainWebHome.dart';
import 'assets.dart';

class ItsAMatch extends StatefulWidget {
  final BaseModel user;

  const ItsAMatch({Key key, this.user}) : super(key: key);
  @override
  _ItsAMatchState createState() => _ItsAMatchState();
}

class _ItsAMatchState extends State<ItsAMatch> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    overlayController.add(false);
    pushNotificationToUser(widget.user, NOTIFY_MATCHED);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        overlayController.add(true);
        Navigator.pop(context, "");
        return false;
      },
      child: Material(
        color: transparent,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Align(
              alignment: Alignment.center,
              child: CachedNetworkImage(
                imageUrl: widget.user.profilePhotos[0].imageUrl,
                fit: BoxFit.cover,
                height: MediaQuery.of(context).size.height,
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: GestureDetector(
                onTap: () {
                  overlayController.add(true);
                  Navigator.pop(context, "");
                },
                child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                    child: Container(
                      color: black.withOpacity(.6),
                      height: MediaQuery.of(context).size.height,
                    )),
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                          width: 140,
                          height: 140,
                          child: LoadingIndicator(
                            indicatorType: Indicator.ballScaleMultiple,
                            color: red,
                          )),
                      Container(
                        height: 80,
                        width: 80,
                        decoration:
                            BoxDecoration(color: red, shape: BoxShape.circle),
                        child: Icon(
                          Icons.favorite,
                          size: 50,
                          color: white,
                        ),
                      ),
                    ],
                  ),
                  addSpace(10),
                  Text(
                    "It's a Match!",
                    style: /*GoogleFonts.niconne*/ TextStyle(
                        color: white,
                        fontSize: 60,
                        fontWeight: FontWeight.bold),
                    //style: textStyle(true, 25, white),
                  ),
                  addSpace(10),
                  FlatButton(
                    onPressed: () {
                      clickChat(context, widget.user, false,
                          replace: true, depend: false);
                    },
                    color: AppConfig.appColor,
                    padding: EdgeInsets.all(15),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    child: Text(
                      "Chat Now!",
                      style: textStyle(true, 16, white),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
