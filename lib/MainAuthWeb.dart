import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainAuthAdmin.dart';
import 'package:Strokes/app_config.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login_web/flutter_facebook_login_web.dart';
// import 'package:flutter_facebook_login_web/flutter_facebook_login_web.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'MainWebHome.dart';
import 'app/navigation.dart';
import 'assets.dart';
import 'auth/auth_main.dart';
import 'basemodel.dart';

String privacyLink =
    "https://www.datahr-solutions.co.uk/services/strock-privacy-policy/";
String termsLink =
    "https://www.datahr-solutions.co.uk/services/strock-privacy-policy/";

class MainAuthWeb extends StatefulWidget {
  @override
  _MainAuthWebState createState() => _MainAuthWebState();
}

class _MainAuthWebState extends State<MainAuthWeb> {
  final vp = PageController();
  int currentPage = 0;

  String _message;
  final String clientId = "FBClientId";
  final String redirectUri = "http://localhost";

  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: white,
      child: LayoutBuilder(builder: (context, con) {
        double padding = scaledSize(100);
        bool showRight = true;
        bool enableColumn2 = false;
        bool enableColumn = false;
        if (con.maxWidth <= 1100) {
          padding = scaledSize(50);
          enableColumn2 = true;
        }

        if (con.maxWidth <= 800) {
          enableColumn2 = false;
          enableColumn = true;
        }
        print('wddd ${con.maxWidth}');

        return Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(
                        "assets/icons/auth_bg.jpg",
                      ),
                      fit: BoxFit.cover)),
            ),
            Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
                black.withOpacity(.1),
                black.withOpacity(.4),
              ])),
            ),
            Align(
              alignment: Alignment.center,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 50),
                    child: GestureDetector(
                      onLongPress: () {
                        pushAndResult(context, MainAuthAdmin());
                      },
                      child: Container(
                        padding: EdgeInsets.all(15),
                        child: Text(
                          "A few clicks away from meeting people 😇",
                          textAlign: TextAlign.center,
                          style: textStyle(true, 25, white),
                        ),
                      ),
                    ),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: List.generate(2, (p) {
                      String asset = ic_facebook;
                      String text = "CONTINUE WITH FACEBOOK";
                      Color color = Color(0xFF4267B2);

                      if (p == 1) {
                        asset = ic_google;
                        text = "CONTINUE WITH GOOGLE";
                        color = Colors.deepOrange;
                      }

                      if (p == 2) {
                        asset = ic_gender;
                        text = "Others";
                      }

                      return Container(
                        margin: EdgeInsets.all(10),
                        child: RaisedButton.icon(
                            onPressed: () {
                              if (p == 0) {
                                handleSignIn("facebook");
                              }
                              if (p == 1) {
                                handleSignIn("google");
                              }
                            },
                            color: color,
                            padding: const EdgeInsets.all(20),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            icon: Image.asset(
                              asset,
                              height: 18,
                              width: 18,
                              color: white.withOpacity(.8),
                            ),
                            label: Container(
                              width: 200,
                              alignment: Alignment.center,
                              child: Text(
                                text,
                                style: textStyle(true, 12, Colors.white),
                              ),
                            )),
                      );
                    }),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                              text:
                                  'By Clicking on "CONTINUE WITH", You hereby agree to our ',
                              style: textStyle(false, 14, white)),
                          TextSpan(
                              text: 'Terms of Service',
                              recognizer: new TapGestureRecognizer()
                                ..onTap = () => openLink(termsLink),
                              style: textStyle(true, 14, AppConfig.appColor)),
                          TextSpan(
                              text: ' and ',
                              style: textStyle(false, 14, white)),
                          TextSpan(
                              text: 'Privacy Policy',
                              recognizer: new TapGestureRecognizer()
                                ..onTap = () => openLink(privacyLink),
                              style: textStyle(true, 14, AppConfig.appColor)),
                          TextSpan(
                              text: ' Binding our community.',
                              style: textStyle(false, 12, white)),
                        ],
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            )
          ],
        );

        /*return Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  addSpace(10),
                  RaisedButton(
                    onPressed: () {
                      pushAndResult(context, AlertAuth());
                    },
                    color: blue3,
                    elevation: 10,
                    padding: EdgeInsets.all(20),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    child: Text(
                      'Continue to Strock'.toUpperCase(),
                      style: textStyle(true, 14, white),
                    ),
                  ),
                  RowColumnFlexer(
                    enableColumn: enableColumn,
                    children: [
                      Container(
                        margin: EdgeInsets.all(30),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(25),
                          child: Image.asset(
                            'assets/icons/strockBg.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      if (enableColumn)
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text.rich(
                              TextSpan(children: [
                                TextSpan(
                                    text: 'Strock ',
                                    style: textStyle(true, 40, black)),
                                TextSpan(
                                    text: 'on the Go',
                                    style:
                                        textStyle(true, 40, AppConfig.appColor))
                              ]),
                              textAlign: TextAlign.center,
                            ),
                            addSpace(10),
                            Text(
                                "Life is short, so don’t waste time. Meet new people whenever you want!\n"
                                "Strock is available on any device. Stay in touch with thousands of potential dates!",
                                textAlign: TextAlign.center,
                                style: textStyle(false, 20, black)),
                            addSpace(10),
                            RowColumnFlexer(
                                enableColumn: enableColumn2,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      openLink(
                                          'https://play.google.com/store/apps/details?id=com.strock');
                                    },
                                    borderRadius: BorderRadius.circular(30),
                                    child: Container(
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: black, width: 2),
                                          color: AppConfig.appColor,
                                          borderRadius:
                                              BorderRadius.circular(30)),
                                      width: enableColumn2 ? 150 : 200,
                                      padding: EdgeInsets.all(15),
                                      child: Image.asset(
                                        'assets/icons/ic_play_store.png',
                                        height: 40,
                                        width: 40,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  enableColumn2
                                      ? addSpace(10)
                                      : addSpaceWidth(10),
                                  InkWell(
                                    onTap: () {
                                      openLink(
                                          'https://play.google.com/store/apps/details?id=com.strock');
                                    },
                                    borderRadius: BorderRadius.circular(30),
                                    child: Container(
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: AppConfig.appColor,
                                              width: 2),
                                          color: black,
                                          borderRadius:
                                              BorderRadius.circular(30)),
                                      width: enableColumn2 ? 150 : 200,
                                      padding: EdgeInsets.all(15),
                                      child: Image.asset(
                                        'assets/icons/ic_apple_store.png',
                                        height: 40,
                                        width: 40,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ]),
                          ],
                        )
                      else
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text.rich(TextSpan(children: [
                                TextSpan(
                                    text: 'Strock ',
                                    style: textStyle(true, 40, black)),
                                TextSpan(
                                    text: 'on the Go',
                                    style:
                                        textStyle(true, 40, AppConfig.appColor))
                              ])),
                              addSpace(10),
                              Text(
                                  "Life is short, so don’t waste time. Meet new people whenever you want!\n\n"
                                  "Strock is available on any device. Stay in touch with thousands of potential dates!",
                                  style: textStyle(false, 20, black)),
                              addSpace(10),
                              RowColumnFlexer(
                                  enableColumn: enableColumn2,
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        openLink(
                                            'https://play.google.com/store/apps/details?id=com.strock');
                                      },
                                      borderRadius: BorderRadius.circular(30),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: black, width: 2),
                                            color: AppConfig.appColor,
                                            borderRadius:
                                                BorderRadius.circular(30)),
                                        width: 200,
                                        padding: EdgeInsets.all(15),
                                        child: Image.asset(
                                          'assets/icons/ic_play_store.png',
                                          height: 40,
                                          width: 40,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    enableColumn2
                                        ? addSpace(10)
                                        : addSpaceWidth(10),
                                    InkWell(
                                      onTap: () {
                                        openLink(
                                            'https://play.google.com/store/apps/details?id=com.strock');
                                      },
                                      borderRadius: BorderRadius.circular(30),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: AppConfig.appColor,
                                                width: 2),
                                            color: black,
                                            borderRadius:
                                                BorderRadius.circular(30)),
                                        width: 200,
                                        padding: EdgeInsets.all(15),
                                        child: Image.asset(
                                          'assets/icons/ic_apple_store.png',
                                          height: 40,
                                          width: 40,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                  ]),
                            ],
                          ),
                        )
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.all(20),
                    child: Text(
                      '©2020 Strock.fun',
                      style: textStyle(false, 16, black),
                    ),
                  ),
                ],
              ),
            ),
          ],
        );*/
      }),
    );
  }

  double scaledSize(double size) {
    double screenWidth = getScreenWidth(context);
    double scaleFactor = (screenWidth / 1000).clamp(0.5, 1);
    return size * scaleFactor;
  }

  handleSignIn(String type) async {
    showProgress(true, context, msg: "Logging In");
    if (type == "google") {
      GoogleSignIn googleSignIn = GoogleSignIn();
      googleSignIn.signIn().then((account) async {
        account.authentication.then((googleAuth) {
          final credential = GoogleAuthProvider.credential(
            accessToken: googleAuth.accessToken,
            idToken: googleAuth.idToken,
          );
          loginIntoApp(credential, AUTH_PROVIDER_GOOGLE);
        }).catchError((e) {
          onError("Error 001", e);
        });
      }).catchError((e) {
        onError("Error 01", e);
      });
    }

    if (type == "facebook") {
      FacebookLoginWeb().logIn(['email']).then((account) {
        final credential = FacebookAuthProvider.credential(
          account.accessToken.token,
        );
        loginIntoApp(credential, AUTH_PROVIDER_FACEBOOK);
      }).catchError((e) {
        onError("Error 02", e);
      });
    }

    /*if (type == "apple") {
      SignInWithApple.getAppleIDCredential(
        scopes: [
          AppleIDAuthorizationScopes.email,
          AppleIDAuthorizationScopes.fullName,
        ],
      ).then((value) {
        final credential = OAuthProvider('apple.com').credential(
          accessToken: value.authorizationCode,
          idToken: value.identityToken,
        );
        loginIntoApp(credential);
      }).catchError((e) {
        onError("Error 04", e);
      });
    }*/
  }

  loginIntoApp(AuthCredential credential, int provider) async {
    FirebaseAuth.instance.signInWithCredential(credential).then((value) async {
      final account = value.user;
      FirebaseFirestore.instance
          .collection(USER_BASE)
          .doc(account.uid)
          .get()
          .then((doc) {
        if (!doc.exists) {
          userModel
            ..put(OBJECT_ID, account.uid)
            ..put(USER_ID, account.uid)
            ..put(EMAIL, account.email)
            ..put(USER_IMAGE, account.photoURL)
            ..put(NAME, account.displayName)
            ..saveItem(USER_BASE, false, document: account.uid, merged: true);
          popUpUntil(context, AuthMain());
          return;
        }
        userModel = BaseModel(doc: doc);
        if (!userModel.signUpCompleted) {
          popUpUntil(context, AuthMain());
          return;
        }

        popUpUntil(context, MainWebHome());
      }).catchError((e) {
        onError("Error 04", e);
      });
    }).catchError((e) {
      onError("Error 03", e);
    });
  }

  onError(String type, e) {
    showProgress(false, context);
    showMessage(context, Icons.error, red0, type, e.toString(),
        delayInMilli: 950, cancellable: true);
  }
}

/*class RowColumnFlexer extends StatelessWidget {
  final bool enableColumn;
  final List<Widget> children;
  const RowColumnFlexer(
      {Key key, @required this.enableColumn, @required this.children})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    if (enableColumn)
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: children,
      );

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: children,
    );
  }
}

class AlertAuth extends StatefulWidget {
  @override
  _AlertAuthState createState() => _AlertAuthState();
}

class _AlertAuthState extends State<AlertAuth> {
  @override
  Widget build(BuildContext context) {
    return Material(
        color: black.withOpacity(.5),
        child: Stack(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                color: transparent,
                alignment: Alignment.center,
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Container(
                decoration: BoxDecoration(
                    color: white, borderRadius: BorderRadius.circular(30)),
                padding: EdgeInsets.all(30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    // Text(
                    //   'Sign In or Create an Account',
                    //   style: textStyle(true, 20, black),
                    // ),
                    Image.asset(
                      ic_launcher,
                      height: 40,
                      width: 40,
                      fit: BoxFit.cover,
                    ),
                    addSpaceWidth(5),
                    Text(
                      'Strock.fun',
                      style: textStyle(false, 20, black),
                    ),
                    addSpace(10),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: List.generate(2, (p) {
                        String asset = ic_facebook;
                        String text = "CONTINUE WITH FACEBOOK";
                        Color color = Color(0xFF4267B2);

                        if (p == 1) {
                          asset = ic_google;
                          text = "CONTINUE WITH GOOGLE";
                          color = Colors.deepOrange;
                        }

                        if (p == 2) {
                          asset = ic_gender;
                          text = "Others";
                        }

                        return Container(
                          margin: EdgeInsets.all(10),
                          child: RaisedButton.icon(
                              onPressed: () {
                                if (p == 0) {
                                  //handleSignIn("facebook");
                                }
                                if (p == 1) {
                                  handleSignIn("google");
                                }
                              },
                              color: color,
                              padding: const EdgeInsets.all(15.0),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25)),
                              icon: Image.asset(
                                asset,
                                height: 18,
                                width: 18,
                                color: white.withOpacity(.8),
                              ),
                              label: Container(
                                //width: 150,
                                alignment: Alignment.center,
                                child: Text(
                                  text,
                                  style: textStyle(true, 12, Colors.white),
                                ),
                              )),
                        );
                      }),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  height: 55,
                  width: 55,
                  margin: EdgeInsets.all(10),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        height: 55,
                        width: 55,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: white.withOpacity(.5), width: 3),
                            shape: BoxShape.circle),
                      ),
                      Container(
                        height: 50,
                        width: 50,
                        child: Icon(
                          Icons.clear,
                          color: white,
                        ),
                        decoration:
                            BoxDecoration(color: red, shape: BoxShape.circle),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ));
  }

  handleSignIn(String type) async {
    showProgress(true, context, msg: "Loggin In");
    if (type == "google") {
      GoogleSignIn googleSignIn = GoogleSignIn();
      googleSignIn.signIn().then((account) async {
        account.authentication.then((googleAuth) {
          final credential = GoogleAuthProvider.getCredential(
            accessToken: googleAuth.accessToken,
            idToken: googleAuth.idToken,
          );
          loginIntoApp(credential, AUTH_PROVIDER_GOOGLE);
        }).catchError((e) {
          onError("Error 001", e);
        });
      }).catchError((e) {
        onError("Error 01", e);
      });
    }

    if (type == "facebook") {
      // FacebookLoginWeb().logIn(['email']).then((account) {
      //   final credential = FacebookAuthProvider.credential(
      //     account.accessToken.token,
      //   );
      //   loginIntoApp(credential, AUTH_PROVIDER_FACEBOOK);
      // }).catchError((e) {
      //   onError("Error 02", e);
      // });
    }

    */ /*if (type == "apple") {
      SignInWithApple.getAppleIDCredential(
        scopes: [
          AppleIDAuthorizationScopes.email,
          AppleIDAuthorizationScopes.fullName,
        ],
      ).then((value) {
        final credential = OAuthProvider('apple.com').credential(
          accessToken: value.authorizationCode,
          idToken: value.identityToken,
        );
        loginIntoApp(credential);
      }).catchError((e) {
        onError("Error 04", e);
      });
    }*/ /*
  }

  loginIntoApp(AuthCredential credential, int provider) async {
    FirebaseAuth.instance.signInWithCredential(credential).then((value) async {
      final account = value.user;
      Firestore.instance
          .collection(USER_BASE)
          .document(account.uid)
          .get()
          .then((doc) {
        userModel = BaseModel(doc: doc);
        if (!doc.exists || !userModel.signUpCompleted) {
          showProgress(false, context);
          showMessage(
              context,
              Icons.error_sharp,
              red,
              "SignUp Required",
              "Our Web Version Doesn't Support Signing up yet.Please"
                  " download our app and sign up to be able to access strock web",
              delayInMilli: 1500);
          // userModel
          //   ..put(USER_ID, account.uid)
          //   ..put(EMAIL, account.email)
          //   ..put(USER_IMAGE, account.photoUrl)
          //   ..put(NAME, account.displayName)
          //   ..saveItem(USER_BASE, false, document: account.uid, onComplete: () {
          //     pushAndResult(context, AuthMain());
          //   });
          return;
        }

        SharedPreferences.getInstance().then((pref) {
          final model = userModel
            ..remove(CREATED_AT)
            ..remove(UPDATED_AT)
            ..remove(POSITION)
            ..remove(MY_LOCATION);
          pref.setString(USER_BASE, jsonEncode(model.items));
          popUpUntil(context, MainWebHome());
        });
      }).catchError((e) {
        onError("Error 04", e);
      });
    }).catchError((e) {
      onError("Error 03", e);
    });
  }

  onError(String type, e) {
    showProgress(false, context);
    showMessage(context, Icons.error, red0, type, e.toString(),
        delayInMilli: 950, cancellable: true);
  }
}*/
