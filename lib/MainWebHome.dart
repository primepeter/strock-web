import 'dart:async';
import 'dart:io';
import 'dart:io' as io;
import 'dart:ui';

import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:Strokes/main_pages/ShowProfile.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:js/js.dart';
// import 'package:location/location.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:synchronized/synchronized.dart';

import 'AppEngine.dart';
import 'ChatMain.dart';
import 'FCMService.dart';
import 'LocationJs.dart';
import 'ReportMain.dart';
import 'WebHomeDesktop.dart';
import 'app_config.dart';
import 'its_a_match.dart';
import 'main_pages/ShowProfileViews.dart';
import 'main_pages/ShowRecommendations.dart';
import 'main_pages/chat_page.dart';
import 'main_pages/nearby_search.dart';
import 'tinder_cards/meetMe.dart';

Map<String, List> unreadCounter = Map();
Map otherPeronInfo = Map();
List<BaseModel> allStoryList = new List();
final fbMessaging = FirebaseMessaging();
final chatMessageController = StreamController<bool>.broadcast();
final homeRefreshController = StreamController<bool>.broadcast();
final uploadingController = StreamController<String>.broadcast();
final pageSubController = StreamController<int>.broadcast();
final overlayController = StreamController<bool>.broadcast();
final matchedController = StreamController<BaseModel>.broadcast();
final subscriptionController = StreamController<bool>.broadcast();
final preferenceController = StreamController<bool>.broadcast();
final adsController = StreamController<bool>.broadcast();

bool packagesAvailable = false;
bool _purchasePending = false;
bool _loading = true;
String _queryProductError;

List connectCount = [];
List<String> stopListening = List();
List<BaseModel> lastMessages = List();
List loadedIds = [];
bool chatSetup = false;
List showNewMessageDot = [];
bool showNewNotifyDot = false;
List newStoryIds = [];
String visibleChatId;
bool itemsLoaded = false;
List nearByList = [];
bool nearbyReady = false;

List<BaseModel> recommendedMatches = [];
bool recommendedSetup = false;

GeoFirePoint myLocation;

bool serviceEnabled = false;

List<BaseModel> adsList = [];
bool adsSetup = false;

List<BaseModel> likesList = [];
bool likesSetup = false;
List<BaseModel> superLikesList = [];
bool superSetup = false;
List<BaseModel> matchesList = [];
bool matchesSetup = false;
List<BaseModel> seenList = [];
bool seenSetup = false;
List<BaseModel> seenByList = [];
bool seenBySetup = false;

List<BaseModel> meetMeList = [];
bool meetMeSetup = false;

class MainWebHome extends StatefulWidget {
  @override
  _MainWebHomeState createState() => _MainWebHomeState();
}

class _MainWebHomeState extends State<MainWebHome>
    with WidgetsBindingObserver, AutomaticKeepAliveClientMixin {
  PageController peoplePageController = PageController();
  List<StreamSubscription> subs = List();
  int timeOnline = 0;
  String noInternetText = "";

  String flashText = "";
  bool setup = false;
  bool settingsLoaded = false;
  String uploadingText;
  int peopleCurrentPage = 0;
  bool tipHandled = false;
  bool tipShown = true;

  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance.removeObserver(this);
    for (var sub in subs) sub?.cancel();
    strockImagesTimer?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    itemsLoaded = false;
    WidgetsBinding.instance.addObserver(this);
    Future.delayed(Duration(seconds: 1), () {
      createUserListener();
    });
    var subSub = subscriptionController.stream.listen((b) {
      if (!b) return;
      showMessage(context, Icons.check, green, "Congratulations!",
          "You are now a Premium User. Enjoy the benefits!");
      loadSeenBy();
    });
    subs.add(subSub);
    var pageSub = pageSubController.stream.listen((int p) {
      peopleCurrentPage = p;
      if (mounted) setState(() {});
    });
    subs.add(pageSub);
    var uploadingSub = uploadingController.stream.listen((text) {
      uploadingText = text;
      if (mounted) setState(() {});
    });
    subs.add(uploadingSub);

    var prefSub = preferenceController.stream.listen((b) {
      reloadPreference(true);
    });
    subs.add(prefSub);

    var matchedSub = matchedController.stream.listen((user) {
      userModel
        ..putInList(MATCHED_LIST, user.getUserId(), true)
        ..updateItems();
      Future.delayed(Duration(milliseconds: 15), () {
        pushAndResult(
            context,
            ItsAMatch(
              user: user,
            ),
            depend: false);
      });
    });
    subs.add(matchedSub);

    var refresh = homeRefreshController.stream.listen((event) {
      setState(() {});
    });
    subs.add(refresh);
  }

  checkTip() {
    if (!tipHandled &&
        peopleCurrentPage != nearByList.length - 1 &&
        nearByList.length > 1) {
      tipHandled = true;
      Future.delayed(Duration(seconds: 1), () async {
        SharedPreferences pref = await SharedPreferences.getInstance();
        tipShown = pref.getBool("swipe_tipx") ?? false;
        if (!tipShown) {
          pref.setBool("swipe_tipx", true);
          if (mounted) setState(() {});
        }
      });
    }
  }

  okLayout(bool manually) {
    checkTip();
    itemsLoaded = true;
    if (mounted) setState(() {});
    Future.delayed(Duration(milliseconds: 1000), () {
//      if(manually)pageScrollController.add(-1);
      if (manually) peoplePageController.jumpToPage(peopleCurrentPage);
    });
  }

  Future<int> getSeenCount(String id) async {
    var pref = await SharedPreferences.getInstance();
    List<String> list = pref.getStringList(SHOWN) ?? [];
    int index = list.indexWhere((s) => s.contains(id));
    if (index != -1) {
      String item = list[index];
      var parts = item.split(SPACE);
      int seenCount = int.parse(parts[1].trim());
      return seenCount;
    }
    return 0;
  }

  void createUserListener() async {
    var userSub = FirebaseFirestore.instance
        .collection(USER_BASE)
        .doc(userModel.getUserId())
        .snapshots()
        .listen((shot) async {
      if (shot != null) {
        userModel = BaseModel(doc: shot);

        if (userModel.getBoolean(DELETED)) {
          shot.reference.delete();
          userModel = BaseModel();
          return;
        }

        isAdmin = userModel.getBoolean(IS_ADMIN) ||
            userModel.getString(EMAIL).contains('maugost') ||
            userModel.getString(EMAIL) == "johnebere58@gmail.com" ||
            userModel.getString(EMAIL) == "ammaugost@gmail.com";
        loadBlocked();
        if (userModel.getInt(DEF_PROFILE_PHOTO).isNegative) {
          userModel
            ..put(DEF_PROFILE_PHOTO, 0)
            ..updateItems(delaySeconds: 2);
        }

        if (!userModel.getBoolean(FIX_APPLIED)) {
          userModel
            ..put(VIEWED_LIST, [])
            ..put(FIX_APPLIED, true)
            ..updateItems(delaySeconds: 2);
        }

        if (!settingsLoaded) {
          settingsLoaded = true;
          loadSettings();
        }
      }
    });
    subs.add(userSub);
  }

  loadSettings() async {
    var settingsSub = Firestore.instance
        .collection(APP_SETTINGS_BASE)
        .document(APP_SETTINGS)
        .snapshots()
        .listen((shot) {
      if (shot != null) {
        appSettingsModel = BaseModel(doc: shot);
        chkUpdate();
        List banned = appSettingsModel.getList(BANNED);

        if (banned.contains(userModel.getObjectId()) ||
            banned.contains(userModel.getString(DEVICE_ID)) ||
            banned.contains(userModel.getEmail())) {
          io.exit(0);
        }

        String genMessage = appSettingsModel.getString(GEN_MESSAGE);
        int genMessageTime = appSettingsModel.getInt(GEN_MESSAGE_TIME);

        if (userModel.getInt(GEN_MESSAGE_TIME) != genMessageTime &&
            genMessageTime > userModel.getTime()) {
          userModel.put(GEN_MESSAGE_TIME, genMessageTime);
          userModel.updateItems();

          String title = !genMessage.contains("+")
              ? "Announcement!"
              : genMessage.split("+")[0].trim();
          String message = !genMessage.contains("+")
              ? genMessage
              : genMessage.split("+")[1].trim();
          showMessage(context, Icons.info, blue0, title, message);
        }

        if (!setup) {
          setup = true;

          if (userModel.subscriptionExpired)
            userModel
              ..put(ACCOUNT_TYPE, ACCOUNT_TYPE_REGULAR)
              ..updateItems();

          blockedIds.addAll(userModel.getList(BLOCKED));
          onResume();
          //loadNotification();
          //requestNotificationPermission();
          applyProfileViews();
          //loadMessages();
          //applyLocationFix();
          loadBlocked();
          setUpLocation();
          loadSeenBy();
          loadMatches();
          loadLikes();
          loadSuperLikes();
          loadProfilesViewed();
          //reloadPreference(false);

          /* Future.delayed(Duration(milliseconds: 15), () {
            pushAndResult(
                context,
                ItsAMatch(
                  user: userModel,
                ),
                depend: false);
          });*/
        }
      }
    });
    subs.add(settingsSub);
  }

  applyProfileViews() async {
    List views = userModel.getList(VIEWED_LIST);
    List deleted = userModel.getList(DELETED_ACCOUNTS);
    for (int p = 0; p < views.length; p++) {
      String id = views[p];
      final doc =
          await FirebaseFirestore.instance.collection(USER_BASE).doc(id).get();
      if (doc == null || !doc.exists) {
        userModel
          ..putInList(VIEWED_LIST, id, false)
          ..updateItems(delaySeconds: 1);
        continue;
      }
      BaseModel model = BaseModel(doc: doc);
      if (model.getBoolean(DELETED) || deleted.contains(id)) {
        userModel
          ..putInList(VIEWED_LIST, id, false)
          ..updateItems(delaySeconds: 1);
        if (deleted.contains(id)) {
          model.deleteItem();
          appSettingsModel
            ..putInList(DELETED_ACCOUNTS, id, false)
            ..updateItems(delaySeconds: 1);
        }
        continue;
      }
      userModel
        ..putInList(VIEWED_LIST, id, false)
        ..updateItems(delaySeconds: 1);
      model
        ..putInList(PROFILE_VIEWS, userModel.getUserId())
        ..updateItems(delaySeconds: 1);
    }
  }

  reloadPreference(bool nearby) {
    loadMeetMe();
    //loadRecommended();
    loadNearby();
  }

  String token = "";

  requestNotificationPermission() async {
    FCMService fcm = FCMService();
    fcm.requestPermissionAndGetToken();
  }

  loadSeenBy() async {
    //load persons i have seen there profile
    Firestore.instance
        .collection(USER_BASE)
        .where(VIEWED_LIST, arrayContains: userModel.getUserId())
        .limit(25)
        .getDocuments()
        .then((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
        if (!model.signUpCompleted) {
//          userModel
//            ..putInList(VIEWED_LIST, model.getUserId(), false)
//            ..updateItems();
          continue;
        }
        int index = seenByList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (index == -1) {
          seenByList.add(model);
        } else {
          seenByList[index] = model;
        }
      }
      seenBySetup = true;
      if (mounted) setState(() {});
    });
  }

  loadMatches() async {
    //load persons i have been matched with
    //load my matches
    FirebaseFirestore.instance
        .collection(USER_BASE)
        .where(MATCHED_LIST, arrayContains: userModel.getObjectId())
        .where(SIGNUP_COMPLETED, isEqualTo: true)
        .limit(50)
        .getDocuments()
        .then((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
        int index = matchesList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (index == -1) {
          matchesList.add(model);
        } else {
          matchesList[index] = model;
        }
      }
      matchesSetup = true;
      if (mounted) setState(() {});
    });
  }

  loadSuperLikes() async {
    Firestore.instance
        .collection(USER_BASE)
        .where(SUPER_LIKE_LIST, arrayContains: userModel.getObjectId())
        .limit(50)
        .getDocuments()
        .then((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
        if (!model.signUpCompleted) {
//          userModel
//            ..putInList(SUPER_LIKE_LIST, model.getUserId(), false)
//            ..updateItems();
          continue;
        }
        //if (!userModel.getList(SUPER_LIKE_LIST).contains(model.getObjectId()))continue;
        int index = superLikesList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (index == -1) {
          superLikesList.add(model);
        } else {
          superLikesList[index] = model;
        }
      }
      superSetup = true;
      if (mounted) setState(() {});
    });
  }

  loadLikes() async {
    Firestore.instance
        .collection(USER_BASE)
        .where(LIKE_LIST, arrayContains: userModel.getObjectId())
        .limit(50)
        .getDocuments()
        .then((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
        if (!model.signUpCompleted) {
//          userModel
//            ..putInList(LIKE_LIST, model.getUserId(), false)
//            ..updateItems();
          continue;
        }
        //if (!userModel.getList(LIKE_LIST).contains(model.getObjectId()))continue;
        int index = likesList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (index == -1) {
          likesList.add(model);
        } else {
          likesList[index] = model;
        }
      }
      likesSetup = true;
      if (mounted) setState(() {});
    });
  }

  loadMeetMe() async {
    return;
    meetMeList.clear();
    double limit = appSettingsModel.getDouble(MILES_LIMIT);
    bool useLimit = appSettingsModel.getBoolean(USE_RADIUS_LIMIT);
    QuerySnapshot shots = await FirebaseFirestore.instance
        .collection(USER_BASE)
        .where(GENDER, isEqualTo: userModel.getInt(PREFERENCE))
        .limit(50)
        .get();

    for (DocumentSnapshot doc in shots.documents) {
      BaseModel model = BaseModel(doc: doc);
      if (model.myItem()) continue;
      if (!model.signUpCompleted) continue;
      if (model.getList(MATCHED_LIST).contains(userModel.getUserId())) continue;

      if (model.getMap(POSITION).isEmpty) continue;

      /*  final geoPoint = model.getModel(POSITION).get("geopoint") as GeoPoint;
      final distance = (await calculateDistanceTo(geoPoint) / 1000);

      if (distance > 1000) continue;*/
      //print("Distance $distance limit $limit");
      //model.put(DISTANCE, distance);
      int index = meetMeList
          .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
      if (index == -1) {
        meetMeList.add(model);
      } else {
        meetMeList.add(model);
      }
    }

    meetMeSetup = true;
    if (mounted) setState(() {});

    //injectAds();
  }

  loadProfilesViewed() async {
    //load persons i have seen there profile
    Firestore.instance
        .collection(USER_BASE)
        .where(PROFILE_VIEWS, arrayContains: userModel.getUserId())
        .limit(25)
        .getDocuments()
        .then((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
        if (!model.signUpCompleted) {
//          userModel
//            ..putInList(VIEWED_LIST, model.getUserId(), false)
//            ..updateItems();
          continue;
        }
        int index = seenList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (index == -1) {
          seenList.add(model);
        } else {
          seenList[index] = model;
        }
      }
      seenSetup = true;
      if (mounted) setState(() {});
    });
  }

  loadAds() async {
    var adsSub = Firestore.instance
        .collection(ADS_BASE)
        .where(COUNTRY, isEqualTo: userModel.getString(COUNTRY))
        .where(ADS_EXPIRY, isLessThan: DateTime.now().millisecondsSinceEpoch)
        .snapshots()
        .listen((shots) {
      for (var doc in shots.documentChanges) {
        final model = BaseModel(doc: doc.document);
        if (doc.type == DocumentChangeType.removed) {
          adsList.removeWhere((e) => e.getObjectId() == model.getObjectId());
          continue;
        }
        //if (model.myItem()) continue;
        if (model.getInt(STATUS) != APPROVED) continue;
        int p =
            adsList.indexWhere((e) => e.getObjectId() == model.getObjectId());
        if (p != -1) {
          adsList[p] = model;
        } else {
          adsList.add(model);
        }
      }

      adsSetup = true;
      if (mounted) setState(() {});
    });
    subs.add(adsSub);
  }

  loadRecommended({bool reset = false}) async {
    recommendedMatches.clear();
    if (mounted) setState(() {});
    QuerySnapshot shots1 = await FirebaseFirestore.instance
        .collection(USER_BASE)
        .where(GENDER, isEqualTo: userModel.getInt(PREFERENCE))
        //.where(RELATIONSHIP, isEqualTo: userModel.getInt(RELATIONSHIP))
        .limit(30)
        .orderBy(TIME, descending: true)
        .get();

    // QuerySnapshot shots2 = await FirebaseFirestore.instance
    //     .collection(USER_BASE)
    //     .where(GENDER, isEqualTo: userModel.getInt(PREFERENCE))
    //     .where(ETHNICITY, isEqualTo: userModel.getInt(ETHNICITY))
    //     .limit(30)
    //     .orderBy(TIME, descending: true)
    //     .get();

    List allShots = [];
    allShots.addAll(shots1.documents);
    // allShots.addAll(shots2.documents);

    double limit = appSettingsModel.getDouble(MILES_LIMIT);
    bool useLimit = appSettingsModel.getBoolean(USE_RADIUS_LIMIT);

    for (DocumentSnapshot doc in allShots) {
      BaseModel model = BaseModel(doc: doc);
      if (model.myItem()) continue;
      if (!model.signUpCompleted) continue;
      if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;

      if (model.getMap(POSITION).isEmpty) continue;
      /*  if (model.getMap(POSITION).isNotEmpty) {
        final geoPoint = model.getModel(POSITION).get("geopoint") as GeoPoint;
        final distance = (await calculateDistanceTo(geoPoint,) / 1000);
        if ((distance > limit)) continue;
        print("Distance $distance limit $limit");
        model.put(DISTANCE, distance);
      }*/

      int index = recommendedMatches
          .indexWhere((bm) => model.getObjectId() == bm.getObjectId());
      if (index == -1) {
        recommendedMatches.add(model);
      } else {
        recommendedMatches[index] = model;
      }
    }
    recommendedSetup = true;
    if (mounted) setState(() {});
  }

  String strockImage;
  var loadingSub;
  int genderType = -1;
  int onlineType = -1;
  int interestType = -1;
  int minAge = 18;
  int maxAge = 80;
  bool filtering = false;
  Timer strockImagesTimer;

  loadNearby() async {
    meetMeList.clear();
    Geoflutterfire geo = Geoflutterfire();
    Map myPosition = userModel.getMap(POSITION);
    if (myPosition.isEmpty) {
      return;
    }
    GeoPoint myGeoPoint = myPosition["geopoint"];
    double lat = myGeoPoint.latitude;
    double lon = myGeoPoint.longitude;
    GeoFirePoint center = geo.point(latitude: lat, longitude: lon);
    final collectionReference =
        FirebaseFirestore.instance.collection(USER_BASE);

    double radius = appSettingsModel.getDouble(NEARBY_RADIUS);
    double limit = appSettingsModel.getDouble(MILES_LIMIT);
    bool useLimit = appSettingsModel.getBoolean(USE_RADIUS_LIMIT);
    bool useNearby = appSettingsModel.getBoolean(USE_NEARBY_LIMIT);

    geo
        .collection(collectionRef: collectionReference)
        .within(center: center, radius: radius, field: POSITION)
        .first
        .then((event) async {
      for (DocumentSnapshot doc in event) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (!model.signUpCompleted) continue;
        if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
        if (model.getInt(GENDER) != userModel.getInt(PREFERENCE)) continue;
        final geoPoint = model.getModel(POSITION).get("geopoint") as GeoPoint;
        final distance =
            (await calculateDistanceTo(geoPoint, myGeoPoint) * 0.001);
        if (distance > 1000) continue;
        model.put(DISTANCE, distance);
        int index = meetMeList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (index == -1)
          meetMeList.add(model);
        else
          meetMeList[index] = model;
      }
      //loadingSub.cancel();
      meetMeSetup = true;
      if (mounted) setState(() {});
    });
  }

  chkUpdate() async {
    return;
    int version = appSettingsModel.getInt(VERSION_CODE);
    PackageInfo pack = await PackageInfo.fromPlatform();
    String v = pack.buildNumber;
    int myVersion = int.parse(v);
    if (myVersion < version) {
      pushAndResult(context, UpdateLayout(), opaque: false);
    }
  }

  handleMessage(var message) async {
    final dynamic data = Platform.isAndroid ? message['data'] : message;
    BaseModel model = BaseModel(items: data);
    String title = model.getString("title");
    String body = model.getString("message");

//    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
//        'strock.maugost.nt', 'Maugost', 'your channel description',
//        importance: Importance.Max, priority: Priority.High, ticker: 'ticker');
//    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
//    var platformChannelSpecifics = NotificationDetails(
//        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//    await notificationsPlugin.show(0, title, body, platformChannelSpecifics,
//        payload: 'item x');

    if (data != null) {
      String type = data[TYPE];
      String id = data[OBJECT_ID];
      if (type != null) {
        if (type == PUSH_TYPE_CHAT && visibleChatId != id) {
          pushAndResult(
              context,
              ChatMain(
                id,
                otherPerson: null,
              ));
        }
      }
    }
  }

  setUpLocation() async {
    getCurrentPosition(allowInterop((pos) => success(pos)));
  }

  success(pos) {
    Geoflutterfire geo = Geoflutterfire();
    double lat = 53.506764; //filterLocation.getDouble(LATITUDE);
    double lon = -2.192136; //filterLocation.getDouble(LONGITUDE);

    GeoFirePoint center1 = geo.point(latitude: lat, longitude: lon);

    GeoFirePoint center = geo.point(
        latitude: pos.coords.latitude, longitude: pos.coords.longitude);
    userModel
      ..put(POSITION, center.data)
      ..updateItems();
    loadNearby();

    Map myPosition = userModel.getMap(POSITION);
    GeoPoint geoPoint = myPosition["geopoint"];

    print("**center ${center.data}");
    print("**myPosition $myPosition --- empty ${myPosition.isEmpty}");
    print("**geoPoint ${geoPoint.latitude} ---- ${geoPoint.longitude}");
  }

  applyLocationFix() {
    FirebaseFirestore.instance
        .collection(USER_BASE)
        //.where(STATE_FILTER, isNull: true)
        .get()
        .then((value) async {
      print("fix started ");
      for (DocumentSnapshot doc in value.docs) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (!model.signUpCompleted) continue;
        if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
        if (model.getString(STATE_FILTER).isNotEmpty) continue;
        Map position = model.getMap(POSITION);
        if (position.isEmpty) continue;
        GeoPoint geo = position["geopoint"];
        double lat = geo.latitude;
        double lon = geo.longitude;
        String mStateFilter = await stateNameFromLat(lat, lon);
        print("mStateFilter $mStateFilter user ${model.getString(NAME)}");
        model.updateItems(delaySeconds: 2);
      }
      print("fix applied ");
    });
  }

  setupPush() async {
    fbMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        //handleMessage(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        handleMessage(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        handleMessage(message);
      },
    );
    fbMessaging.requestNotificationPermissions(const IosNotificationSettings(
        sound: true, badge: true, alert: true, provisional: false));
    fbMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    fbMessaging.subscribeToTopic('all');
    fbMessaging.subscribeToTopic(userModel.getObjectId());
    fbMessaging.getToken().then((String token) async {
      if (userModel.getString(TOKEN).isEmpty)
        userModel
          ..put(TOKEN, token)
          ..updateItems();
    });
  }

  void onPause() {
    if (userModel == null) return;
    int prevTimeOnline = userModel.getInt(TIME_ONLINE);
    int timeActive = (DateTime.now().millisecondsSinceEpoch) - timeOnline;
    int newTimeOnline = timeActive + prevTimeOnline;
    userModel.put(IS_ONLINE, false);
    userModel.put(TIME_ONLINE, newTimeOnline);
    userModel.updateItems();
    timeOnline = 0;
  }

  void onResume() async {
    if (userModel == null) return;
    timeOnline = DateTime.now().millisecondsSinceEpoch;
    userModel.put(IS_ONLINE, true);
    userModel.put(
        PLATFORM,
        platformISWeb
            ? WEB
            : Platform.isAndroid
                ? ANDROID
                : IOS);
    if (!userModel.getBoolean(NEW_APP)) {
      userModel.put(NEW_APP, true);
    }
    userModel.updateItems();

    Future.delayed(Duration(seconds: 2), () {
      // setUpLocation();
      checkLaunch();
    });
  }

  Future<void> checkLaunch() async {
    return;
    const platform = const MethodChannel("channel.john");
    try {
      Map response = await platform
          .invokeMethod('launch', <String, String>{'message': ""});
      int type = response[TYPE];
      String chatId = response[CHAT_ID];

//      toastInAndroid(type.toString());
//      toastInAndroid(chatId);

      if (type == LAUNCH_CHAT) {
        pushAndResult(
            context,
            ChatMain(
              chatId,
              otherPerson: null,
            ));
      }

      if (type == LAUNCH_REPORTS) {
        pushAndResult(context, ReportMain());
      }

      //toastInAndroid(response);
    } catch (e) {
      //toastInAndroid(e.toString());
      //batteryLevel = "Failed to get what he said: '${e.message}'.";
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    if (state == AppLifecycleState.paused) {
      onPause();
    }
    if (state == AppLifecycleState.resumed) {
      onResume();
    }

    super.didChangeAppLifecycleState(state);
  }

  List loadedIds = [];
  loadMessages() async {
    //var lock = Lock();
    // await lock.synchronized(() async {
    await Future.delayed(Duration(seconds: 10));
    var sub = FirebaseFirestore.instance
        .collection(CHAT_IDS_BASE)
        .where(PARTIES, arrayContains: userModel.getObjectId())
        .snapshots()
        .listen((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel chatIdModel = BaseModel(doc: doc);
        String chatId = chatIdModel.getObjectId();
        List partyModels = chatIdModel.getList(PARTIES_MODEL);
        List parties = chatIdModel.getList(PARTIES);

        if (userModel.getList(DELETED_CHATS).contains(chatId)) continue;
        if (loadedIds.contains(chatId)) {
          continue;
        }
        if (partyModels.isEmpty || partyModels.length != parties) {
          loadPartyUpdate(chatIdModel);
        }
        loadedIds.add(chatId);
        var sub = FirebaseFirestore.instance
            .collection(CHAT_BASE)
            .where(PARTIES, arrayContains: userModel.getUserId())
            .where(CHAT_ID, isEqualTo: chatId)
            .orderBy(TIME, descending: true)
            .limit(1)
            .snapshots()
            .listen((shots) async {
          if (shots.docs.isNotEmpty) {
            BaseModel cModel = BaseModel(doc: (shots.docs[0]));
            if (isBlocked(null, userId: getOtherPersonId(cModel))) {
              lastMessages.removeWhere(
                  (bm) => bm.getString(CHAT_ID) == cModel.getString(CHAT_ID));
              chatMessageController.add(true);
              return;
            }
          }
          if (stopListening.contains(chatId)) return;
          for (DocumentSnapshot doc in shots.docs) {
            if (!doc.exists) continue;
            BaseModel model = BaseModel(doc: doc);
            String chatId = model.getString(CHAT_ID);
            model.put(PARTIES_MODEL, partyModels);
            int index = lastMessages.indexWhere(
                (bm) => bm.getString(CHAT_ID) == model.getString(CHAT_ID));
            if (index == -1) {
              lastMessages.add(model);
            } else {
              lastMessages[index] = model;
            }

            if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
                !model.myItem() &&
                visibleChatId != model.getString(CHAT_ID)) {
              try {
                if (!showNewMessageDot.contains(chatId))
                  showNewMessageDot.add(chatId);
                if (mounted) setState(() {});
              } catch (E) {
                if (!showNewMessageDot.contains(chatId))
                  showNewMessageDot.add(chatId);
                if (mounted) setState(() {});
              }
              //countUnread(chatId);
            }
          }

          try {
            lastMessages
                .sort((bm1, bm2) => bm2.getTime().compareTo(bm1.getTime()));
          } catch (E) {}
        });

        subs.add(sub);
      }
      chatSetup = true;
      if (mounted) setState(() {});
    });
    subs.add(sub);
    // });
  }

  loadPartyUpdate(BaseModel chatIdModel) async {
    List partyModels = List.from(chatIdModel.getList(PARTIES_MODEL));
    List parties = chatIdModel.getList(PARTIES);

    for (int p = 0; p < parties.length; p++) {
      String userId = parties[p];
      FirebaseFirestore.instance
          .collection(USER_BASE)
          .doc(userId)
          .get()
          .then((value) {
        String chatId = chatIdModel.getObjectId();

        int index =
            lastMessages.indexWhere((bm) => bm.getString(CHAT_ID) == chatId);
        if (!value.exists) {
          lastMessages.removeWhere((bm) => bm.getString(CHAT_ID) == chatId);
          setState(() {});
          return;
        }

        BaseModel model = BaseModel(doc: value);
        int pp =
            partyModels.indexWhere((e) => e[OBJECT_ID] == model.getUserId());
        if (pp == -1)
          partyModels.add(chatPartyModel(model));
        else
          partyModels[p] == chatPartyModel(model);

        chatIdModel.put(PARTIES_MODEL, partyModels);
        chatIdModel.updateItems(delaySeconds: 1);

        if (index != -1) {
          BaseModel chatModel = lastMessages[index];
          chatModel.put(PARTIES_MODEL, partyModels);
          chatModel.updateItems(delaySeconds: 1);
        }
        if (mounted) setState(() {});
      });
    }
  }

  loadOtherPerson(String uId, {int delay = 0}) async {
    var lock = Lock();
    await lock.synchronized(() async {
      Future.delayed(Duration(seconds: delay), () async {
        if (uId.isEmpty) return;
        Firestore.instance
            .collection(USER_BASE)
            .document(uId)
            .get()
            .then((doc) {
          if (doc == null) return;
          if (!doc.exists) return;

          BaseModel user = BaseModel(doc: doc);
          otherPeronInfo[uId] = user;
          if (mounted) setState(() {});
        });
      });
    }, timeout: Duration(seconds: 10));
  }

  countUnread(String chatId) async {
    var lock = Lock();
    lock.synchronized(() async {
      int count = 0;
      print("Counting Unread: $chatId");
      QuerySnapshot shots = await FirebaseFirestore.instance
          .collection(CHAT_BASE)
          .where(CHAT_ID, isEqualTo: chatId)
          .limit(10)
          .orderBy(TIME, descending: true)
          .get();
      List list = [];
      for (DocumentSnapshot doc in shots.docs) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) break;
        if (chatRemoved(model)) continue;
        if (model.getUserId().isEmpty) continue;
        if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
            !model.myItem()) {
          list.add(model);
          count++;
        }
      }
      unreadCounter[chatId] = list;
      chatMessageController.add(true);
    });
  }

  loadNotification() async {
    var sub = Firestore.instance
        .collection(NOTIFY_BASE)
        .where(PARTIES, arrayContains: userModel.getUserId())
        .limit(1)
        .orderBy(TIME_UPDATED, descending: true)
        .snapshots()
        .listen((shots) {
      //toastInAndroid(shots.documents.length.toString());
      for (DocumentSnapshot d in shots.documents) {
        BaseModel model = BaseModel(doc: d);
        /*int p = nList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (p == -1) {
          nList.add(model);
        } else {
          nList[p] = model;
        }*/

        if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
            !model.myItem()) {
          showNewNotifyDot = true;
          if (mounted) setState(() {});
        }
      }
      /*nList.sort((bm1, bm2) =>
          bm2.getInt(TIME_UPDATED).compareTo(bm1.getInt(TIME_UPDATED)));*/
    });

    subs.add(sub);
    //notifySetup = true;
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return WillPopScope(
      onWillPop: () {
        //backThings();
        io.exit(0);
        return;
      },
      child: Scaffold(
        backgroundColor: AppConfig.appColor,
        body: LayoutBuilder(
          builder: (c, b) {
            final sw = screenWidth(context);
            if (sw <= 500) return mobileView();

            return WebHomeDesktop();
          },
        ),
      ),
    );
  }

  mobileView() {
    return SafeArea(
      child: Stack(
        children: [
          Container(
            color: AppConfig.appColor,
            height: MediaQuery.of(context).size.height * .3,
          ),
          page()
        ],
      ),
    );
  }

  topWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          color: showTop ? AppConfig.appColor : transparent,
//      height: 100,
          margin: EdgeInsets.only(bottom: 0),
          padding: EdgeInsets.only(top: 0, right: 15, left: 15, bottom: 20),
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            Row(
//        mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Flexible(
                  fit: FlexFit.tight,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text.rich(TextSpan(children: [
                        TextSpan(
                            text: "Hi ",
                            style: textStyle(true, showTop ? 20 : 30, black)),
                        TextSpan(
                            text: "${userModel.firstName},",
                            // text: "Maugost,",
                            style: textStyle(true, showTop ? 20 : 30, black))
                      ])),
                      Text("Meet awesome people.",
                          style: textStyle(true, showTop ? 15 : 20, black))
                    ],
                  ),
                ),
                userImageItem(
                  context,
                  userModel,
                  size: showTop ? 55 : 80,
                )
              ],
            ),
          ]),
        ),
        if (showTop) gradientLine(reverse: true)
      ],
    );
  }

  bool showTop = false;
  page() {
    return Container(
        child: ListView(
      key: const ValueKey('1232323homeList'),
      cacheExtent: 999999999999999.0,
      //physics: AlwaysScrollableScrollPhysics(),
      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
      children: [
        addSpace(40),
        topWidget(),
        Card(
          margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
          elevation: 5, shadowColor: black.withOpacity(.5),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10))),
          clipBehavior: Clip.antiAlias,
          //padding: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              addSpace(10),
              InkWell(
                onTap: () {
                  pushAndResult(context, MessagesPage(), result: (_) {
                    setState(() {});
                  });
                },
                child: Container(
                  // height: 100,
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onLongPress: () {
                          /* if (isAdmin)
                            pushAndResult(
                                context,
                                PaymentDetails(
                                  amount: 100.0,
                                  isAds: false,
                                  premiumIndex: 2,
                                ));*/
                        },
                        child: Container(
                            decoration: BoxDecoration(
                                color: white,
                                shape: BoxShape.circle,
                                border: Border.all(
                                    width: 2, color: black.withOpacity(.7))),
                            padding: EdgeInsets.all(10),
                            height: 60,
                            width: 60,
                            child: Image.asset(
                              "assets/icons/messages.png",
                              height: 40,
                              width: 40,
                            )),
                      ),
                      addSpaceWidth(10),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "My Messages",
                              style: textStyle(false, 18, black),
                            ),
                            if (lastMessages.isNotEmpty) addSpace(10),
                            if (lastMessages.isNotEmpty)
                              SingleChildScrollView(
                                padding: EdgeInsets.zero,
                                scrollDirection: Axis.horizontal,
                                child: Row(
                                  children: List.generate(
                                      lastMessages.length.clamp(0, 10), (p) {
                                    BaseModel model = lastMessages[p];

                                    String chatId = model.getString(CHAT_ID);
                                    //String chatId = model.getString(CHAT_ID);
                                    String otherPersonId =
                                        getOtherPersonId(model);
                                    List partyModels =
                                        model.getList(PARTIES_MODEL);
                                    Map otherPersonItem =
                                        otherParties.singleWhere(
                                            (e) =>
                                                e[OBJECT_ID] == otherPersonId,
                                            orElse: () => {});
                                    BaseModel otherPerson =
                                        BaseModel(items: otherPersonItem);

                                    String name = otherPerson.getString(NAME);
                                    String image =
                                        otherPerson.getString(IMAGE_URL);
                                    if (image.isEmpty)
                                      image = otherPerson.userImage;
                                    if (name.isEmpty) name = "No Name";

                                    return Container(
                                      height: 30,
                                      margin: EdgeInsets.only(right: 5),
                                      padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                      constraints:
                                          BoxConstraints(maxWidth: 150),
                                      decoration: BoxDecoration(
                                          color: default_white,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(25))),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Container(
                                            width: 30,
                                            height: 30,
                                            child: Card(
                                                color: black.withOpacity(.1),
                                                elevation: 0,
                                                clipBehavior: Clip.antiAlias,
                                                shape: CircleBorder(
                                                    side: BorderSide(
                                                        color: black
                                                            .withOpacity(.2),
                                                        width: .9)),
                                                child: Image.network(image,
                                                    fit: BoxFit.cover)),
                                          ),
                                          addSpaceWidth(5),
                                          if (model.getType() != CHAT_TYPE_TEXT)
                                            Icon(getChatIcon(model)),
                                          if (model.getType() != CHAT_TYPE_TEXT)
                                            addSpaceWidth(5),
                                          Flexible(
                                            fit: FlexFit.loose,
                                            child: Text(
                                              getChatMessage(model),
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          )
                                        ],
                                      ),
                                    );
                                  }),
                                ),
                              )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              if (!isAdmin && !userModel.isPremium) ...[
                addSpace(10),
                FlatButton(
                  onPressed: () {
                    openSubscriptionPage(context);
                  },
                  padding: EdgeInsets.all(15),
                  color: AppConfig.appColor,
                  child: Center(
                      child: Text(
                    "Go Premium",
                    style: textStyle(true, 18, black),
                  )),
                )
              ]
            ],
          ),
        ),
        Container(
          color: white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              addSpace(10),
              /*adsenseAdsView(),
                    addSpace(10),*/
              // youTubeBox(),
              addSpace(10),
              Container(
                padding:
                    EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
                alignment: Alignment.centerLeft,
                child: Text(
                  "See who's around you $token",
                  style: textStyle(true, 18, black),
                ),
              ),
              addSpace(20),
              Container(
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  children: [
                    Flexible(
                      child: Container(
                        height: 120,
                        child: MaterialButton(
                          onPressed: () {
                            pushAndResult(context, NearbySearch());
                          },
                          elevation: 5,
                          color: white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                          child: Stack(
                            fit: StackFit.expand,
                            children: [
                              Container(
                                margin: EdgeInsets.all(5),
                                width: double.infinity,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      "assets/icons/nearby.png",
                                      height: 40,
                                      width: 40,
                                    ),
                                    addSpace(10),
                                    Text(
                                      "NearBy",
                                      textAlign: TextAlign.center,
                                      style: textStyle(true, 15, black),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    addSpaceWidth(20),
                    Flexible(
                      child: Container(
                        height: 120,
                        child: MaterialButton(
                          onPressed: () {
                            pushAndResult(context, MeetMe(), depend: false);
                          },
                          elevation: 5,
                          color: white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                          child: Stack(
                            fit: StackFit.expand,
                            children: [
                              Container(
                                margin: EdgeInsets.all(5),
                                width: double.infinity,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      "assets/icons/heart1.png",
                                      height: 40,
                                      width: 40,
                                    ),
                                    addSpace(10),
                                    Text(
                                      "Meet Me",
                                      textAlign: TextAlign.center,
                                      style: textStyle(true, 15, black),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              addSpace(20),
              Container(
                height: 50,
                margin: EdgeInsets.only(right: 20, left: 20),
                child: MaterialButton(
                  onPressed: () {
                    if (isAdmin || userModel.isPremium) {
                      pushAndResult(context, ShowProfileViews(), result: (_) {
                        loadSeenBy();
                        if (mounted) setState(() {});
                      });
                      return;
                    }
                    showMessage(context, Icons.error, red0, "Opps Sorry!",
                        "You cannot view persons who viewed your profile until you become a Premium User",
                        textSize: 14,
                        clickYesText: "Subscribe", onClicked: (_) {
                      if (_) {
                        openSubscriptionPage(context);
                      }
                    }, clickNoText: "Cancel");
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  clipBehavior: Clip.antiAlias,
                  color: white,
                  padding: EdgeInsets.only(),
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Container(
                            padding: EdgeInsets.all(16),
                            child: Text(
                              seenByList.isEmpty
                                  ? "You have no Profile views yet"
                                  : "Your Profile was viewed by ${seenByList.length > 9 ? "9+" : seenByList.length} people",
                              style: textStyle(true, 16, black),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(16),
                          color: AppConfig.appColor,
                          child: Icon(
                            Icons.navigate_next,
                            color: white,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              addSpace(30),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      pushAndResult(context, ShowRecommendations());
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                        left: 20,
                        right: 20,
                      ),
                      alignment: Alignment.centerLeft,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Recommended Matches",
                            style: textStyle(true, 18, black),
                          ),
                          Text(
                            "View All",
                            style: textStyle(
                              false,
                              14,
                              black.withOpacity(.5),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  addSpace(10),
                  if (meetMeList.isNotEmpty)
                    SingleChildScrollView(
                      key: const ValueKey('1112322scrollRecom@'),
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: List.generate(
                            meetMeList.length > 15 ? 15 : meetMeList.length,
                            (p) {
                          BaseModel model = meetMeList[p];

                          String image = "";
                          bool isVideo = false;
                          if (model.profilePhotos.isNotEmpty) {
                            isVideo = model
                                .profilePhotos[model.getInt(DEF_PROFILE_PHOTO)]
                                .isVideo;
                            image = isVideo
                                ? model
                                    .profilePhotos[
                                        model.getInt(DEF_PROFILE_PHOTO)]
                                    .thumbnailUrl
                                : model
                                    .profilePhotos[
                                        model.getInt(DEF_PROFILE_PHOTO)]
                                    .imageUrl;
                          }

                          return Container(
                            width: 90,
                            margin: EdgeInsets.all(4),
//                            height: 90,
//                            width: 90,
                            //margin: EdgeInsets.all(6),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
//                                        userImageItem(context, model,
//                                            size: 80, padLeft: false),
                                Stack(
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: GestureDetector(
                                          onTap: () {
                                            pushAndResult(
                                                context,
                                                ShowProfile(
                                                  theUser: model,
                                                  fromMeetMe: false,
                                                ),
                                                depend: false);
                                          },
                                          child: Stack(
                                            children: [
                                              Container(
                                                  width: 100,
                                                  height: 100,
                                                  child: Center(
                                                      child: Icon(
                                                    Icons.person,
                                                    color: white,
                                                    size: 15,
                                                  )),
                                                  decoration: BoxDecoration(
                                                    color:
                                                        black.withOpacity(.09),
                                                    //shape: BoxShape.circle
                                                  )),
                                              Image.network(image,
                                                  width: 100,
                                                  height: 100,
                                                  fit: BoxFit.cover)
                                            ],
                                          )),
                                    ),
                                    if (isVideo)
                                      Container(
                                        width: 100,
                                        height: 100,
                                        child: Center(
                                          child: Container(
                                            height: 30,
                                            width: 30,
                                            child: Icon(
                                              Icons.play_arrow,
                                              color: Colors.white,
                                              size: 14,
                                            ),
                                            decoration: BoxDecoration(
                                                color: Colors.black
                                                    .withOpacity(0.8),
                                                border: Border.all(
                                                    color: Colors.white,
                                                    width: 1.5),
                                                shape: BoxShape.circle),
                                          ),
                                        ),
                                      ),
                                  ],
                                ),
                                addSpace(5),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    if (isOnline(model)) ...[
                                      Container(
                                        height: 8,
                                        width: 8,
                                        decoration: BoxDecoration(
                                            color: green,
                                            shape: BoxShape.circle),
                                      ),
                                      addSpaceWidth(2),
                                    ],
                                    Flexible(
                                      child: Text(
                                        getFirstName(model),
                                        style: textStyle(true, 14, black),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          );
                        }),
                      ),
                    )
                ],
              ),
              addSpace(50),
            ],
          ),
        )
      ],
    ));
  }

  backThings() {
    if (userModel != null && !userModel.getBoolean(HAS_RATED)) {
      showMessage(context, Icons.star, blue0, "Rate Us",
          "Enjoying the App? Please support us with 5 stars",
          clickYesText: "RATE APP", clickNoText: "Later", onClicked: (_) {
        if (_ == true) {
          if (appSettingsModel == null ||
              appSettingsModel.getString(PACKAGE_NAME).isEmpty) {
            onPause();
            Future.delayed(Duration(seconds: 1), () {
              io.exit(0);
            });
          } else {
            rateApp();
          }
        } else {
          onPause();
          Future.delayed(Duration(seconds: 1), () {
            io.exit(0);
          });
        }
      });
      return;
    }
    onPause();
    Future.delayed(Duration(seconds: 1), () {
      io.exit(0);
    });
  }

  loadStory() async {
    var storySub = Firestore.instance
        .collection(STORY_BASE)
        .where(GENDER, isEqualTo: userModel.isMale() ? FEMALE : MALE)
        .where(TIME,
            isGreaterThan: (DateTime.now().millisecondsSinceEpoch -
                (Duration.millisecondsPerDay * 2)))
        .snapshots()
        .listen((shots) {
      bool added = false;
      for (DocumentSnapshot shot in shots.documents) {
        if (!shot.exists) continue;
        BaseModel model = BaseModel(doc: shot);
        if (isBlocked(model)) continue;
        int index = allStoryList
            .indexWhere(((bm) => bm.getObjectId() == model.getObjectId()));
        if (index == -1) {
          allStoryList.add(model);
          added = true;
          if (!model.myItem() &&
              !model.getList(SHOWN).contains(userModel.getObjectId())) {
            if (!newStoryIds.contains(model.getObjectId()))
              newStoryIds.add(model.getObjectId());
          }
        } else {
          allStoryList[index] = model;
        }
      }
      homeRefreshController.add(true);
    });
    var myStorySub = Firestore.instance
        .collection(STORY_BASE)
        .where(USER_ID, isEqualTo: userModel.getObjectId())
        .snapshots()
        .listen((shots) {
      bool added = false;
      for (DocumentSnapshot shot in shots.documents) {
        if (!shot.exists) continue;
        BaseModel model = BaseModel(doc: shot);
        if (isBlocked(model)) continue;
        int index = allStoryList
            .indexWhere(((bm) => bm.getObjectId() == model.getObjectId()));
        if (index == -1) {
          allStoryList.add(model);
          added = true;
        } else {
          allStoryList[index] = model;
        }
      }
      homeRefreshController.add(true);
    });
    subs.add(storySub);
    subs.add(myStorySub);
  }

  loadBlocked() async {
    var lock = Lock();
    lock.synchronized(() async {
      QuerySnapshot shots = await Firestore.instance
          .collection(USER_BASE)
          .where(BLOCKED, arrayContains: userModel.getObjectId())
          .getDocuments();

      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        String uId = model.getObjectId();
        String deviceId = model.getString(DEVICE_ID);
        if (!blockedIds.contains(uId)) blockedIds.add(uId);
        if (deviceId.isNotEmpty) if (!blockedIds.contains(deviceId))
          blockedIds.add(deviceId);
      }
    }, timeout: Duration(seconds: 10));
  }

  saveStories(List models) async {
    if (models.isEmpty) {
      uploadingController.add("Uploading Successful");
      Future.delayed(Duration(seconds: 1), () {
        uploadingController.add(null);
      });
      return;
    }
    uploadingController.add("Uploading Story");

    BaseModel model = models[0];
    String image = model.getString(STORY_IMAGE);
    uploadFile(File(image), (res, error) {
      if (error != null) {
        saveStories(models);
        return;
      }
      model.put(STORY_IMAGE, res);
      model.saveItem(STORY_BASE, true);
      models.removeAt(0);
      saveStories(models);
    });
  }

  getStackedImages(List list) {
    List items = [];
    int count = 0;
    for (int i = 0; i < list.length; i++) {
      if (count > 10) break;
      BaseModel model = nearByList[i];
      items.add(Container(
        margin: EdgeInsets.only(left: double.parse((i * 20).toString())),
        child: userImageItem(context, model,
            size: 40, padLeft: false, type: "nah"),
      ));
      count++;
    }
    List<Widget> children = List.from(items.reversed);
    return IgnorePointer(
      ignoring: true,
      child: Container(
        height: 40,
        child: Stack(
          children: children,
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class UpdateLayout extends StatelessWidget {
  BuildContext con;
  @override
  Widget build(BuildContext context) {
    String features = appSettingsModel.getString(NEW_FEATURE);
    if (features.isNotEmpty) features = "* $features";
    bool mustUpdate = appSettingsModel.getBoolean(MUST_UPDATE);
    con = context;
    return WillPopScope(
      onWillPop: () {},
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                color: black.withOpacity(.6),
              )),
          Container(
            padding: EdgeInsets.all(15),
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      if (isAdmin) {
                        Navigator.pop(con);
                      }
                    },
                    child: Image.asset(
                      ic_plain,
                      width: 60,
                      height: 60,
                      color: white,
                    ),
                  ),
                  addSpace(10),
                  Text(
                    "New Update Available",
                    style: textStyle(true, 22, white),
                    textAlign: TextAlign.center,
                  ),
                  addSpace(10),
                  Text(
                    features.isEmpty
                        ? "Please update your App to proceed"
                        : features,
                    style: textStyle(false, 16, white.withOpacity(.5)),
                    textAlign: TextAlign.center,
                  ),
                  addSpace(15),
                  Container(
                    height: 40,
                    width: 120,
                    child: FlatButton(
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25)),
                        color: blue3,
                        onPressed: () {
                          String appLink =
                              appSettingsModel.getString(APP_LINK_IOS);
                          if (Platform.isAndroid)
                            appLink =
                                appSettingsModel.getString(APP_LINK_ANDROID);
                          openLink(appLink);
                        },
                        child: Text(
                          "UPDATE",
                          style: textStyle(true, 14, white),
                        )),
                  ),
                  addSpace(15),
                  if (!mustUpdate)
                    Container(
                      height: 40,
                      child: FlatButton(
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25)),
                          color: red0,
                          onPressed: () {
                            Navigator.pop(con);
                          },
                          child: Text(
                            "Later",
                            style: textStyle(true, 14, white),
                          )),
                    )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
