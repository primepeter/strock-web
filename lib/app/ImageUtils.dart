// import 'dart:html' as html;

import 'dart:html';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:firebase/firebase.dart' as fb;
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_picker_web_redux/image_picker_web_redux.dart';
// import 'package:image_picker_web/image_picker_web.dart';

class ImageUtils {
  firebase_storage.FirebaseStorage storage =
      firebase_storage.FirebaseStorage.instance;

  static Future<BaseModel> pickImageFromGallery() async {
    final imageFile = await ImagePickerWeb.getImageInfo;
    if (imageFile == null) return null;

    print("Data ${imageFile.data}");
    print("fileName ${imageFile.fileName}");

    BaseModel model = BaseModel();
    model.put(OBJECT_ID, imageFile.fileName.hashCode.toString());
    model.put(IMAGE_PATH, imageFile.data);
    model.put(FILE_NAME, imageFile.fileName);
    return model;
  }

  static uploadImageToFirebaseStore(
      MediaInfo info, onComplete(res, error)) async {
    final String id = getRandomId();

    /* String mimeType = mime(path.basename(info.name));
    final extension = extensionFromMime(mimeType);
    var metadata = fb.UploadMetadata(
      contentType: mimeType,
    );*/
    fb.StorageReference ref = fb
        .app()
        .storage()
        .refFromURL('gs://quickhooks-c4691.appspot.com')
        .child(id);
    // fb.UploadTask uploadTask = ref.put(info.data, metadata);
    fb.UploadTask uploadTask = ref.put(info);
    fb.UploadTaskSnapshot taskSnapshot = await uploadTask.future;
    taskSnapshot.ref.getDownloadURL().then((_) {
      print('url location $_');
      BaseModel model = new BaseModel();
      model.put(FILE_URL, _.toString());
      model.put(REFERENCE, ref.fullPath);
      model.saveItem(REFERENCE_BASE, false);
      onComplete(_.toString(), null);
    }, onError: (error) {
      onComplete(null, error);
    });
  }

  static uploadImageToFirebaseStoreFile(
      /*MediaInfo*/ File info,
      onComplete(res, error)) async {
    final String id = getRandomId();

    /* String mimeType = mime(path.basename(info.name));
    final extension = extensionFromMime(mimeType);
    var metadata = fb.UploadMetadata(
      contentType: mimeType,
    );*/
    fb.StorageReference ref = fb
        .app()
        .storage()
        .refFromURL('gs://quickhooks-c4691.appspot.com')
        .child(id);
    // fb.UploadTask uploadTask = ref.put(info.data, metadata);
    fb.UploadTask uploadTask = ref.put(info);
    fb.UploadTaskSnapshot taskSnapshot = await uploadTask.future;
    taskSnapshot.ref.getDownloadURL().then((_) {
      print('url location $_');
      BaseModel model = new BaseModel();
      model.put(FILE_URL, _.toString());
      model.put(REFERENCE, ref.fullPath);
      model.saveItem(REFERENCE_BASE, false);
      onComplete(_.toString(), null);
    }, onError: (error) {
      onComplete(null, error);
    });
  }

  static uploadImageToFirebaseStoreMod(
      PickedFile pickedFile, onComplete(res, error)) async {
    final bytes = await pickedFile.readAsBytes();
    String randFile = DateTime.now().millisecondsSinceEpoch.toString() + "pick";
    File blob = File(bytes, randFile);
    final String id = getRandomId();
    firebase_storage.Reference ref =
        firebase_storage.FirebaseStorage.instance.ref(id);
    firebase_storage.UploadTask uploadTask = ref.putBlob(blob);
    uploadTask.then((task) {
      if (task != null) {
        task.ref.getDownloadURL().then((_) {
          print('url location $_');
          // print(ref.);
          BaseModel model = new BaseModel();
          model.put(FILE_URL, _.toString());
          model.put(REFERENCE, ref.fullPath);
          model.saveItem(REFERENCE_BASE, false);
          onComplete(_.toString(), null);
        }, onError: (error) {
          onComplete(null, error);
        });
      }
    }, onError: (error) {
      onComplete(null, error);
    });
  }

  static void uploadImage({@required Function(File file) onSelected}) {
    InputElement uploadInput = FileUploadInputElement()..accept = 'image/*';
    uploadInput.click();

    uploadInput.onChange.listen((event) {
      final file = uploadInput.files.first;
      final reader = FileReader();
      reader.readAsDataUrl(file);
      reader.onLoadEnd.listen((event) {
        onSelected(file);
      });
    });
  }

//   var getFileBlob = function (url, cb) {
//   var xhr = new XMLHttpRequest();
//   xhr.open("GET", url);
//   xhr.responseType = "blob";
//   xhr.addEventListener('load', function() {
//     cb(xhr.response);
//   });
//   xhr.send();
// };
}
