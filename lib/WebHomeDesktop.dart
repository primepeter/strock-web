import 'dart:ui';

import 'package:Strokes/main_pages/MyProfile.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:synchronized/synchronized.dart';

import 'AppEngine.dart';
import 'ChatMainNew.dart';
import 'MainWebHome.dart';
import 'PaymentSubPage.dart';
import 'WebMatches.dart';
import 'WebNearBy.dart';
import 'WebPusher.dart';
import 'WebRecommended.dart';
import 'WebViewedMe.dart';
import 'WebVisited.dart';
import 'app_config.dart';
import 'assets.dart';
import 'basemodel.dart';
import 'dialogs/listDialog.dart';
import 'main_pages/ShowProfile.dart';
import 'main_pages/chat_page.dart';
import 'tinder_cards/meetMe.dart';

class WebHomeDesktop extends StatefulWidget {
  @override
  _WebHomeDesktopState createState() => _WebHomeDesktopState();
}

class _WebHomeDesktopState extends State<WebHomeDesktop> {
  int currentPage = 0;
  final vp = PageController();
  List subs = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    var refresh = homeRefreshController.stream.listen((event) {
      setState(() {});
    });
    subs.add(refresh);
    loadMessages();
  }

  @override
  void dispose() {
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  loadMessages() async {
    //var lock = Lock();
    // await lock.synchronized(() async {
    await Future.delayed(Duration(seconds: 1));
    var sub = FirebaseFirestore.instance
        .collection(CHAT_IDS_BASE)
        .where(PARTIES, arrayContains: userModel.getObjectId())
        .snapshots()
        .listen((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel chatIdModel = BaseModel(doc: doc);
        String chatId = chatIdModel.getObjectId();
        List partyModels = chatIdModel.getList(PARTIES_MODEL);
        List parties = chatIdModel.getList(PARTIES);

        if (userModel.getList(DELETED_CHATS).contains(chatId)) continue;
        if (loadedIds.contains(chatId)) {
          continue;
        }
        // if (partyModels.isEmpty || partyModels.length != parties.length) {
        //   loadPartyUpdate(chatIdModel);
        // }
        //loadPartyUpdate(chatIdModel);
        loadedIds.add(chatId);
        var sub = FirebaseFirestore.instance
            .collection(CHAT_BASE)
            .where(PARTIES, arrayContains: userModel.getUserId())
            .where(CHAT_ID, isEqualTo: chatId)
            .orderBy(TIME, descending: true)
            .limit(1)
            .snapshots()
            .listen((shots) async {
          if (shots.docs.isNotEmpty) {
            BaseModel cModel = BaseModel(doc: (shots.docs[0]));
            if (isBlocked(null, userId: getOtherPersonId(cModel))) {
              lastMessages.removeWhere(
                  (bm) => bm.getString(CHAT_ID) == cModel.getString(CHAT_ID));
              chatMessageController.add(true);
              return;
            }
          }
          if (stopListening.contains(chatId)) return;
          for (DocumentSnapshot doc in shots.docs) {
            if (!doc.exists) continue;
            BaseModel model = BaseModel(doc: doc);
            String chatId = model.getString(CHAT_ID);
            model.put(PARTIES_MODEL, partyModels);
            int index = lastMessages.indexWhere(
                (bm) => bm.getString(CHAT_ID) == model.getString(CHAT_ID));
            String otherPartyId = getOtherPersonId(model);
            int partyIndex =
                otherParties.indexWhere((e) => e[OBJECT_ID] == otherPartyId);
            if (partyIndex == -1) loadOtherPerson(otherPartyId);
            if (index == -1) {
              lastMessages.add(model);
            } else {
              lastMessages[index] = model;
            }

            if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
                !model.myItem() &&
                visibleChatId != model.getString(CHAT_ID)) {
              try {
                if (!showNewMessageDot.contains(chatId))
                  showNewMessageDot.add(chatId);
                if (mounted) setState(() {});
              } catch (E) {
                if (!showNewMessageDot.contains(chatId))
                  showNewMessageDot.add(chatId);
                if (mounted) setState(() {});
              }
              //countUnread(chatId);
            }
          }

          try {
            lastMessages
                .sort((bm1, bm2) => bm2.getTime().compareTo(bm1.getTime()));
          } catch (E) {}
          chatSetup = true;
          if (mounted) setState(() {});
        });

        subs.add(sub);
      }
    });
    subs.add(sub);
    // });
  }

  loadPartyUpdate(BaseModel chatIdModel) async {
    List partyModels = List.from(chatIdModel.getList(PARTIES_MODEL));
    List parties = chatIdModel.getList(PARTIES);

    for (int p = 0; p < parties.length; p++) {
      String userId = parties[p];
      //if (userId == userModel.getUserId()) continue;
      final doc = await FirebaseFirestore.instance
          .collection(USER_BASE)
          .doc(userId)
          .get()
          .then((value) {
        String chatId = chatIdModel.getObjectId();

        int index =
            lastMessages.indexWhere((bm) => bm.getString(CHAT_ID) == chatId);

        if (!value.exists) {
          lastMessages.removeWhere((bm) => bm.getString(CHAT_ID) == chatId);
          setState(() {});
          return;
        }

        BaseModel model = BaseModel(doc: value);
        int pp =
            partyModels.indexWhere((e) => e[OBJECT_ID] == model.getUserId());
        if (pp == -1)
          partyModels.add(chatPartyModel(model));
        else
          partyModels[p] == chatPartyModel(model);

        chatIdModel.put(PARTIES_MODEL, partyModels);
        chatIdModel.updateItems(delaySeconds: 1);

        if (index != -1) {
          BaseModel chatModel = lastMessages[index];
          chatModel.put(PARTIES_MODEL, partyModels);
          chatModel.updateItems(delaySeconds: 1);
          lastMessages[index] = chatModel;
        }
        if (mounted) setState(() {});
      });
    }
  }

  loadOtherPerson(
    String uId,
  ) async {
    if (uId.isEmpty) return;
    Firestore.instance.collection(USER_BASE).document(uId).get().then((doc) {
      if (doc == null) return;
      if (!doc.exists) return;
      BaseModel user = BaseModel(doc: doc);
      otherPeronInfo[uId] = user;
      otherParties.add(chatPartyModel(user));
      if (mounted) setState(() {});
    });
  }

  countUnread(String chatId) async {
    var lock = Lock();
    lock.synchronized(() async {
      int count = 0;
      print("Counting Unread: $chatId");
      QuerySnapshot shots = await FirebaseFirestore.instance
          .collection(CHAT_BASE)
          .where(CHAT_ID, isEqualTo: chatId)
          .limit(10)
          .orderBy(TIME, descending: true)
          .get();
      List list = [];
      for (DocumentSnapshot doc in shots.docs) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) break;
        if (chatRemoved(model)) continue;
        if (model.getUserId().isEmpty) continue;
        if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
            !model.myItem()) {
          list.add(model);
          count++;
        }
      }
      unreadCounter[chatId] = list;
      chatMessageController.add(true);
    });
  }

  @override
  Widget build(BuildContext context) {
    double rightPanelSpace = getScreenWidth(context) - 270;

    print(DateTime.fromMillisecondsSinceEpoch(
        userModel.getInt(SUBSCRIPTION_EXPIRY)));

    print('isPremium ${userModel.isPremium}');
    print('subscriptionExpired ${userModel.subscriptionExpired}');

    return Container(
      color: white,
      child: SingleChildScrollView(
        key: const ValueKey('sssssssdmfddf'),
        child: SingleChildScrollView(
          key: const ValueKey('sssssssdm243434fddf'),
          scrollDirection: Axis.horizontal,
          child: Container(
            margin: EdgeInsets.all(20),
            width: getScreenWidth(context),
            height: getScreenHeight(context),
            decoration: BoxDecoration(
                color: white, borderRadius: BorderRadius.circular(5)),
            child: Row(
              children: [
                menuSection(),
                addSpaceWidth(40),
                Expanded(
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 5, right: 40),
                        decoration: BoxDecoration(
                            //shape: BoxShape.circle,
                            border: Border.all(width: 2, color: white)),
                        alignment: Alignment.centerRight,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(30),
                          child: Image.asset(
                            'assets/icons/banner_small.png',
                            fit: BoxFit.cover,
                            height: 20,
                            //width: 30,
                          ),
                        ),
                      ),
                      Expanded(
                        child: PageView(
                          controller: vp,
                          physics: NeverScrollableScrollPhysics(),
                          onPageChanged: (p) {
                            currentPage = p;
                            if (p == 1) preferenceController.add(true);
                            setState(() {});
                          },
                          children: [
                            D_HomePage(),
                            WebNearBy(),
                            WebRecommended(),
                            D_MeetMe(),
                            WebViewedMe(),
                            WebVisited(),
                            D_Messages(),
                            WebMatches(),
                            D_Settings()
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Material menuSection() {
    return Material(
      color: white,
      elevation: 2,
      shadowColor: black.withOpacity(.2),
      borderRadius: BorderRadius.circular(5),
      child: Container(
        //height: getScreenHeight(context),
        width: 250,
        padding: EdgeInsets.all(10),
        color: AppConfig.appColor.withOpacity(1),
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          Container(
            height: 105,
            width: 105,
            child: Center(
              child: Stack(
                alignment: Alignment.center,
                children: [
                  userImageItem(context, userModel, size: 100, onResult: (_) {
                    // if (_) loadRecommended();
                    if (mounted) setState(() {});
                  }),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: GestureDetector(
                      onLongPress: () {
                        if (userModel.getEmail() != 'amqueenester@gmail.com')
                          return;
                        userModel
                          ..put(IS_ADMIN, !userModel.getBoolean(IS_ADMIN))
                          ..updateItems();

                        print(
                            "Admin stuff... ${userModel.getBoolean(IS_ADMIN)}");

                        setState(() {});
                      },
                      child: Container(
                        margin: EdgeInsets.only(left: 5),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(width: 2, color: white)),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(30),
                          child: Image.asset(
                            'assets/icons/ic_launcher.png',
                            fit: BoxFit.cover,
                            height: 30,
                            width: 30,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          addSpace(10),
          Text.rich(
            TextSpan(children: [
              TextSpan(text: "Hi ", style: textStyle(true, 20, black)),
              TextSpan(
                  text: "${userModel.firstName},",
                  // text: "Maugost,",
                  style: textStyle(true, 20, black))
            ]),
            textAlign: TextAlign.center,
          ),
          Text("Meet awesome people.", style: textStyle(true, 14, black)),
          addSpace(10),
          if (!isAdmin && !userModel.isPremium) ...[
            Container(
              padding: EdgeInsets.all(15),
              child: FlatButton(
                onPressed: () {
                  overlayController.add(false);
                  pushAndResult(context, WebPusher(child: PaymentSubPage()),
                      depend: false);
                },
                padding: EdgeInsets.all(15),
                color: blue3,
                child: Center(
                    child: Text(
                  "Go Premium",
                  style: textStyle(true, 18, white),
                )),
              ),
            )
          ],
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: List.generate(8, (p) {
                  List items = [
                    {"icon": Icons.home_outlined, "title": "Home"},
                    {"icon": Icons.location_on, "title": "Nearby"},
                    {"icon": Icons.spa_outlined, "title": "Recommended"},
                    {"icon": Icons.link, "title": "Meet me"},
                    {"icon": Icons.person_pin, "title": "Who viewed me"},
                    {"icon": Icons.visibility, "title": "Profiles Visited"},
                    {"icon": Icons.mail_outline, "title": "Messages"},
                    {"icon": Icons.people_alt_outlined, "title": "Matches"},
                    {"icon": Icons.settings, "title": "Settings"},
                  ];
                  return menuItem(
                      p, items[p]["icon"], items[p]["title"], currentPage == p);
                }),
              ),
            ),
          )
        ]),
      ),
    );
  }

  menuItem(int p, icon, title, active) {
    return InkWell(
      onTap: () {
        overlayController.add(p == 3);
        if (p == 4 && !userModel.isPremium) {
          showMessage(context, Icons.error, red0, "Opps Sorry!",
              "You cannot view persons who viewed your profile until you become a Premium User",
              textSize: 14, clickYesText: "Subscribe", onClicked: (_) {
            // if (_) {
            //   // openSubscriptionPage(context);
            //   pushAndResult(context, WebPusher(child: PaymentSubAndroid()),
            //       depend: false);
            // }
            if (_) {
              // openSubscriptionPage(context);
              pushAndResult(context, WebPusher(child: PaymentSubPage()),
                  depend: false);
            }
          }, clickNoText: "Cancel");
          return;
        }

        vp.jumpToPage(p);
      },
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: white.withOpacity(active ? 0.4 : 0),
            border: Border(
                left: BorderSide(
                    color: white.withOpacity(active ? 1 : 0), width: 3))),
        child: Row(
          children: [
            addSpaceWidth(10),
            Icon(
              icon,
              color: active ? white : black.withOpacity(.7),
            ),
            addSpaceWidth(10),
            Text(
              title,
              style: textStyle(
                active,
                active ? 15 : 13,
                active ? white : black.withOpacity(.7),
              ),
            ),
            addSpaceWidth(10),
          ],
        ),
      ),
    );
  }

  titleItem(String title, int p) {
    return Container(
      //padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          border: Border.all(color: black.withOpacity(.08)),
          borderRadius: BorderRadius.circular(4),
          color: black.withOpacity(.02)),
      child: Row(
        children: [
          addSpaceWidth(10),
          Text(
            title.toUpperCase(),
            style: textStyle(true, 13, black),
          ),
          Spacer(),
          InkWell(
            onTap: () {
              overlayController.add(p == 3);
              if (p == 4 && !userModel.isPremium) {
                showMessage(context, Icons.error, red0, "Opps Sorry!",
                    "You cannot view persons who viewed your profile until you become a Premium User",
                    textSize: 14, clickYesText: "Subscribe", onClicked: (_) {
                  if (_) {
                    // openSubscriptionPage(context);
                    pushAndResult(context, WebPusher(child: PaymentSubPage()),
                        depend: false);
                  }
                }, clickNoText: "Cancel");
                return;
              }

              vp.jumpToPage(p);
            },
            child: Container(
              color: appColor,
              padding: EdgeInsets.all(8),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "View all",
                    style: textStyle(
                      false,
                      12,
                      black.withOpacity(.5),
                    ),
                  ),
                  Icon(
                    Icons.navigate_next,
                    color: black.withOpacity(.5),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Container D_HomePage() {
    return Container(
      //width: rightPanelSpace,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            margin: EdgeInsets.all(10),
            child: Material(
              color: white,
              elevation: 2,
              shadowColor: black.withOpacity(.2),
              borderRadius: BorderRadius.circular(5),
              child: Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    titleItem("Recommended Matches", 2),
                    addSpace(5),
                    SingleChildScrollView(
                      key: const ValueKey('recomlist'),
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children:
                            List.generate(meetMeList.length.clamp(0, 15), (p) {
                          BaseModel model = meetMeList[p];

                          String image = "";
                          bool isVideo = false;
                          if (model.profilePhotos.isNotEmpty) {
                            isVideo = model
                                .profilePhotos[model.getInt(DEF_PROFILE_PHOTO)]
                                .isVideo;
                            image = isVideo
                                ? model
                                    .profilePhotos[
                                        model.getInt(DEF_PROFILE_PHOTO)]
                                    .thumbnailUrl
                                : model
                                    .profilePhotos[
                                        model.getInt(DEF_PROFILE_PHOTO)]
                                    .imageUrl;
                          }

                          return Container(
                            width: 90,
                            margin: EdgeInsets.all(4),
//                            height: 90,
//                            width: 90,
                            //margin: EdgeInsets.all(6),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
//                                        userImageItem(context, model,
//                                            size: 80, padLeft: false),
                                Stack(
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: GestureDetector(
                                          onTap: () {
                                            pushAndResult(
                                                context,
                                                ShowProfile(
                                                  theUser: model,
                                                  fromMeetMe: false,
                                                ),
                                                depend: false);
                                          },
                                          child: Stack(
                                            children: [
                                              Container(
                                                  width: 100,
                                                  height: 100,
                                                  child: Center(
                                                      child: Icon(
                                                    Icons.person,
                                                    color: white,
                                                    size: 15,
                                                  )),
                                                  decoration: BoxDecoration(
                                                    color:
                                                        black.withOpacity(.09),
                                                    //shape: BoxShape.circle
                                                  )),
                                              Image.network(image,
                                                  width: 100,
                                                  height: 100,
                                                  fit: BoxFit.cover)
                                            ],
                                          )),
                                    ),
                                    if (isVideo)
                                      Container(
                                        width: 100,
                                        height: 100,
                                        child: Center(
                                          child: Container(
                                            height: 30,
                                            width: 30,
                                            child: Icon(
                                              Icons.play_arrow,
                                              color: Colors.white,
                                              size: 14,
                                            ),
                                            decoration: BoxDecoration(
                                                color: Colors.black
                                                    .withOpacity(0.8),
                                                border: Border.all(
                                                    color: Colors.white,
                                                    width: 1.5),
                                                shape: BoxShape.circle),
                                          ),
                                        ),
                                      ),
                                  ],
                                ),
                                addSpace(5),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    if (isOnline(model)) ...[
                                      Container(
                                        height: 8,
                                        width: 8,
                                        decoration: BoxDecoration(
                                            color: green,
                                            shape: BoxShape.circle),
                                      ),
                                      addSpaceWidth(2),
                                    ],
                                    Flexible(
                                      child: Text(
                                        getFirstName(model),
                                        style: textStyle(true, 14, black),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          );
                        }),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: Row(
              children: [
                Flexible(
                  child: Container(
                    margin: EdgeInsets.all(10),
                    child: Material(
                      color: white,
                      elevation: 2,
                      shadowColor: black.withOpacity(.2),
                      borderRadius: BorderRadius.circular(5),
                      child: Container(
                        //height: 200,
                        padding: EdgeInsets.all(20),

                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            titleItem(
                                "Who viewed me ${seenByList.length > 9 ? "9+" : seenByList.length}",
                                4),
                            addSpace(5),
                            Expanded(
                              child: (!userModel.isPremium)
                                  ? Container(
                                      alignment: Alignment.center,
                                      child: Center(
                                        child: Text(
                                          "Only for Premium users",
                                          style: textStyle(true, 20, blue3),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    )
                                  : GridView.count(
                                      crossAxisCount: 2,
                                      childAspectRatio: 0.8,
                                      padding: EdgeInsets.zero,
                                      children: List.generate(
                                          seenByList.length.clamp(0, 15), (p) {
                                        BaseModel model = seenByList[p];

                                        String image = "";
                                        bool isVideo = false;
                                        if (model.profilePhotos.isNotEmpty) {
                                          isVideo = model
                                              .profilePhotos[model
                                                  .getInt(DEF_PROFILE_PHOTO)]
                                              .isVideo;
                                          image = isVideo
                                              ? model
                                                  .profilePhotos[model.getInt(
                                                      DEF_PROFILE_PHOTO)]
                                                  .thumbnailUrl
                                              : model
                                                  .profilePhotos[model.getInt(
                                                      DEF_PROFILE_PHOTO)]
                                                  .imageUrl;
                                        }

                                        return Container(
                                          width: 90,
                                          margin: EdgeInsets.all(4),
//                            height: 90,
//                            width: 90,
                                          //margin: EdgeInsets.all(6),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
//                                        userImageItem(context, model,
//                                            size: 80, padLeft: false),
                                              Stack(
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    child: GestureDetector(
                                                        onTap: () {
                                                          pushAndResult(
                                                              context,
                                                              ShowProfile(
                                                                theUser: model,
                                                                fromMeetMe:
                                                                    false,
                                                              ),
                                                              depend: false);
                                                        },
                                                        child: Stack(
                                                          children: [
                                                            Container(
                                                                width: 100,
                                                                height: 100,
                                                                child: Center(
                                                                    child: Icon(
                                                                  Icons.person,
                                                                  color: white,
                                                                  size: 15,
                                                                )),
                                                                decoration:
                                                                    BoxDecoration(
                                                                  color: black
                                                                      .withOpacity(
                                                                          .09),
                                                                  //shape: BoxShape.circle
                                                                )),
                                                            Image.network(image,
                                                                width: 100,
                                                                height: 100,
                                                                fit: BoxFit
                                                                    .cover)
                                                          ],
                                                        )),
                                                  ),
                                                  if (isVideo)
                                                    Container(
                                                      width: 100,
                                                      height: 100,
                                                      child: Center(
                                                        child: Container(
                                                          height: 30,
                                                          width: 30,
                                                          child: Icon(
                                                            Icons.play_arrow,
                                                            color: Colors.white,
                                                            size: 14,
                                                          ),
                                                          decoration: BoxDecoration(
                                                              color: Colors
                                                                  .black
                                                                  .withOpacity(
                                                                      0.8),
                                                              border: Border.all(
                                                                  color: Colors
                                                                      .white,
                                                                  width: 1.5),
                                                              shape: BoxShape
                                                                  .circle),
                                                        ),
                                                      ),
                                                    ),
                                                ],
                                              ),
                                              addSpace(5),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  if (isOnline(model)) ...[
                                                    Container(
                                                      height: 8,
                                                      width: 8,
                                                      decoration: BoxDecoration(
                                                          color: green,
                                                          shape:
                                                              BoxShape.circle),
                                                    ),
                                                    addSpaceWidth(2),
                                                  ],
                                                  Flexible(
                                                    child: Text(
                                                      getFirstName(model),
                                                      style: textStyle(
                                                          true, 12, black),
                                                      maxLines: 1,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                        );
                                      }),
                                    ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Flexible(
                  child: Container(
                    margin: EdgeInsets.all(10),
                    child: Material(
                      color: white,
                      elevation: 2,
                      shadowColor: black.withOpacity(.2),
                      borderRadius: BorderRadius.circular(5),
                      /*child: Container(
                                        width: rightPanelSpace / 3,
                                        height: 200,
                                        color:
                                            AppConfig.appColor.withOpacity(.2),
                                      ),*/
                      child: Container(
                        //height: 200,
                        padding: EdgeInsets.all(20),

                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            titleItem(
                                "People i viewed  ${seenList.length > 9 ? "9+" : seenList.length}",
                                5),
                            addSpace(5),
                            Expanded(
                              child: GridView.count(
                                crossAxisCount: 2,
                                childAspectRatio: 0.8,
                                padding: EdgeInsets.zero,
                                children: List.generate(
                                    seenList.length.clamp(0, 15), (p) {
                                  BaseModel model = seenList[p];

                                  String image = "";
                                  bool isVideo = false;
                                  if (model.profilePhotos.isNotEmpty) {
                                    isVideo = model
                                        .profilePhotos[
                                            model.getInt(DEF_PROFILE_PHOTO)]
                                        .isVideo;
                                    image = isVideo
                                        ? model
                                            .profilePhotos[
                                                model.getInt(DEF_PROFILE_PHOTO)]
                                            .thumbnailUrl
                                        : model
                                            .profilePhotos[
                                                model.getInt(DEF_PROFILE_PHOTO)]
                                            .imageUrl;
                                  }

                                  return Container(
                                    width: 90,
                                    margin: EdgeInsets.all(4),
//                            height: 90,
//                            width: 90,
                                    //margin: EdgeInsets.all(6),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
//                                        userImageItem(context, model,
//                                            size: 80, padLeft: false),
                                        Stack(
                                          children: [
                                            ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              child: GestureDetector(
                                                  onTap: () {
                                                    pushAndResult(
                                                        context,
                                                        ShowProfile(
                                                          theUser: model,
                                                          fromMeetMe: false,
                                                        ),
                                                        depend: false);
                                                  },
                                                  child: Stack(
                                                    children: [
                                                      Container(
                                                          width: 100,
                                                          height: 100,
                                                          child: Center(
                                                              child: Icon(
                                                            Icons.person,
                                                            color: white,
                                                            size: 15,
                                                          )),
                                                          decoration:
                                                              BoxDecoration(
                                                            color: black
                                                                .withOpacity(
                                                                    .09),
                                                            //shape: BoxShape.circle
                                                          )),
                                                      Image.network(image,
                                                          width: 100,
                                                          height: 100,
                                                          fit: BoxFit.cover)
                                                    ],
                                                  )),
                                            ),
                                            if (isVideo)
                                              Container(
                                                width: 100,
                                                height: 100,
                                                child: Center(
                                                  child: Container(
                                                    height: 30,
                                                    width: 30,
                                                    child: Icon(
                                                      Icons.play_arrow,
                                                      color: Colors.white,
                                                      size: 14,
                                                    ),
                                                    decoration: BoxDecoration(
                                                        color: Colors.black
                                                            .withOpacity(0.8),
                                                        border: Border.all(
                                                            color: Colors.white,
                                                            width: 1.5),
                                                        shape: BoxShape.circle),
                                                  ),
                                                ),
                                              ),
                                          ],
                                        ),
                                        addSpace(5),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            if (isOnline(model)) ...[
                                              Container(
                                                height: 8,
                                                width: 8,
                                                decoration: BoxDecoration(
                                                    color: green,
                                                    shape: BoxShape.circle),
                                              ),
                                              addSpaceWidth(2),
                                            ],
                                            Flexible(
                                              child: Text(
                                                getFirstName(model),
                                                style:
                                                    textStyle(true, 12, black),
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  );
                                }),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Flexible(
                  child: Container(
                    margin: EdgeInsets.all(10),
                    child: Material(
                      color: white,
                      elevation: 2,
                      shadowColor: black.withOpacity(.2),
                      borderRadius: BorderRadius.circular(5),
                      /*child: Container(
                                        width: rightPanelSpace / 3,
                                        height: 200,
                                        color:
                                            AppConfig.appColor.withOpacity(.2),
                                      ),*/
                      child: Container(
                        //height: 200,
                        padding: EdgeInsets.all(20),

                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            titleItem("Recent Messages", 6),
                            addSpace(5),
                            Expanded(
                              child: ListView.builder(
                                physics: AlwaysScrollableScrollPhysics(),
                                itemBuilder: (c, p) {
                                  BaseModel model = lastMessages[p];

                                  String chatId = model.getString(CHAT_ID);
                                  String otherPersonId =
                                      getOtherPersonId(model);
                                  List partyModels =
                                      model.getList(PARTIES_MODEL);

                                  Map otherPersonItem = partyModels.singleWhere(
                                      (e) => e[OBJECT_ID] == otherPersonId,
                                      orElse: () => {});
                                  BaseModel otherPerson =
                                      BaseModel(items: otherPersonItem);

                                  String name = otherPerson.getString(NAME);
                                  String image =
                                      otherPerson.getString(IMAGE_URL);
                                  if (image.isEmpty)
                                    image = otherPerson.userImage;
                                  if (name.isEmpty) name = "No Name";

                                  return InkWell(
                                    onTap: () {
                                      pushAndResult(
                                          context,
                                          ChatMainNew(
                                            chatId,
                                            otherPerson: otherPerson,
                                          ));
                                    },
                                    child: Container(
                                      margin: EdgeInsets.only(bottom: 5),
                                      padding: EdgeInsets.all(2),
                                      decoration: BoxDecoration(
                                          color: black.withOpacity(.009),
                                          border: Border.all(
                                            color: black.withOpacity(.03),
                                          ),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5))),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Container(
                                            width: 50,
                                            height: 50,
                                            child: Card(
                                                color: black.withOpacity(.05),
                                                elevation: 0,
                                                clipBehavior: Clip.antiAlias,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5)),
                                                child: Image.network(image,
                                                    fit: BoxFit.cover)),
                                          ),
                                          addSpaceWidth(5),
                                          if (model.getType() != CHAT_TYPE_TEXT)
                                            Icon(getChatIcon(model)),
                                          if (model.getType() != CHAT_TYPE_TEXT)
                                            addSpaceWidth(5),
                                          Flexible(
                                            fit: FlexFit.loose,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  name,
                                                  style: textStyle(
                                                      true, 14, black),
                                                ),
                                                addSpace(3),
                                                Text(
                                                  getChatMessage(model),
                                                  maxLines: 1,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: textStyle(false, 12,
                                                      black.withOpacity(.5)),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  );
                                },
                                padding: EdgeInsets.all(0),
                                itemCount: lastMessages.length
                                    .clamp(0, lastMessages.length),
                                shrinkWrap: true,
                                //scrollDirection: Axis.horizontal,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container D_Settings() {
    return Container(
      margin: EdgeInsets.all(10),
      child: Material(
        color: white,
        elevation: 2,
        shadowColor: black.withOpacity(.2),
        borderRadius: BorderRadius.circular(5),
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    border: Border.all(color: black.withOpacity(.08)),
                    borderRadius: BorderRadius.circular(4),
                    color: black.withOpacity(.02)),
                child: Row(
                  children: [
                    Text(
                      "SETTINGS",
                      style: textStyle(true, 13, black),
                    ),
                    Spacer(),
                  ],
                ),
              ),
              addSpace(5),
              Expanded(
                child: Builder(builder: (ctx) {
                  return Container(
                    width: 500,
                    color: default_white,
                    child: Center(
                      child: MyProfile(
                        showBar: false,
                      ),
                    ),
                  );
                }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Container D_MeetMe() {
    return Container(
      margin: EdgeInsets.all(10),
      child: Material(
        color: white,
        elevation: 2,
        shadowColor: black.withOpacity(.2),
        borderRadius: BorderRadius.circular(5),
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    border: Border.all(color: black.withOpacity(.08)),
                    borderRadius: BorderRadius.circular(4),
                    color: black.withOpacity(.02)),
                child: Row(
                  children: [
                    Text(
                      "MEET ME",
                      style: textStyle(true, 13, black),
                    ),
                    Spacer(),
                  ],
                ),
              ),
              addSpace(5),
              Expanded(
                child: Builder(builder: (ctx) {
                  return Row(
                    children: [
                      Expanded(
                        child: Container(
                          width: 500,
                          color: default_white,
                          child: Center(
                            child: MeetMe(
                              webMode: true,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Builder(builder: (ctx) {
                          return Container(
                            width: 500,
                            color: black.withOpacity(.05),
                          );
                        }),
                      ),
                    ],
                  );
                }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Container D_Messages() {
    return Container(
      margin: EdgeInsets.all(10),
      child: Material(
        color: white,
        elevation: 2,
        shadowColor: black.withOpacity(.2),
        borderRadius: BorderRadius.circular(5),
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    border: Border.all(color: black.withOpacity(.08)),
                    borderRadius: BorderRadius.circular(4),
                    color: black.withOpacity(.02)),
                child: Row(
                  children: [
                    Text(
                      "MESSAGES",
                      style: textStyle(true, 13, black),
                    ),
                    Spacer(),
                  ],
                ),
              ),
              addSpace(5),
              Expanded(
                child: Container(
                  // width: getScreenWidth(context) / 3,
                  child: Builder(builder: (ctx) {
                    if (!chatSetup) return loadingLayout();
                    if (lastMessages.isEmpty)
                      return Center(
                        child: Padding(
                          padding: const EdgeInsets.all(10),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Image.asset(
                                ic_chat,
                                width: 50,
                                height: 50,
                                color: AppConfig.appColor,
                              ),
                              Text(
                                "No Chat Yet",
                                style: textStyle(true, 20, black),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      );

                    return Container(
                        child: ListView.builder(
                      key: const ValueKey('cpList'),
                      itemBuilder: (c, p) {
                        BaseModel model = lastMessages[p];

                        String chatId = model.getString(CHAT_ID);
                        String otherPersonId = getOtherPersonId(model);
                        List partyModels = model.getList(PARTIES_MODEL);

                        Map otherPersonItem = partyModels.singleWhere(
                            (e) => e[OBJECT_ID] == otherPersonId,
                            orElse: () => {});
                        BaseModel otherPerson =
                            BaseModel(items: otherPersonItem);

                        String name = otherPerson.getString(NAME);
                        String image = otherPerson.getString(IMAGE_URL);
                        if (image.isEmpty) image = otherPerson.userImage;
                        if (name.isEmpty) name = "No Name";

                        return chatItem(image, name, model,
                            p == lastMessages.length - 1, otherPerson);
                      },
                      shrinkWrap: true,
                      itemCount: lastMessages.length,
                      padding: EdgeInsets.all(0),
                    ));
                  }),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  chatItem(String image, String name, BaseModel chatModel, bool last,
      BaseModel otherPerson) {
    int type = chatModel.getInt(TYPE);
    String chatId = chatModel.getString(CHAT_ID);
    bool myItem = chatModel.myItem();

    String hisId = chatId.replaceAll(userModel.getObjectId(), "");
    bool read = chatModel.getList(READ_BY).contains(hisId);
    bool myRead = chatModel.getList(READ_BY).contains(userModel.getObjectId());
    List mutedList = userModel.getList(MUTED);
    int unread =
        unreadCounter[chatId] == null ? 0 : unreadCounter[chatId].length;

    return new InkWell(
      onLongPress: () {
        pushAndResult(
            context,
            listDialog([
              mutedList.contains(chatId) ? "Unmute Chat" : "Mute Chat",
              "Delete Chat"
            ]), result: (_) {
          if (_ == "Mute Chat" || _ == "Unmute Chat") {
            if (mutedList.contains(chatId)) {
              mutedList.remove(chatId);
            } else {
              mutedList.add(chatId);
            }
            userModel.put(MUTED, mutedList);
            userModel.updateItems();
            setState(() {});
          }
          if (_ == "Delete Chat") {
            yesNoDialog(context, "Delete Chat?",
                "Are you sure you want to delete this chat?", () {
              deleteChat(chatId);
            });
          }
        }, depend: false);
      },
      onTap: () {
        BaseModel chat = BaseModel();
        chat.put(PARTIES, [userModel.getObjectId(), otherPerson.getObjectId()]);
        chat.put(PARTIES_MODEL,
            [chatPartyModel(userModel), chatPartyModel(otherPerson)]);
        chat.saveItem(CHAT_IDS_BASE, false, document: chatId, merged: true);

        chatModel.putInList(READ_BY, userModel.getObjectId(), true);
        chatModel.updateItems();
        unreadCounter.remove(chatId);
        showNewMessageDot.removeWhere((id) => id == chatId);

        pushAndResult(
            context,
            ChatMainNew(
              chatId,
              otherPerson: otherPerson,
            ), result: (_) {
          setState(() {});
        });
      },
      child: Container(
        decoration: BoxDecoration(
            color: white,
            boxShadow: [BoxShadow(color: black.withOpacity(.1), blurRadius: 5)],
            borderRadius: BorderRadius.circular(5)),
        padding: EdgeInsets.fromLTRB(10, 5, 10, 0),
        margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
//                  margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                  width: 80,
                  height: 80,
                  child: Card(
                      color: black.withOpacity(.1),
                      elevation: 0,
                      clipBehavior: Clip.antiAlias,
                      shape: CircleBorder(
                          side: BorderSide(
                              color: black.withOpacity(.2), width: .9)),
                      // shape: RoundedRectangleBorder(
                      //     borderRadius: BorderRadius.all(Radius.circular(10)),
                      //     side: BorderSide(
                      //         color: black.withOpacity(.1), width: .8)),
                      child: Stack(
                        children: [
                          Center(
                              child: Icon(
                            Icons.person,
                            color: white.withOpacity(.7),
                          )),
                          (otherPerson.getBoolean(IS_ADMIN))
                              ? Image.asset(
                                  ic_launcher,
                                  width: 40,
                                  height: 40,
                                  fit: BoxFit.cover,
                                )
                              : Image.network(
                                  image,
                                  fit: BoxFit.cover,
                                  width: 80,
                                  height: 80,
                                ),
                        ],
                      )),
                ),
                addSpaceWidth(10),
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: new Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Text(
                              //"Emeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",
                              (otherPerson.getBoolean(IS_ADMIN))
                                  ? "Strock Support"
                                  : name,
                              maxLines: 1, overflow: TextOverflow.ellipsis,
                              style: textStyle(true, 20, black),
                            ),
                          ),
                          addSpaceWidth(5),
                          !myItem && !myRead && unread > 0
                              ? /*Icon(
                                  Icons.new_releases,
                                  size: 20,
                                  color: white,
                                )*/
                              (Container(
                                  width: 25,
                                  height: 25,
                                  decoration: BoxDecoration(
                                    color: AppConfig.appColor,
                                    shape: BoxShape.circle,
//                                      border:
//                                          Border.all(color: white, width: 2)
                                  ),
                                  child: Center(
                                      child: Text(
                                    "${unread > 9 ? "9+" : unread}",
                                    style: textStyle(true, 12, white),
                                  )),
                                ))
                              : Container(),
                          addSpaceWidth(5),
                          Text(
                            getChatTime(chatModel.getTime()),
                            style: textStyle(false, 12, black.withOpacity(.8)),
                            textAlign: TextAlign.end,
                          ),

                          //addSpaceWidth(5),
                        ],
                      ),
                      addSpace(5),
                      Row(
                        children: <Widget>[
                          Flexible(
                            fit: FlexFit.tight,
                            child: Row(
                              children: <Widget>[
                                Flexible(
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      if (myItem && read)
                                        Container(
                                            margin: EdgeInsets.only(right: 5),
                                            child: Icon(
                                              Icons.remove_red_eye,
                                              size: 12,
                                              color: blue0,
                                            )),
                                      Icon(
                                        type == CHAT_TYPE_TEXT
                                            ? Icons.message
                                            : type == CHAT_TYPE_IMAGE
                                                ? Icons.camera_alt
                                                : type == CHAT_TYPE_VIDEO
                                                    ? Icons.videocam
                                                    : type == CHAT_TYPE_REC
                                                        ? Icons.mic
                                                        : Icons.library_books,
                                        color: black.withOpacity(.8),
                                        size: 12,
                                      ),
                                      addSpaceWidth(5),
                                      Flexible(
                                        flex: 1,
                                        child: Text(
                                          chatRemoved(chatModel)
                                              ? "This message has been removed"
                                              : type == CHAT_TYPE_TEXT
                                                  ? chatModel.getString(MESSAGE)
                                                  : type == CHAT_TYPE_IMAGE
                                                      ? "Photo"
                                                      : type == CHAT_TYPE_VIDEO
                                                          ? "Video"
                                                          : type ==
                                                                  CHAT_TYPE_REC
                                                              ? "Voice Note (${chatModel.getString(AUDIO_LENGTH)})"
                                                              : "Document",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: textStyle(
                                              false, 14, black.withOpacity(.8)),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          if (mutedList.contains(chatId))
                            Image.asset(
                              ic_mute,
                              width: 18,
                              height: 18,
                              color: white.withOpacity(.5),
                            )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
            addSpace(5),
            addLine(.5, white.withOpacity(.3), 0, 0, 0, 0)
          ],
        ),
      ),
    );
  }

  deleteChat(String chatId) {
    lastMessages.removeWhere((bm) => bm.getString(CHAT_ID) == chatId);
    stopListening.add(chatId);
    userModel.putInList(DELETED_CHATS, chatId, true);
    userModel.updateItems();
    if (mounted) setState(() {});

    Firestore.instance
        .collection(CHAT_BASE)
        .where(PARTIES, arrayContains: userModel.getUserId())
        .where(CHAT_ID, isEqualTo: chatId)
        .orderBy(TIME, descending: false)
        .getDocuments()
        .then((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel chat = BaseModel(doc: doc);
        if (chat.myItem()) {
          chat.put(DELETED, true);
          chat.updateItems();
        } else {
          List hidden = List.from(chat.getList(HIDDEN));
          hidden.add(userModel.getObjectId());
          chat.put(HIDDEN, hidden);
          chat.updateItems();
        }
      }
    });
  }
}
