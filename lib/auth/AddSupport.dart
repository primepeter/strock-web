import 'dart:ui';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/basemodel.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../assets.dart';

class AddSupport extends StatefulWidget {
  AddSupport({Key key, this.title, this.theUser, this.changePass = false})
      : super(key: key);
  final String title;
  final BaseModel theUser;
  final bool changePass;
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<AddSupport> {
  bool isChecked = false;
  String errorText = "";
  bool isLoading = false;
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final passController = TextEditingController();
  final passController2 = TextEditingController();
  BaseModel model;

  showError(String text) {
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 1), () {
      errorText = "";
      setState(() {});
    });
  }

  showLoading(bool value) {
    isLoading = value;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    if (widget.theUser != null) {
      model = widget.theUser;
      nameController.text = model.getString(NAME);
      emailController.text = model.getEmail();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: transparent,
        body: Stack(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
                  child: Container(
                    color: black.withOpacity(.7),
                  )),
            ),
            Center(
              child: Card(
                elevation: 5.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25)),
                child: Container(
                  width: MediaQuery.of(context).size.width / 2.5,
                  //height: MediaQuery.of(context).size.height / 1.5,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      AnimatedContainer(
                        duration: Duration(milliseconds: 500),
                        width: double.infinity,
                        height: errorText.isEmpty ? 0 : 40,
                        color: red0,
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: Center(
                            child: Text(
                          errorText,
                          style: textStyle(true, 16, white),
                        )),
                      ),
                      AnimatedContainer(
                        duration: Duration(milliseconds: 500),
                        width: double.infinity,
                        height: isLoading ? 40 : 0,
//                      height: 40,
                        color: green,
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: Stack(
                          children: [
                            Center(
                                child: Text(
                              widget.theUser != null
                                  ? "Updating..."
                                  : "Loggin In...",
                              style: textStyle(true, 16, white),
                            )),
                            LinearProgressIndicator()
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(30),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Image.asset(
                              ic_launcher,
                              height: 50,
                              width: 50,
                            ),
                            addSpace(20),
                            Text(
                              widget.theUser != null
                                  ? "Update Staff Profile"
                                  : "Add Strock Support",
                              textAlign: TextAlign.center,
                              style: textStyle(true, 28, black),
                            ),
                            addSpace(48),
                            TextFormField(
                              controller: nameController,
                              enabled:
                                  widget.theUser != null ? false : !isLoading,
                              keyboardType: TextInputType.emailAddress,
                              autofocus: false,
                              decoration: InputDecoration(
                                hintText: 'Staff Name',
                                contentPadding:
                                    EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                              ),
                            ),
                            addSpace(8),
                            TextFormField(
                              controller: emailController,
                              enabled:
                                  widget.theUser != null ? false : !isLoading,
                              keyboardType: TextInputType.emailAddress,
                              autofocus: false,
                              decoration: InputDecoration(
                                hintText: 'Staff Email',
                                contentPadding:
                                    EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                              ),
                            ),
                            addSpace(8),
                            TextFormField(
                              controller: passController,
                              enabled: !isLoading,
                              autofocus: false,
                              obscureText: true,
                              decoration: InputDecoration(
                                hintText: widget.theUser != null
                                    ? "Staff Password (Old)"
                                    : 'Staff Password',
                                contentPadding:
                                    EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                              ),
                            ),
                            if (widget.theUser != null) ...[
                              addSpace(8),
                              TextFormField(
                                controller: passController2,
                                enabled: !isLoading,
                                autofocus: false,
                                obscureText: true,
                                decoration: InputDecoration(
                                  hintText: "Staff Password (New)",
                                  contentPadding: EdgeInsets.fromLTRB(
                                      20.0, 10.0, 20.0, 10.0),
                                ),
                              ),
                            ],
                            addSpace(24),
                            RaisedButton(
                              onPressed: isLoading
                                  ? null
                                  : () {
                                      handleSignUp();
                                    },
                              padding: EdgeInsets.all(12),
                              color: AppConfig.appColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15)),
                              child: Center(
                                child: Text(
                                    widget.theUser != null
                                        ? "Update Profile"
                                        : 'Create Support Staff',
                                    style: textStyle(true, 16, black)),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ));
  }

  handleSignUp() async {
    final name = nameController.text.trim();
    final email = emailController.text.trim();
    final password = passController.text.trim();
    final password2 = passController2.text.trim();

    if (name.isEmpty) {
      showError("Enter support staff name");
      return;
    }
    if (email.isEmpty) {
      showError("Enter support staff email address");
      return;
    }

    if (password.isEmpty) {
      showError((widget.theUser != null)
          ? "Enter your old password"
          : "Enter your password");
      return;
    }

    if ((widget.theUser != null) && password2.isEmpty) {
      showError("Enter new password");
      return;
    }

    showLoading(true);

    if ((widget.theUser != null)) {
      FirebaseAuth.instance.currentUser.updatePassword(password2);
      model.put(PASSWORD, password2);
      model.updateItems();
      Navigator.pop(context, model);
      return;
    }

    FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email, password: password)
        .then((value) {
      BaseModel theUser = BaseModel();
      theUser.put(NAME, name);
      theUser.put(EMAIL, email);
      theUser.put(PASSWORD, password);
      theUser.put(USER_ID, value.user.uid);
      theUser.put(SIGNUP_COMPLETED, true);
      theUser.put(IS_SUPPORT, true);
      theUser.saveItem(USER_BASE, false, document: value.user.uid,
          onComplete: () {
        Navigator.pop(context, theUser);
      });
    }).catchError((e) {
      showLoading(false);
      showError(e.message);
    });
  }
}
