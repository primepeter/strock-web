import 'dart:async';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/app/navigation.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/auth/auth_main.dart';
import 'package:Strokes/preinit.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() {
    return _SignUpState();
  }
}

class _SignUpState extends State<SignUp> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final nameController = TextEditingController();
  bool emailInvalid = false;
  bool passwordInvalid = false;
  final focusName = new FocusNode();
  final focusEmail = new FocusNode();
  final focusPassword = new FocusNode();
  BuildContext context;
  bool passwordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    focusName.requestFocus();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    emailController.dispose();
    passwordController.dispose();
  }

  @override
  Widget build(BuildContext cc) {
    return WillPopScope(
      onWillPop: () async {
        pushReplacementAndResult(context, PreInit());
        return false;
      },
      child: Scaffold(
          backgroundColor: white,
          key: _scaffoldKey,
          resizeToAvoidBottomInset: true,
          body: Builder(builder: (c) {
            context = c;
            return page();
          })),
    );
  }

  page() {
    return Column(
      children: <Widget>[
        Align(
            alignment: Alignment.topLeft,
            child: Container(
              padding: EdgeInsets.only(top: 40, left: 10),
              child: Row(
                children: [
                  BackButton(
                    color: black,
                    onPressed: () {
                      pushReplacementAndResult(context, PreInit());
                    },
                  ),
                  Text("SignUp for Strock", style: textStyle(true, 22, black)),
                ],
              ),
            )),
        Image.asset("assets/icons/ic_launcher.png", height: 50, width: 50),
        addSpace(20),
        Expanded(
          flex: 1,
          child: SingleChildScrollView(
            padding: EdgeInsets.all(0),
            child: Container(
              color: white,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  textbox(nameController, "Name", focusNode: focusName),
                  textbox(emailController, "Email Address",
                      focusNode: focusEmail),
                  textbox(passwordController, "Password",
                      focusNode: focusPassword,
                      isPass: true,
                      refresh: () => setState(() {})),
////                  addSpace(10),
//                  GestureDetector(
//                    onTap: () {
//                      pushAndResult(context, ForgotPassword(), depend: false);
//                    },
//                    child: new Text(
//                      "FORGOT PASSWORD?",
//                      style: textStyle(true, 17, black),
//                    ),
//                  ),
//          addLine(.5, black.withOpacity(.1), 15, 0, 15, 0),
                ],
              ),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.all(15),
          child: Text.rich(
            TextSpan(
              children: [
                TextSpan(
                    text:
                        'By Clicking on "CREATE ACCOUNT", You hereby agree to our ',
                    style: textStyle(false, 14, black)),
                TextSpan(
                    text: 'Terms of Service',
                    recognizer: new TapGestureRecognizer()
                      ..onTap = () =>
                          openLink(appSettingsModel.getString(TERMS_LINK)),
                    style: textStyle(true, 14, AppConfig.appColor)),
                TextSpan(text: ' and ', style: textStyle(false, 14, black)),
                TextSpan(
                    text: 'Privacy Policy',
                    recognizer: new TapGestureRecognizer()
                      ..onTap = () =>
                          openLink(appSettingsModel.getString(PRIVACY_LINK)),
                    style: textStyle(true, 14, AppConfig.appColor)),
                TextSpan(
                    text: ' Binding our community.',
                    style: textStyle(false, 14, black)),
              ],
            ),
            textAlign: TextAlign.center,
          ),
        ),
        Container(
          width: double.infinity,
          height: 60,
          margin: EdgeInsets.all(0),
          child: FlatButton(
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(0)),
              color: AppConfig.appColor,
              onPressed: () {
                signUp();
              },
              child: Text(
                "CREATE ACCOUNT",
                style: textStyle(true, 16, white),
              )),
        )
      ],
    );
  }

  void signUp() async {
    String name = nameController.text;
    String email = emailController.text.trim();
    String password = passwordController.text;

    emailInvalid = !isEmailValid(email);
    passwordInvalid = password.length < 6;

    if (name.isEmpty) {
      passwordInvalid = false;
      FocusScope.of(context).requestFocus(focusName);
      setState(() {});
      snack("Enter your name");
      return;
    }

    if (emailInvalid) {
      passwordInvalid = false;
      FocusScope.of(context).requestFocus(focusEmail);
      setState(() {});
      snack("Enter your email address");
      return;
    }

    if (!emailInvalid && password.isEmpty) {
      passwordInvalid = false;
      FocusScope.of(context).requestFocus(focusPassword);
      setState(() {});
      snack("Enter your password");
      return;
    }

    if (passwordInvalid) {
      FocusScope.of(context).requestFocus(focusPassword);
      setState(() {});
      snack("Enter your password");
      return;
    }

    startSignUp(name, email, password);
  }

  startSignUp(name, email, password) async {
    var result = await (Connectivity().checkConnectivity());
    if (result == ConnectivityResult.none) {
      snack("No internet connectivity");
      return;
    }

    showProgress(true, context, msg: "Signing Up");

    final FirebaseAuth mAuth = FirebaseAuth.instance;
    mAuth
        .createUserWithEmailAndPassword(email: email, password: password)
        .then((value) {
      userModel
        ..put(NAME, name)
        ..put(EMAIL, email)
        ..put(PASSWORD, password)
        ..put(USER_ID, value.user.uid)
        ..put(OBJECT_ID, value.user.uid)
        ..saveItem(USER_BASE, false, document: value.user.uid, onComplete: () {
          Future.delayed(Duration(seconds: 1), () {
            popUpUntil(context, AuthMain());
          });
        });
    }).catchError((e) {
      handleError(email, password, e);
    });
  }

  handleError(email, password, error) {
    showProgress(false, context);

    if (error.toString().toLowerCase().contains("badly formatted")) {
      snack("Invalid email address/password");
      FocusScope.of(context).requestFocus(focusEmail);
      return;
    }

    if (error.toString().toLowerCase().contains("password is invalid")) {
      snack("Invalid email address/password");
      FocusScope.of(context).requestFocus(focusPassword);
      return;
    }
    snack("Error occurred, try again");
  }

  final String progressId = getRandomId();

  bool isEmailValid(String email) {
    if (!email.contains("@") || !email.contains(".")) return false;
    return true;
  }

  snack(String text) {
    Future.delayed(Duration(milliseconds: 500), () {
      showSnack(_scaffoldKey, text, useWife: true);
    });
  }
}
