// ignore: avoid_web_libraries_in_flutter
import 'dart:html';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';

Widget adsenseAdsView() {
  // ignore: undefined_prefixed_name
  ui.platformViewRegistry.registerViewFactory(
      'adViewType',
      (int viewID) => IFrameElement()
        ..width = '320'
        ..height = '100'
        ..src = 'https://strock.fun/adview.html'

        /* ..srcdoc = """
        
     <html>
    <head>
        This is the head of your page
        <title>Example HTML page</title>
        <script data-ad-client="ca-pub-6974625882855412" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    </head>
    <body>
        **This is the area an ad should display...**
        This is the body of your page.
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Homepage Leaderboard -->
        <ins class="adsbygoogle"
            style="display:block"
            data-ad-client="ca-pub-6974625882855412"
            data-ad-slot="1838912890"
             data-ad-format="auto"
     data-full-width-responsive="true"
    <!-- data-ad-test="on"-->
    ></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </body>
</html>   
        """*/
        ..style.border = 'none');

  return SizedBox(
    height: 100.0,
    width: 320.0,
    child: HtmlElementView(
      viewType: 'adViewType',
    ),
  );
}

Widget youTubeBox() {
  ui.platformViewRegistry.registerViewFactory(
      'hello-world-html',
      (int viewId) => IFrameElement()
        ..width = '640'
        ..height = '360'
        ..src = 'https://www.youtube.com/embed/IyFZznAk69U'
        ..style.border = 'none');

  return SizedBox(
    height: 360.0,
    width: 640.0,
    child: HtmlElementView(
      viewType: 'hello-world-html',
    ),
  );
}
