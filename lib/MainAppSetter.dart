import 'package:Strokes/MainAdminHome.dart';
import 'package:Strokes/app/navigation.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'AppEngine.dart';
import 'MainAuthAdmin.dart';
import 'MainAuthWeb.dart';
import 'MainWebHome.dart';
import 'auth/auth_main.dart';

class MainAppSetter extends StatefulWidget {
  final bool isAdmin;

  const MainAppSetter({Key key, this.isAdmin = false}) : super(key: key);
  @override
  _MainAppSetterState createState() => _MainAppSetterState();
}

class _MainAppSetterState extends State<MainAppSetter> {
  bool get admin => widget.isAdmin;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      checkUser();
      // checkUserAdmin();
    });
  }

  checkUser() async {
    await Future.delayed(Duration(seconds: 3));
    final FirebaseAuth _auth = FirebaseAuth.instance;
    User user = _auth.currentUser;
    if (user != null) {
      FirebaseFirestore.instance
          .collection(USER_BASE)
          .doc(user.uid)
          .get(/*GetOptions(source: Source.cache)*/)
          .then((doc) {
        userModel = BaseModel(doc: doc);
        isAdmin = userModel.getBoolean(IS_ADMIN) ||
            userModel.getString(EMAIL).contains('maugost') ||
            userModel.getString(EMAIL) == "johnebere58@gmail.com" ||
            userModel.getString(EMAIL) == "ammaugost@gmail.com";

        if (!userModel.signUpCompleted) {
          popUpUntil(context, AuthMain());
          return;
        }
        popUpUntil(context, MainWebHome());
      });
    } else {
      // loadTestAcct();
      // return;
      popUpUntil(context, MainAuthWeb());
    }
  }

  loadTestAcct() {
    /*String id = getRandomId();
    userModel = BaseModel(items: {
      "name": "Maugost Mtellect",
      "objectId": id,
      "userId": id,
    });
    popUpUntil(context, MyProfile());
    return;*/

    FirebaseFirestore.instance
        .collection(USER_BASE)
        .where(EMAIL, isEqualTo: "co.petersnr@gmail.com")
        // .where(EMAIL, isEqualTo: "peter.okachie@gmail.com")
        // .where(EMAIL, isEqualTo: "davefaps@gmail.com")
        .limit(1)
        .get()
        .then((value) {
      if (value.size == 0) {
        popUpUntil(context, AuthMain());
        return;
      }

      userModel = BaseModel(doc: value.docs[0]);
      popUpUntil(context, MainWebHome());
    });
  }

  checkUserAdmin() async {
    await Future.delayed(Duration(seconds: 5));
    final FirebaseAuth _auth = FirebaseAuth.instance;
    _auth.authStateChanges().listen((user) {
      print('Current user  $user');
      print('Current user  ${user == null}');
      if (user != null) {
        loadLocalUser(user.uid);
        popUpUntil(context, MainAdminHome());
      } else {
        popUpUntil(context, MainAuthAdmin());
      }
    });
  }

  loadSettings() {
    FirebaseFirestore.instance
        .collection(APP_SETTINGS_BASE)
        .doc(APP_SETTINGS)
        .get()
        .then((doc) {
      if (!doc.exists) {
        appSettingsModel = new BaseModel();
        appSettingsModel.saveItem(APP_SETTINGS_BASE, false,
            document: APP_SETTINGS);
        return;
      }
      appSettingsModel = BaseModel(doc: doc);
    });
  }

  loadLocalUser(String userId) {
    FirebaseFirestore.instance
        .collection(USER_BASE)
        .doc(userId)
        .get(GetOptions(source: Source.cache))
        .then((doc) {
      userModel = BaseModel(doc: doc);
      isAdmin = userModel.getBoolean(IS_ADMIN) ||
          userModel.getString(EMAIL).contains('maugost') ||
          userModel.getString(EMAIL) == "johnebere58@gmail.com" ||
          userModel.getString(EMAIL) == "ammaugost@gmail.com";

      if (!userModel.signUpCompleted) {
        popUpUntil(context, AuthMain());
        return;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return loadingLayout();
  }
}
