import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainWebHome.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'ShowProfile.dart';

class ShowProfileViews extends StatefulWidget {
  @override
  _ShowProfileViewsState createState() => _ShowProfileViewsState();
}

class _ShowProfileViewsState extends State<ShowProfileViews> {
  @override
  void initState() {
    super.initState();
    overlayController.add(false);
    loadSeenBy();
  }

  loadSeenBy() async {
    //load persons i have seen there profile
    Firestore.instance
        .collection(USER_BASE)
        .where(VIEWED_LIST, arrayContains: userModel.getUserId())
        .getDocuments()
        .then((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
        if (!model.signUpCompleted) {
          userModel
            ..putInList(VIEWED_LIST, model.getUserId(), false)
            ..updateItems();
          continue;
        }
        int index = seenByList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (index == -1) {
          seenByList.add(model);
        } else {
          seenByList[index] = model;
        }
      }
      seenBySetup = true;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        overlayController.add(true);
        Future.delayed(Duration(milliseconds: 20), () {
          Navigator.pop(context, "");
        });
        return false;
      },
      child: Scaffold(
        body: page(),
      ),
    );
  }

  page() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(0, 5, 0, 10),
          color: white,
          child: Row(
            children: <Widget>[
              IconButton(
                  onPressed: () {
                    overlayController.add(true);
                    Future.delayed(Duration(milliseconds: 20), () {
                      Navigator.pop(context, "");
                    });
                  },
                  icon: Icon(
                    Icons.keyboard_backspace,
                    color: black,
                    size: 25,
                  )),
              Text(
                "Viewed Me (${seenByList.length})",
                style: textStyle(true, 25, black),
              ),
              Spacer()
            ],
          ),
        ),
        Expanded(
          flex: 1,
          child: body(),
        )
      ],
    );
  }

  body() {
    if (!seenBySetup) return loadingLayout();
    if (seenByList.isEmpty)
      return emptyLayout(Icons.person, "No Views Yet", "");
    return Container(
        child: GridView.builder(
      key: const ValueKey('pVList'),
      itemBuilder: (c, p) {
        return personItem(p);
      },
      physics: AlwaysScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: seenByList.length,
      padding: EdgeInsets.only(top: 10, right: 5, left: 5, bottom: 40),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 0.8,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5),
    ));
  }

  personItem(int p) {
    BaseModel model = seenByList[p];

    bool isVideo = model.profilePhotos[0].isVideo;
    String imageUrl =
        model.profilePhotos[0].getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);

    return GestureDetector(
      onTap: () {
        pushAndResult(
            context,
            ShowProfile(
              theUser: model,
              //fromMeetMe: widget.fromStrock,
            ));
      },
      child: Card(
        color: white,
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: Stack(
                  children: [
                    Container(
                        height: double.infinity,
                        width: double.infinity,
                        child: Center(
                            child: Icon(
                          Icons.person,
                          color: white,
                          size: 15,
                        )),
                        decoration: BoxDecoration(
                          color: black.withOpacity(.09),
                          //shape: BoxShape.circle
                        )),
                    Image.network(
                      imageUrl,
                      fit: BoxFit.cover,
                      width: double.infinity,
                      height: double.infinity,
                    ),
                    if (isVideo)
                      Center(
                        child: Container(
                          height: 50,
                          width: 50,
                          child: Icon(
                            Icons.play_arrow,
                            color: Colors.white,
                          ),
                          decoration: BoxDecoration(
                              color: Colors.black.withOpacity(0.8),
                              border:
                                  Border.all(color: Colors.white, width: 1.5),
                              shape: BoxShape.circle),
                        ),
                      ),
                  ],
                ),
              ),
            ),
            Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Flexible(
                          fit: FlexFit.loose,
                          child: Text(
                            getFirstName(model),
                            style: textStyle(true, 14, black),
                            maxLines: 1,
                            textAlign: TextAlign.center,
                          ),
                        ),
                        addSpaceWidth(5),
                        if (isOnline(model))
                          Container(
                            height: 8,
                            width: 8,
                            decoration: BoxDecoration(
                                color: green, shape: BoxShape.circle),
                          ),
                        Text(
                          getMyAge(model).toString(),
                          style: textStyle(false, 11, black.withOpacity(.5)),
                        ),
                      ],
                    ),
//                    FlatButton(
//                      onPressed: () {
//                        clickChat(context, model, false);
//                      },
//                      padding: EdgeInsets.all(5),
//                      shape: RoundedRectangleBorder(
//                          side: BorderSide(color: black.withOpacity(.2))),
//                      child: Center(
//                        child: Text(
//                          "Chat Now",
//                          style: textStyle(false, 12, black.withOpacity(.7)),
//                        ),
//                      ),
//                    )
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
