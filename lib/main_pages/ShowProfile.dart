import 'dart:async';
import 'dart:math';
import 'dart:ui';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainWebHome.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:Strokes/dialogs/inputDialog.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:intl/intl.dart';

class ShowProfile extends StatefulWidget {
  final BaseModel theUser;
  final bool fromMeetMe;

  const ShowProfile({Key key, this.theUser, this.fromMeetMe = false})
      : super(key: key);
  @override
  _ShowProfileState createState() => _ShowProfileState();
}

class _ShowProfileState extends State<ShowProfile>
    with TickerProviderStateMixin {
  BaseModel theUser;
  double matchRate = 0;
  double pixels = 0;
  double distance = 0;
  bool fromMeetMe = false;
  @override
  void initState() {
    super.initState();
    theUser = widget.theUser;
    fromMeetMe = widget.fromMeetMe;
    if (!userModel.getList(VIEWED_LIST).contains(theUser.getUserId())) {
      pushNotificationToUser(
          theUser, NOTIFY_VIEWED_YOU);
      userModel
        ..putInList(VIEWED_LIST, theUser.getUserId(), true)
        ..updateItems();
    }

    widget.theUser
      ..putInList(PROFILE_VIEWS, userModel.getUserId(), true)
      ..updateItems(updateTime: false);

    int index = seenList
        .indexWhere((bm) => bm.getObjectId() == widget.theUser.getObjectId());
    if (index == -1) {
      seenList.add(widget.theUser);
    } else {
      seenList[index] = widget.theUser;
    }

    if (theUser.getInt(ETHNICITY) == userModel.getInt(ETHNICITY)) {
      matchRate = matchRate + 80;
    }
    if (theUser.getInt(RELATIONSHIP) == userModel.getInt(RELATIONSHIP)) {
      matchRate = matchRate + 80;
    }
    matchRate = matchRate > 100 ? 100 : matchRate;
    matchRate = matchRate < 50 ? 50 : matchRate;
    reloadUser();
  }

  @override
  dispose() {
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  List<StreamSubscription> subs = [];

  reloadUser() async {
    var sub = Firestore.instance
        .collection(USER_BASE)
        .document(widget.theUser.getUserId())
        .snapshots()
        .listen((event) async {
      theUser = BaseModel(doc: event);
      matchRate = 0;
      if (theUser.getInt(ETHNICITY) == userModel.getInt(ETHNICITY)) {
        matchRate = matchRate + 80;
      }
      if (theUser.getInt(RELATIONSHIP) == userModel.getInt(RELATIONSHIP)) {
        matchRate = matchRate + 80;
      }
      matchRate = matchRate > 100 ? 100 : matchRate;
      matchRate = matchRate < 50 ? 50 : matchRate;

      Geoflutterfire geo = Geoflutterfire();
      Map myPosition = userModel.getMap(POSITION);

      GeoPoint myGeoPoint = myPosition["geopoint"];
      double lat = myGeoPoint.latitude;
      double lon = myGeoPoint.longitude;
      GeoFirePoint center = geo.point(latitude: lat, longitude: lon);
      if (theUser.getMap(POSITION).isNotEmpty) {
        final geoPoint = theUser.getModel(POSITION).get("geopoint") as GeoPoint;
        distance = (await calculateDistanceTo(geoPoint, myGeoPoint) / 1000);
      }

      distance = distance.roundToDouble();
      setState(() {});
    });
    subs.add(sub);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        overlayController.add(false);

        return true;
      },
      child: Scaffold(
        backgroundColor: white,
        body: page(),
      ),
    );
  }

  String get formatBirthDate {
    final date = DateTime.parse(theUser.birthDate);
    return new DateFormat("MMMM d").format(date);
  }

  //Gender relationship and ethnicity if 3 match 80% else if all match 100%
  //For 100% match
  page() {
    List images = theUser.getList(PROFILE_PHOTOS);
    bool isVideo =
        theUser.profilePhotos[theUser.getInt(DEF_PROFILE_PHOTO)].isVideo;
    String image = isVideo
        ? theUser.profilePhotos[theUser.getInt(DEF_PROFILE_PHOTO)].thumbnailUrl
        : theUser.profilePhotos[theUser.getInt(DEF_PROFILE_PHOTO)].imageUrl;
//    String image =
//        theUser.profilePhotos[theUser.getInt(DEF_PROFILE_PHOTO)].imageUrl;

    double blur = (pixels / 10);
    double imageHeight =
        (getScreenHeight(context) * 0.5) - (pixels > 0 ? 0 : (pixels));
    return Stack(
      children: [
        Container(
            height: imageHeight,
            width: double.infinity,
            child: Center(
                child: Icon(
              Icons.person,
              color: white,
              size: 15,
            )),
            decoration: BoxDecoration(
              color: black.withOpacity(.09),
              //shape: BoxShape.circle
            )),
        if (image.contains("http"))
          Image.network(
            image,
            fit: BoxFit.cover,
            width: double.infinity,
            height: imageHeight,
          ),
        Container(
            height: imageHeight,
            child: ClipRect(
              child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: blur, sigmaY: blur),
                  child: Container(
                    color: black.withOpacity(.2),
                  )),
            )),
        Align(
          alignment: Alignment.topCenter,
          child: Opacity(
            opacity: pixels < 0 ? 0 : (min(1, (pixels / 100))),
            child: Container(
              margin: EdgeInsets.only(top: 30),
              child: Card(
                  clipBehavior: Clip.antiAlias,
                  shape:
                      CircleBorder(/*side: BorderSide(color: white,width: 3)*/),
                  elevation: 0,
                  child: Stack(
                    children: [
                      Container(
                          height: (max(50, (imageHeight - pixels) - 90)),
                          width: (max(50, (imageHeight - pixels) - 90)),
                          child: Center(
                              child: Icon(
                            Icons.person,
                            color: white,
                            size: 15,
                          )),
                          decoration: BoxDecoration(
                            color: black.withOpacity(.09),
                            //shape: BoxShape.circle
                          )),
                      Image.network(
                        image,
                        fit: BoxFit.cover,
                        height: (max(50, (imageHeight - pixels) - 90)),
                        width: (max(50, (imageHeight - pixels) - 90)),
                      ),
                    ],
                  )),
            ),
          ),
        ),
        Column(
          children: [
            Container(
              height: 100,
              //color: black.withOpacity(.1),
            ),
            Flexible(
              child: NotificationListener(
                onNotification: (ScrollNotification sn) {
                  pixels = sn.metrics.pixels;
                  setState(() {});
                  return false;
                },
                child: ListView(
                  key: const ValueKey('spList'),
                  physics: AlwaysScrollableScrollPhysics(),
                  children: [
                    Container(
                      height: getScreenHeight(context) * 0.25,
                    ),
                    Container(
                      // height: 220,
                      //margin: EdgeInsets.all(20),
                      color: transparent,
                      child: Container(
                        child: Stack(
                          //mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              margin: EdgeInsets.all(20),
                              padding: EdgeInsets.all(20),
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                        color: black.withOpacity(.3),
                                        blurRadius: 5),
                                  ],
                                  color: white,
                                  borderRadius: BorderRadius.circular(25)),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text(
                                    theUser.getString(NAME),
                                    style: textStyle(true, 18, black),
                                  ),
                                  addSpace(10),
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.date_range,
                                        color: black.withOpacity(.5),
                                      ),
                                      addSpaceWidth(5),
                                      Text.rich(TextSpan(children: [
                                        TextSpan(
                                          text:
                                              "I am ${getMyAge(theUser).toString()} years old",
                                          style: textStyle(false, 18, black),
                                        ),
//                                        TextSpan(
//                                          text: getMyAge(theUser).toString(),
//                                          style: textStyle(false, 18, black),
//                                        )
                                      ]))
                                    ],
                                  ),
                                  addSpace(10),
                                  /*Row(
                                    children: [
                                      Icon(
                                        Icons.location_on,
                                        color: black.withOpacity(.5),
                                      ),
                                      addSpaceWidth(5),
                                      Flexible(
                                        child: Text.rich(TextSpan(children: [
                                          TextSpan(
                                            text: "Lives in ${theUser.city}",
                                            style: textStyle(false, 18, black),
                                          ),
                                        ])),
                                      )
                                    ],
                                  ),
                                  addSpace(10),*/
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.map,
                                        color: black.withOpacity(.5),
                                      ),
                                      addSpaceWidth(5),
                                      Flexible(
                                        child: Text.rich(TextSpan(children: [
                                          TextSpan(
                                            text: "Distance ",
                                            style: textStyle(false, 18, black),
                                          ),
                                          if (distance != 0)
                                            TextSpan(
                                              text: "(${distance}KM Away)",
                                              style:
                                                  textStyle(false, 18, black),
                                            )
                                        ])),
                                      )
                                    ],
                                  ),
                                  addSpace(10),
                                  Row(
                                    children: [
                                      Image.asset(
                                        "assets/icons/gender.png",
                                        height: 30,
                                        width: 30,
                                        color: black.withOpacity(.6),
                                        fit: BoxFit.cover,
                                      ),
                                      addSpaceWidth(5),
                                      Text.rich(TextSpan(children: [
                                        TextSpan(text: "Interested in "),
                                        TextSpan(
                                          text: preferenceType[
                                              theUser.getInt(PREFERENCE)],
                                          style: textStyle(false, 18, black),
                                        )
                                      ]))
                                    ],
                                  ),
                                  addSpace(10),
                                  Row(
                                    children: [
                                      Image.asset(
                                        "assets/icons/gender.png",
                                        height: 30,
                                        width: 30,
                                        color: black.withOpacity(.6),
                                        fit: BoxFit.cover,
                                      ),
                                      addSpaceWidth(5),
                                      Text.rich(TextSpan(children: [
                                        TextSpan(text: "Relationship Pref "),
                                        TextSpan(
                                          text: relationshipType[
                                              theUser.getInt(RELATIONSHIP)],
                                          style: textStyle(false, 18, black),
                                        )
                                      ]))
                                    ],
                                  ),
                                  addSpace(10),
                                  Row(
                                    children: [
                                      Image.asset(
                                        "assets/icons/network.png",
                                        height: 30,
                                        width: 30,
                                        color: black.withOpacity(.6),
                                        fit: BoxFit.cover,
                                      ),
                                      addSpaceWidth(5),
                                      Text.rich(TextSpan(children: [
                                        TextSpan(text: "Ethnicity Pref "),
                                        TextSpan(
                                          text: ethnicityType[
                                              theUser.getInt(ETHNICITY)],
                                          style: textStyle(false, 18, black),
                                        )
                                      ]))
                                    ],
                                  ),
                                  addSpace(10),
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.access_time,
                                        color: black.withOpacity(.5),
                                      ),
                                      addSpaceWidth(5),
                                      Text.rich(TextSpan(children: [
                                        //TextSpan(text: "Last Seen "),
                                        TextSpan(
                                          text: getLastSeen(theUser),
                                          style: textStyle(false, 18, black),
                                        )
                                      ]))
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Align(
                              alignment: Alignment.topCenter,
                              child: Container(
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                    color: AppConfig.appColor,
                                    gradient: LinearGradient(
                                        colors: [
                                          orange01,
                                          orange04,
                                          orange01,
                                          //AppConfig.appColor.withOpacity(.7),
                                        ],
                                        begin: Alignment.topLeft,
                                        end: Alignment.bottomRight),
                                    borderRadius: BorderRadius.circular(25)),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Icon(
                                      Icons.favorite,
                                      color: white,
                                      size: 20,
                                    ),
                                    addSpaceWidth(10),
                                    Text(
                                      "${matchRate.toInt()}% Match!",
                                      style: textStyle(false, 16, white),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20, right: 20),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
//                        Container(
//                          height: 60,
//                          width: 60,
//                          child: Icon(
//                            Icons.more_horiz,
//                            color: white,
//                          ),
//                          decoration: BoxDecoration(
//                            color: AppConfig.appColor,
//                            shape: BoxShape.circle,
//                            gradient: LinearGradient(
//                                colors: [
//                                  orange01,
//                                  orange04,
//                                  //AppConfig.appColor.withOpacity(.7),
//                                ],
//                                begin: Alignment.topLeft,
//                                end: Alignment.bottomRight),
//                          ),
//                        ),
//                        addSpaceWidth(15),
                          GestureDetector(
                            onTap: () {
                              if (null != theUser)
                                clickChat(context, theUser, false);
                            },
                            child: Container(
                              padding: EdgeInsets.all(18),
                              decoration: BoxDecoration(
                                  color: AppConfig.appColor,
                                  gradient: LinearGradient(
                                      colors: [
                                        orange01,
                                        orange04,
                                        //AppConfig.appColor.withOpacity(.7),
                                      ],
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomRight),
                                  borderRadius: BorderRadius.circular(25)),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Icon(
                                    Icons.chat,
                                    color: white,
                                    size: 20,
                                  ),
                                  addSpaceWidth(10),
                                  Text(
                                    "Start Chatting",
                                    style: textStyle(false, 16, white),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(20),
                      padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(boxShadow: [
                        BoxShadow(color: black.withOpacity(.3), blurRadius: 5),
                      ], color: white, borderRadius: BorderRadius.circular(25)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Photos",
                            style: textStyle(true, 16, black),
                          ),
                          //addSpace(10),
                          photoBox(theUser.profilePhotos)
                        ],
                      ),
                    ),
                    if (fromMeetMe)
                      Container(
                        margin: EdgeInsets.all(20),
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                  color: black.withOpacity(.3), blurRadius: 5),
                            ],
                            color: white,
                            borderRadius: BorderRadius.circular(25)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Quick Strock Photos",
                              style: textStyle(true, 16, black),
                            ),
                            //addSpace(10),
                            photoBox(theUser.hookUpPhotos, type: "nah")
                          ],
                        ),
                      ),
                    if (theUser.getString(ABOUT_ME).isNotEmpty)
                      Container(
                        margin: EdgeInsets.all(20),
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                  color: black.withOpacity(.3), blurRadius: 5),
                            ],
                            color: white,
                            borderRadius: BorderRadius.circular(25)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "About Me",
                              style: textStyle(true, 16, black),
                            ),
                            addSpace(10),
                            Text(
                              theUser.getString(ABOUT_ME),
                              style: textStyle(false, 14, black),
                            ),
                          ],
                        ),
                      ),
                    if (theUser.getString(WOW_FACTOR).isNotEmpty)
                      Container(
                        margin: EdgeInsets.all(20),
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                  color: black.withOpacity(.3), blurRadius: 5),
                            ],
                            color: white,
                            borderRadius: BorderRadius.circular(25)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Wow Factor",
                              style: textStyle(true, 16, black),
                            ),
                            addSpace(10),
                            Text(
                              theUser.getString(WOW_FACTOR),
                              style: textStyle(false, 14, black),
                            ),
                          ],
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ],
        ),
        Container(
          padding: EdgeInsets.only(top: 5, right: 15, left: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              BackButton(
                color: white,
              ),
//              IconButton(
//                onPressed: () {},
//                icon: Icon(
//                  Icons.more_vert,
//                  color: white,
//                ),
//              )
            ],
          ),
        ),
        Align(
          alignment: Alignment.topRight,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (theUser.getBoolean(FRAUDULENT))
                Container(
                    padding: EdgeInsets.all(8),
                    margin: EdgeInsets.only(
                      top: 5,
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20), color: red),
                    child: Text(
                      "Beware",
                      style: textStyle(false, 14, white),
                    )),
              InkWell(
                  onTap: () {
                    if (isAdmin) {
                      ShowForAdmin(context, theUser, () {
                        setState(() {});
                      });
                      return;
                    }

                    showListDialog(context, ["Report", "Block"], (int p) {
                      if (p == 0) {
                        showListDialog(
                            context, ["Spam", "Fake Profile", "Others Specify"],
                            (int p) {
                          if (p == 0) {
                            createReport(
                                context, theUser, REPORT_TYPE_PROFILE, "Spam");
                          }
                          if (p == 1) {
                            createReport(context, theUser, REPORT_TYPE_PROFILE,
                                "Fake Profile");
                          }
                          if (p == 2) {
                            pushAndResult(
                                context,
                                inputDialog(
                                  "Report",
                                  hint: "Write a report",
                                ), result: (_) {
                              createReport(context, theUser,
                                  REPORT_TYPE_PROFILE, _.trim());
                            });
                          }
                        }, title: "Report");
                      }
                      if (p == 1) {
                        showMessage(
                            context,
                            Icons.block,
                            red0,
                            "Block ${theUser.getString(NAME)}",
                            "This user won't be able to find your profile or connect with you",
                            clickYesText: "BLOCK",
                            clickNoText: "Cancel", onClicked: (_) {
                          if (_ == true) {
                            performBlocking(theUser);
                            showProgress(true, context, msg: "Blocking...");
                            Future.delayed(Duration(seconds: 2), () {
                              showProgress(false, context);
                              showMessage(
                                  context,
                                  Icons.block,
                                  blue0,
                                  "Blocked!",
                                  "This person has been blocked. Changes will apply when you restart your App",
                                  delayInMilli: 500, onClicked: (_) {
                                Navigator.pop(context);
                              }, cancellable: false);
                            });
                          }
                        });
                      }
                    });
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    margin: EdgeInsets.only(top: 5, right: 5),
                    child: Center(
                        child: Icon(
                      Icons.flag,
                      color: white,
                      size: 25,
                    )),
                  )),
            ],
          ),
        ),
      ],
    );
  }

  photoBox(List<BaseModel> photos, {String type = "normal"}) {
    //final hookUpPhotos = theUser.hookUpPhotos;
    return Column(
      children: [
        if (photos.isNotEmpty)
          Container(
            height: 240,
            child: LayoutBuilder(
              builder: (ctx, b) {
                int photoLength = photos.length;
                return Column(
                  children: <Widget>[
                    Flexible(
                      child: ListView.builder(
                          key: const ValueKey('sppList'),
                          itemCount: photoLength,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (ctx, p) {
                            BaseModel photo = photos[p];
                            bool isVideo = photo.isVideo;
                            String imageUrl = photo
                                .getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);
                            bool isLocal = photo.isLocal;

                            List visibleList =
                                List.from(theUser.getList(VISIBLE_TO));
                            bool isVisible =
                                visibleList.contains(userModel.getUserId());

                            bool isPublic = photo.getInt(VISIBILITY) == 0;

                            return GestureDetector(
                              onTap: () {
                                if (isVideo) {
                                  if (photo.imageUrl.isEmpty) return;
                                  pushAndResult(
                                      context,
                                      PlayVideo(
                                          photo.getObjectId(), photo.imageUrl));
                                } else {
                                  if ((type == "nah") &&
                                      !isPublic &&
                                      !isVisible) {
                                    showMessage(
                                        context,
                                        Icons.info,
                                        black,
                                        "Photo Private",
                                        "Please request access from this user to view photo");
                                    return;
                                  }

//                                  final visiblePhotos = photos.where((e) {
//                                    return e.getInt(VISIBILITY) == 0 &&
//                                        isVisible;
//                                  }).toList();
//
//                                  if (visiblePhotos.isEmpty) return;

                                  pushAndResult(
                                      context, ViewImage([imageUrl], 0),
                                      depend: false);
                                }
                              },
                              child: Stack(
                                alignment: Alignment.center,
                                children: <Widget>[
                                  Opacity(
                                    /* opacity: (type == "normal")
                                        ? 1
                                        : isPublic ? 1 : isVisible ? 1 : 0.15,*/
                                    opacity: 1,
                                    child: Container(
                                      margin: EdgeInsets.all(8),
                                      child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          child: Stack(
                                            children: [
                                              Container(
                                                  height: 200,
                                                  width: 160,
                                                  child: Center(
                                                      child: Icon(
                                                    Icons.person,
                                                    color: white,
                                                    size: 15,
                                                  )),
                                                  decoration: BoxDecoration(
                                                    color:
                                                        black.withOpacity(.09),
                                                    //shape: BoxShape.circle
                                                  )),
                                              Image.network(
                                                imageUrl,
                                                fit: BoxFit.cover,
                                                height: 200,
                                                width: 160,
                                              ),
                                            ],
                                          )),
                                    ),
                                  ),
                                  if ((type == "nah") &&
                                      !isPublic &&
                                      !isVisible)
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Container(
                                        height: 200,
                                        width: 160,
                                        alignment: Alignment.center,
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: black.withOpacity(.5),
                                              shape: BoxShape.circle,
                                              border: Border.all(color: white)),
                                          padding: EdgeInsets.all(10),
                                          child: Icon(
                                            Icons.https,
                                            color: white,
                                          ),
                                        ),
                                        decoration: BoxDecoration(
                                          //color: AppConfig.appColor,
                                          gradient: LinearGradient(
                                              colors: [
                                                orange01.withOpacity(.9),
                                                orange04.withOpacity(.9),
                                                //AppConfig.appColor.withOpacity(.7),
                                              ],
                                              begin: Alignment.topLeft,
                                              end: Alignment.bottomRight),
                                        ),
                                      ),
                                    ),
                                  if (isVideo)
                                    Center(
                                      child: Container(
                                        height: 50,
                                        width: 50,
                                        child: Icon(
                                          Icons.play_arrow,
                                          color: Colors.white,
                                        ),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.black.withOpacity(0.8),
                                            border: Border.all(
                                                color: Colors.white,
                                                width: 1.5),
                                            shape: BoxShape.circle),
                                      ),
                                    ),
                                ],
                              ),
                            );
                          }),
                    ),
                  ],
                );
              },
            ),
          ),
      ],
    );
  }
}
