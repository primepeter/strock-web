import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainWebHome.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'ShowProfile.dart';

//0 -- my matches
//1 -- People i viewed
//2 -- Recommended Matches
//3 -- SuperLikes

class PeopleItems extends StatefulWidget {
  final int type;
  final bool webMode;

  const PeopleItems(this.type, this.webMode);
  @override
  _PeopleItemsState createState() => _PeopleItemsState();
}

class _PeopleItemsState extends State<PeopleItems> {
  final refreshController = RefreshController(initialRefresh: false);
  bool canRefresh = true;
  bool setup = false;
  List<BaseModel> items = [];

  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    overlayController.add(false);
    items = listType;
    setup = listType.isNotEmpty;
    setState(() {});
    // loadItems(false);
    scrollController.addListener(() {
      double pixels = scrollController.position.pixels;
      double maxScrollExtent = scrollController.position.maxScrollExtent;
      if (loadingMore) return;
      if (pixels / maxScrollExtent > 0.33) {
        loadingMore = true;
        loadMoreText = "Loading More...";
        setState(() {});
        loadItems(false);
      }
    });
  }

  @override
  dispose() {
    super.dispose();
    scrollController?.dispose();
  }

  loadItems(bool isNew) async {
    String key = MATCHED_LIST;
    if (widget.type == 1) key = PROFILE_VIEWS;
    if (widget.type == 3) key = SUPER_LIKE_LIST;
    final ref = FirebaseFirestore.instance.collection(USER_BASE);
    Query query = ref.where(key, arrayContains: userModel.getUserId());
    if (widget.type == 2)
      query = ref.where(GENDER, isEqualTo: userModel.getInt(PREFERENCE));

    query
        .limit(50)
        .orderBy(TIME, descending: !isNew)
        .startAt([
          !isNew
              ? (items.isEmpty
                  ? DateTime.now().millisecondsSinceEpoch
                  : items[items.length - 1].getTime())
              : (items.isEmpty ? 0 : items[0].getTime())
        ])
        .get()
        .then((shots) {
          for (var doc in shots.docs) {
            BaseModel model = BaseModel(doc: doc);
            if (!model.signUpCompleted) continue;
            int p =
                items.indexWhere((e) => e.getObjectId() == model.getObjectId());
            if (p == -1)
              items.add(model);
            else
              items[p] = model;
          }

          if (!isNew) {
            int oldLength = items.length;
            int newLength = shots.docs.length;
            if (newLength <= oldLength) {
              //refreshController.loadNoData();
              loadMoreText = "No more data!";
              //canRefresh = false;
            }
          }

          setup = true;
          loadingMore = false;
          updateParentList();
          if (mounted) setState(() {});
        })
        .catchError((e) {
          checkError(context, e);
        });
  }

  List<BaseModel> get listType {
    int type = widget.type;
    if (type == 0) return matchesList;
    if (type == 1) return seenList;
    if (type == 2) return meetMeList;
    if (type == 3) return superLikesList;
    return seenByList;
  }

  updateParentList() {
    int type = widget.type;
    if (type == 0) matchesList = items;
    if (type == 1) seenList = items;
    if (type == 2) meetMeList = items;
    if (type == 3) superLikesList = items;
  }

  bool loadingMore = false;
  String loadMoreText = "Loading More...";
  handleLoadMore(ScrollNotification scroll) {
    if (scroll.metrics.pixels / scroll.metrics.maxScrollExtent > 0.33) {
      if (loadingMore) return;
      loadingMore = true;
      loadMoreText = "Loading More...";
      setState(() {});
      loadItems(false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: body(),
    );
  }

  body() {
    if (!setup)
      return Center(
          child: SizedBox(
        height: 25,
        width: 25,
        child: CircularProgressIndicator(
          strokeWidth: 2,
          valueColor: AlwaysStoppedAnimation(appColor),
        ),
      ));

    if (items.isEmpty)
      return Center(
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(
                Icons.person,
                color: appColor,
                size: 50,
              ),
              Text(
                "No Persons found",
                style: textStyle(true, 16, black),
                textAlign: TextAlign.center,
              ),
              FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  color: appColor,
                  onPressed: () {
                    loadingMore = setup = false;
                    setState(() {});
                    loadItems(true);
                  },
                  child: Text(
                    "Refresh",
                    style: textStyle(true, 14, white),
                  ))
            ],
          ),
        ),
      );

    return RepaintBoundary(
      child: Column(
        children: [
          Expanded(
            child: GridView.builder(
              key: ValueKey('ppleItems${widget.type}'),
              itemBuilder: (c, p) {
                return personItem(p);
              },
              controller: scrollController,
              cacheExtent: 999999999999999.0,
              physics: AlwaysScrollableScrollPhysics(),
              //shrinkWrap: true,
              itemCount: items.length,

              padding: EdgeInsets.only(top: 10, right: 5, left: 5, bottom: 40),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: widget.webMode ? 8 : 2,
                  childAspectRatio: 0.8,
                  crossAxisSpacing: 5,
                  mainAxisSpacing: 5),
            ),
          ),
          if (loadingMore)
            Container(
              padding: EdgeInsets.all(10),
              alignment: Alignment.center,
              child: Text(
                loadMoreText,
                style: textStyle(true, 11, black),
                textAlign: TextAlign.center,
              ),
            ),
        ],
      ),
    );
  }

  personItem(int p) {
    BaseModel model = items[p];

    bool isVideo = model.profilePhotos[0].isVideo;
    String imageUrl =
        model.profilePhotos[0].getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);
    bool loadImages = appSettingsModel.getBoolean("loadImages");

    /*return Card(
      color: appColor,
      child: Center(
        child: Text(
          "$p",
          style: textStyle(true, 20, black),
        ),
      ),
    );*/

    return GestureDetector(
      onDoubleTap: () {
        appSettingsModel
          ..put("loadImages", !loadImages)
          ..updateItems();
        setState(() {});
      },
      onTap: () {
        pushAndResult(
            context,
            ShowProfile(
              theUser: model,
              //fromMeetMe: widget.fromStrock,
            ));
      },
      child: Card(
        color: white,
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: Stack(
                  children: [
                    Container(
                        height: double.infinity,
                        width: double.infinity,
                        child: Center(
                            child: Icon(
                          Icons.person,
                          color: white,
                          size: 15,
                        )),
                        decoration: BoxDecoration(
                          color: black.withOpacity(.09),
                          //shape: BoxShape.circle
                        )),
                    if (loadImages && imageUrl.startsWith("http"))
                      Image.network(
                        imageUrl,
                        key: ValueKey('img_ppleItems${widget.type}'),
                        fit: BoxFit.cover,
                        width: double.infinity,
                        height: double.infinity,
                      ),

                    // FadeInImage.assetNetwork(
                    //   width: double.infinity,
                    //   height: double.infinity,
                    //   fit: BoxFit.cover,
                    //   placeholder: 'assets/icons/ic_users.png',
                    //   placeholderCacheWidth: 30,
                    //   placeholderCacheHeight: 30,
                    //
                    //   image: imageUrl,
                    // ),
                    if (isVideo)
                      Center(
                        child: Container(
                          height: 50,
                          width: 50,
                          child: Icon(
                            Icons.play_arrow,
                            color: Colors.white,
                          ),
                          decoration: BoxDecoration(
                              color: Colors.black.withOpacity(0.8),
                              border:
                                  Border.all(color: Colors.white, width: 1.5),
                              shape: BoxShape.circle),
                        ),
                      ),
                  ],
                ),
              ),
            ),
            Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Flexible(
                          fit: FlexFit.loose,
                          child: Text(
                            getFirstName(model),
                            style: textStyle(true, 14, black),
                            maxLines: 1,
                            textAlign: TextAlign.center,
                          ),
                        ),
                        addSpaceWidth(5),
                        if (isOnline(model))
                          Container(
                            height: 8,
                            width: 8,
                            decoration: BoxDecoration(
                                color: green, shape: BoxShape.circle),
                          ),
                        Text(
                          getMyAge(model).toString(),
                          style: textStyle(false, 11, black.withOpacity(.5)),
                        ),
                      ],
                    ),
//                    FlatButton(
//                      onPressed: () {
//                        clickChat(context, model, false);
//                      },
//                      padding: EdgeInsets.all(5),
//                      shape: RoundedRectangleBorder(
//                          side: BorderSide(color: black.withOpacity(.2))),
//                      child: Center(
//                        child: Text(
//                          "Chat Now",
//                          style: textStyle(false, 12, black.withOpacity(.7)),
//                        ),
//                      ),
//                    )
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
