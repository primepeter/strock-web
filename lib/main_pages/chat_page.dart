import 'dart:async';
import 'dart:html'
    as html; // importing the HTML proxying library and named it as html
import 'dart:ui';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/ChatMainNew.dart';
import 'package:Strokes/MainWebHome.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:Strokes/dialogs/listDialog.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:synchronized/synchronized.dart';

PageController chatPageController = new PageController();
List<BaseModel> notifyList = List();
List otherParties = List();

class MessagesPage extends StatefulWidget {
  final bool showBar;

  const MessagesPage({Key key, this.showBar = true}) : super(key: key);

  @override
  _MessagesPageState createState() => _MessagesPageState();
}

class _MessagesPageState extends State<MessagesPage>
    with AutomaticKeepAliveClientMixin {
  //bool setup = false;
  List subs = [];
  @override
  void initState() {
    super.initState();
    var sub = chatMessageController.stream.listen((_) {
      if (mounted) setState(() {});
    });
    subs.add(sub);
    loadMessages();
  }

  @override
  void dispose() {
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  startRefreshingMessages() {
    Future.delayed(Duration(seconds: 2), () {
      if (mounted) setState(() {});
      startRefreshingMessages();
    });
  }

  loadMessages() async {
    //var lock = Lock();
    // await lock.synchronized(() async {
    await Future.delayed(Duration(seconds: 1));
    var sub = FirebaseFirestore.instance
        .collection(CHAT_IDS_BASE)
        .where(PARTIES, arrayContains: userModel.getObjectId())
        .snapshots()
        .listen((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel chatIdModel = BaseModel(doc: doc);
        String chatId = chatIdModel.getObjectId();
        List partyModels = chatIdModel.getList(PARTIES_MODEL);
        List parties = chatIdModel.getList(PARTIES);

        if (userModel.getList(DELETED_CHATS).contains(chatId)) continue;
        if (loadedIds.contains(chatId)) {
          continue;
        }
        // if (partyModels.isEmpty || partyModels.length != parties.length) {
        //   loadPartyUpdate(chatIdModel);
        // }
        //loadPartyUpdate(chatIdModel);
        loadedIds.add(chatId);
        var sub = FirebaseFirestore.instance
            .collection(CHAT_BASE)
            .where(PARTIES, arrayContains: userModel.getUserId())
            .where(CHAT_ID, isEqualTo: chatId)
            .orderBy(TIME, descending: true)
            .limit(1)
            .snapshots()
            .listen((shots) async {
          if (shots.docs.isNotEmpty) {
            BaseModel cModel = BaseModel(doc: (shots.docs[0]));
            if (isBlocked(null, userId: getOtherPersonId(cModel))) {
              lastMessages.removeWhere(
                  (bm) => bm.getString(CHAT_ID) == cModel.getString(CHAT_ID));
              chatMessageController.add(true);
              return;
            }
          }
          if (stopListening.contains(chatId)) return;
          for (DocumentSnapshot doc in shots.docs) {
            if (!doc.exists) continue;
            BaseModel model = BaseModel(doc: doc);
            String chatId = model.getString(CHAT_ID);
            model.put(PARTIES_MODEL, partyModels);
            int index = lastMessages.indexWhere(
                (bm) => bm.getString(CHAT_ID) == model.getString(CHAT_ID));
            String otherPartyId = getOtherPersonId(model);
            int partyIndex =
                otherParties.indexWhere((e) => e[OBJECT_ID] == otherPartyId);
            if (partyIndex == -1) loadOtherPerson(otherPartyId);
            if (index == -1) {
              lastMessages.add(model);
            } else {
              lastMessages[index] = model;
            }

            if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
                !model.myItem() &&
                visibleChatId != model.getString(CHAT_ID)) {
              try {
                if (!showNewMessageDot.contains(chatId))
                  showNewMessageDot.add(chatId);
                if (mounted) setState(() {});
              } catch (E) {
                if (!showNewMessageDot.contains(chatId))
                  showNewMessageDot.add(chatId);
                if (mounted) setState(() {});
              }
              //countUnread(chatId);
            }
          }

          try {
            lastMessages
                .sort((bm1, bm2) => bm2.getTime().compareTo(bm1.getTime()));
          } catch (E) {}
          chatSetup = true;
          if (mounted) setState(() {});
        });

        subs.add(sub);
      }
    });
    subs.add(sub);
    // });
  }

  loadPartyUpdate(BaseModel chatIdModel) async {
    List partyModels = List.from(chatIdModel.getList(PARTIES_MODEL));
    List parties = chatIdModel.getList(PARTIES);

    for (int p = 0; p < parties.length; p++) {
      String userId = parties[p];
      //if (userId == userModel.getUserId()) continue;
      FirebaseFirestore.instance
          .collection(USER_BASE)
          .doc(userId)
          .get()
          .then((value) {
        String chatId = chatIdModel.getObjectId();

        int index =
            lastMessages.indexWhere((bm) => bm.getString(CHAT_ID) == chatId);

        if (!value.exists) {
          lastMessages.removeWhere((bm) => bm.getString(CHAT_ID) == chatId);
          setState(() {});
          return;
        }

        BaseModel model = BaseModel(doc: value);
        int pp =
            partyModels.indexWhere((e) => e[OBJECT_ID] == model.getUserId());
        if (pp == -1)
          partyModels.add(chatPartyModel(model));
        else
          partyModels[p] == chatPartyModel(model);

        chatIdModel.put(PARTIES_MODEL, partyModels);
        chatIdModel.updateItems(delaySeconds: 1);

        if (index != -1) {
          BaseModel chatModel = lastMessages[index];
          chatModel.put(PARTIES_MODEL, partyModels);
          chatModel.updateItems(delaySeconds: 1);
          lastMessages[index] = chatModel;
        }
        if (mounted) setState(() {});
      });
    }
  }

  loadOtherPerson(
    String uId,
  ) async {
    if (uId.isEmpty) return;
    Firestore.instance.collection(USER_BASE).document(uId).get().then((doc) {
      if (doc == null) return;
      if (!doc.exists) return;
      BaseModel user = BaseModel(doc: doc);
      otherPeronInfo[uId] = user;
      otherParties.add(chatPartyModel(user));
      if (mounted) setState(() {});
    });
  }

  countUnread(String chatId) async {
    var lock = Lock();
    lock.synchronized(() async {
      int count = 0;
      print("Counting Unread: $chatId");
      QuerySnapshot shots = await FirebaseFirestore.instance
          .collection(CHAT_BASE)
          .where(CHAT_ID, isEqualTo: chatId)
          .limit(10)
          .orderBy(TIME, descending: true)
          .get();
      List list = [];
      for (DocumentSnapshot doc in shots.docs) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) break;
        if (chatRemoved(model)) continue;
        if (model.getUserId().isEmpty) continue;
        if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
            !model.myItem()) {
          list.add(model);
          count++;
        }
      }
      unreadCounter[chatId] = list;
      chatMessageController.add(true);
    });
  }

  //git remote add strock https://mtellect@bitbucket.org/primepeter/strock-app.git

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          // CachedNetworkImage(
          //   imageUrl: userModel.getString(USER_IMAGE),
          //   fit: BoxFit.cover,
          //   height: MediaQuery.of(context).size.height,
          // ),
          // BackdropFilter(
          //     filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
          //     child: Container(
          //       color: black.withOpacity(.6),
          //     )),
          page()
        ],
      ),
    );
  }

  page() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        if (widget.showBar)
          Container(
            padding: EdgeInsets.fromLTRB(0, 5, 0, 10),
            color: white,
            child: Row(
              children: <Widget>[
                InkWell(
                    onTap: () {
                      Navigator.of(context).pop("");
                    },
                    child: Container(
                      width: 50,
                      height: 50,
                      child: Center(
                          child: Icon(
                        Icons.keyboard_backspace,
                        color: black,
                        size: 25,
                      )),
                    )),
                Text(
                  "Messages",
                  style: textStyle(true, 25, black),
                ),
                Spacer()
              ],
            ),
          ),
        if (!widget.showBar)
          Container(
            color: white,
            child: Row(
              children: [
                InkWell(
                    onTap: () {
                      Navigator.of(context).pop("");
                    },
                    child: Container(
                      width: 50,
                      height: 50,
                      child: Center(
                          child: Icon(
                        Icons.keyboard_backspace,
                        color: black,
                        size: 25,
                      )),
                    )),
                Flexible(
                  child: Text(
                    "Messages",
                    style: textStyle(true, 25, black),
                  ),
                ),
              ],
            ),
          ),
        Expanded(
            flex: 1,
            child: Builder(builder: (ctx) {
              if (!chatSetup) return loadingLayout();
              if (lastMessages.isEmpty)
                return Center(
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Image.asset(
                          ic_chat,
                          width: 50,
                          height: 50,
                          color: AppConfig.appColor,
                        ),
                        Text(
                          "No Chat Yet",
                          style: textStyle(true, 20, black),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                );

              return Container(
                  child: ListView.builder(
                key: const ValueKey('cpList'),
                itemBuilder: (c, p) {
                  BaseModel model = lastMessages[p];

                  String chatId = model.getString(CHAT_ID);
                  String otherPersonId = getOtherPersonId(model);
                  List partyModels = model.getList(PARTIES_MODEL);
                  Map otherPersonItem = partyModels.singleWhere(
                      (e) => e[OBJECT_ID] == otherPersonId,
                      orElse: () => {});
                  BaseModel otherPerson = BaseModel(items: otherPersonItem);

                  String name = otherPerson.getString(NAME);
                  String image = otherPerson.getString(IMAGE_URL);
                  if (image.isEmpty) image = otherPerson.userImage;
                  if (name.isEmpty) name = "No Name";

                  if (otherPersonItem.isEmpty) return Container();

                  return chatItem(image, name, model,
                      p == lastMessages.length - 1, otherPerson);
                },
                shrinkWrap: true,
                itemCount: lastMessages.length,
                padding: EdgeInsets.all(0),
              ));
            }))
      ],
    );
  }

  chatItem(String image, String name, BaseModel chatModel, bool last,
      BaseModel otherPerson) {
    int type = chatModel.getInt(TYPE);
    String chatId = chatModel.getString(CHAT_ID);
    bool myItem = chatModel.myItem();

    String hisId = chatId.replaceAll(userModel.getObjectId(), "");
    bool read = chatModel.getList(READ_BY).contains(hisId);
    bool myRead = chatModel.getList(READ_BY).contains(userModel.getObjectId());
    List mutedList = userModel.getList(MUTED);
    int unread =
        unreadCounter[chatId] == null ? 0 : unreadCounter[chatId].length;

    return new InkWell(
      onLongPress: () {
        pushAndResult(
            context,
            listDialog([
              mutedList.contains(chatId) ? "Unmute Chat" : "Mute Chat",
              "Delete Chat"
            ]), result: (_) {
          if (_ == "Mute Chat" || _ == "Unmute Chat") {
            if (mutedList.contains(chatId)) {
              mutedList.remove(chatId);
            } else {
              mutedList.add(chatId);
            }
            userModel.put(MUTED, mutedList);
            userModel.updateItems();
            setState(() {});
          }
          if (_ == "Delete Chat") {
            yesNoDialog(context, "Delete Chat?",
                "Are you sure you want to delete this chat?", () {
              deleteChat(chatId);
            });
          }
        }, depend: false);
      },
      onTap: () {
        BaseModel chat = BaseModel();
        chat.put(PARTIES, [userModel.getObjectId(), otherPerson.getObjectId()]);
        chat.put(PARTIES_MODEL, [chatPartyModel(userModel), otherPerson]);
        chat.saveItem(CHAT_IDS_BASE, false, document: chatId);
        chatModel.putInList(READ_BY, userModel.getObjectId(), true);
        chatModel.updateItems();
        unreadCounter.remove(chatId);
        showNewMessageDot.removeWhere((id) => id == chatId);
        FirebaseFirestore.instance.collection(CHAT_IDS_BASE).doc(chatId).set({
          PARTIES_MODEL: [chatPartyModel(userModel), otherPerson]
        }, SetOptions(merge: true));

        // return;
        pushAndResult(
            context,
            ChatMainNew(
              chatId,
              otherPerson: otherPerson,
            ), result: (_) {
          setState(() {});
        });
        return;
        html.window.alert(otherPerson.items.toString() + " $chatId");
      },
      //margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: Container(
        decoration: BoxDecoration(
            color: white,
            boxShadow: [BoxShadow(color: black.withOpacity(.1), blurRadius: 5)],
            borderRadius: BorderRadius.circular(5)),
        padding: EdgeInsets.fromLTRB(10, 5, 10, 0),
        margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
//                  margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                  width: 80,
                  height: 80,
                  child: Card(
                    color: black.withOpacity(.1),
                    elevation: 0,

                    clipBehavior: Clip.antiAlias,
                    shape: CircleBorder(
                        side: BorderSide(
                            color: black.withOpacity(.2), width: .9)),
                    // shape: RoundedRectangleBorder(
                    //     borderRadius: BorderRadius.all(Radius.circular(10)),
                    //     side: BorderSide(
                    //         color: black.withOpacity(.1), width: .8)),
                    child: (otherPerson.getBoolean(IS_ADMIN))
                        ? Image.asset(
                            ic_launcher,
                            width: 40,
                            height: 40,
                            fit: BoxFit.cover,
                          )
                        : Image.network(
                            image,
                            fit: BoxFit.cover,
                            width: 40,
                            height: 40,
                          ),
                  ),
                ),
                addSpaceWidth(10),
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: new Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Text(
                              //"Emeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",
                              (otherPerson.getBoolean(IS_ADMIN))
                                  ? "Strock Support"
                                  : name,
                              maxLines: 1, overflow: TextOverflow.ellipsis,
                              style: textStyle(true, 20, black),
                            ),
                          ),
                          addSpaceWidth(5),
                          !myItem && !myRead && unread > 0
                              ? /*Icon(
                                  Icons.new_releases,
                                  size: 20,
                                  color: white,
                                )*/
                              (Container(
                                  width: 25,
                                  height: 25,
                                  decoration: BoxDecoration(
                                    color: AppConfig.appColor,
                                    shape: BoxShape.circle,
//                                      border:
//                                          Border.all(color: white, width: 2)
                                  ),
                                  child: Center(
                                      child: Text(
                                    "${unread > 9 ? "9+" : unread}",
                                    style: textStyle(true, 12, white),
                                  )),
                                ))
                              : Container(),
                          addSpaceWidth(5),
                          Text(
                            getChatTime(chatModel.getTime()),
                            style: textStyle(false, 12, black.withOpacity(.8)),
                            textAlign: TextAlign.end,
                          ),

                          //addSpaceWidth(5),
                        ],
                      ),
                      addSpace(5),
                      Row(
                        children: <Widget>[
                          Flexible(
                            fit: FlexFit.tight,
                            child: Row(
                              children: <Widget>[
                                Flexible(
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      if (myItem && read)
                                        Container(
                                            margin: EdgeInsets.only(right: 5),
                                            child: Icon(
                                              Icons.remove_red_eye,
                                              size: 12,
                                              color: blue0,
                                            )),
                                      Icon(
                                        type == CHAT_TYPE_TEXT
                                            ? Icons.message
                                            : type == CHAT_TYPE_IMAGE
                                                ? Icons.camera_alt
                                                : type == CHAT_TYPE_VIDEO
                                                    ? Icons.videocam
                                                    : type == CHAT_TYPE_REC
                                                        ? Icons.mic
                                                        : Icons.library_books,
                                        color: black.withOpacity(.8),
                                        size: 12,
                                      ),
                                      addSpaceWidth(5),
                                      Flexible(
                                        flex: 1,
                                        child: Text(
                                          chatRemoved(chatModel)
                                              ? "This message has been removed"
                                              : type == CHAT_TYPE_TEXT
                                                  ? chatModel.getString(MESSAGE)
                                                  : type == CHAT_TYPE_IMAGE
                                                      ? "Photo"
                                                      : type == CHAT_TYPE_VIDEO
                                                          ? "Video"
                                                          : type ==
                                                                  CHAT_TYPE_REC
                                                              ? "Voice Note (${chatModel.getString(AUDIO_LENGTH)})"
                                                              : "Document",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: textStyle(
                                              false, 14, black.withOpacity(.8)),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          if (mutedList.contains(chatId))
                            Image.asset(
                              ic_mute,
                              width: 18,
                              height: 18,
                              color: white.withOpacity(.5),
                            )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
            addSpace(5),
            addLine(.5, white.withOpacity(.3), 0, 0, 0, 0)
          ],
        ),
      ),
    );
  }

  deleteChat(String chatId) {
    lastMessages.removeWhere((bm) => bm.getString(CHAT_ID) == chatId);
    stopListening.add(chatId);
    userModel.putInList(DELETED_CHATS, chatId, true);
    userModel.updateItems();
    if (mounted) setState(() {});

    Firestore.instance
        .collection(CHAT_BASE)
        .where(PARTIES, arrayContains: userModel.getUserId())
        .where(CHAT_ID, isEqualTo: chatId)
        .orderBy(TIME, descending: false)
        .getDocuments()
        .then((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel chat = BaseModel(doc: doc);
        if (chat.myItem()) {
          chat.put(DELETED, true);
          chat.updateItems();
        } else {
          List hidden = List.from(chat.getList(HIDDEN));
          hidden.add(userModel.getObjectId());
          chat.put(HIDDEN, hidden);
          chat.updateItems();
        }
      }
    });
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
