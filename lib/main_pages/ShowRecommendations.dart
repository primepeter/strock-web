import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainWebHome.dart';
import 'package:Strokes/assets.dart';
import 'package:flutter/material.dart';

import 'PeopleItems.dart';

class ShowRecommendations extends StatefulWidget {
  @override
  _ShowRecommendationsState createState() => _ShowRecommendationsState();
}

class _ShowRecommendationsState extends State<ShowRecommendations> {
  final pc = PageController();
  int currentPage = 0;

  @override
  void initState() {
    super.initState();
    overlayController.add(false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: default_white,
      body: page(),
    );
  }

  page() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(0, 5, 0, 10),
          color: white,
          child: Column(
            children: [
              Row(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: Icon(
                        Icons.keyboard_backspace,
                        color: black,
                        size: 25,
                      )),
                  Flexible(
                    child: Container(
                      //height: 50,
//                  width: 270,
                      margin: EdgeInsets.fromLTRB(0, 0, 50, 0),
                      child: Card(
                        color: black.withOpacity(0.2),
                        clipBehavior: Clip.antiAlias,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(25))),
                        child: Row(
                          children: List.generate(2, (p) {
                            String title =
                                p == 0 ? "Recommended" : "Super Likes";
                            bool selected = p == currentPage;
                            return Flexible(
                              child: GestureDetector(
                                onTap: () {
                                  if (p == 1 && !userModel.isPremium) {
                                    showMessage(
                                        context,
                                        Icons.error,
                                        red0,
                                        "Opps Sorry!",
                                        "You cannot view persons who SuperLiked your profile until you become a Premium User",
                                        textSize: 14,
                                        clickYesText: "Subscribe",
                                        onClicked: (_) {
                                      if (_) {
                                        openSubscriptionPage(context);
                                      }
                                    }, clickNoText: "Cancel");

                                    return;
                                  }

                                  pc.animateToPage(p,
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.ease);
                                },
                                child: Container(
                                    margin: EdgeInsets.all(4),
                                    height: 30,
                                    decoration: !selected
                                        ? null
                                        : BoxDecoration(
                                            color:
                                                selected ? white : transparent,
//                      border: Border.all(color: black.withOpacity(.1),width: 3),
                                            borderRadius:
                                                BorderRadius.circular(25)),
                                    child: Center(
                                        child: Text(
                                      title,
                                      style: textStyle(
                                          true,
                                          14,
                                          selected
                                              ? black
                                              : (white.withOpacity(.7))),
                                      textAlign: TextAlign.center,
                                    ))),
                              ),
                              fit: FlexFit.tight,
                            );
                          }),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        Expanded(
            flex: 1,
            child: PageView(
              controller: pc,
              onPageChanged: (p) {
                if (p == 1 && !userModel.isPremium) {
                  showMessage(context, Icons.error, red0, "Opps Sorry!",
                      "You cannot view persons who SuperLiked your profile until you become a Premium User",
                      textSize: 14, clickYesText: "Subscribe", onClicked: (_) {
                    if (_) {
                      openSubscriptionPage(context);
                    }
                  }, clickNoText: "Cancel");

                  return;
                }
                setState(() {
                  currentPage = p;
                });
              },
              physics:
                  userModel.isPremium ? null : NeverScrollableScrollPhysics(),
              children: [PeopleItems(2, false), PeopleItems(3, false)],
            ))
      ],
    );
  }
}
