import 'dart:async';
import 'dart:convert';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/FilterDialog.dart';
import 'package:Strokes/MainWebHome.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:http/http.dart' as http;

import 'MyProfile.dart';
import 'ShowMatches.dart';
import 'ShowProfile.dart';
import 'chat_page.dart';

BuildContext baseContext;

class NearbySearch extends StatefulWidget {
  final bool fromStrock;
  final int currentPage;

  const NearbySearch({Key key, this.fromStrock = false, this.currentPage = 0})
      : super(key: key);
  @override
  _NearbySearchState createState() => _NearbySearchState();
}

class _NearbySearchState extends State<NearbySearch> {
  bool setup = false;
  List peopleList = [];
  BaseModel filterLocation;
  int genderType = userModel.getInt(PREFERENCE); //-1;
  int onlineType = -1;
  int interestType = -1;
  int minAge = 18;
  int maxAge = 80;
  bool filtering = false;

  PageController pc;
  int currentPage = 0;

  List<StreamSubscription> subs = List();

  @override
  void initState() {
    super.initState();

    var prefSub = preferenceController.stream.listen((b) {
      setup = false;
      peopleList.clear();
      setState(() {});
      loadItems();
    });
    subs.add(prefSub);

    currentPage = widget.currentPage;
    pc = PageController(initialPage: widget.currentPage);
    loadItems();
  }

  var loadingSub;
  loadItems() async {
    /* if (loadingSub != null) {
      loadingSub.cancel();
      peopleList.clear();
      setup = false;
      setState(() {});
    }*/

    //loadingSub.cancel();
    peopleList.clear();
    setup = false;
    setState(() {});

    Geoflutterfire geo = Geoflutterfire();
    double myLat = userModel.getDouble(LATITUDE);
    double myLong = userModel.getDouble(LONGITUDE);

    Map myPosition = userModel.getMap(POSITION);
    if (myPosition.isEmpty) {
      showMessage(context, Icons.error, red0, "No Location",
          "Sorry we could no find your location",
          onClicked: (_) => Navigator.pop(context));
      return;
    }
    GeoPoint myGeoPoint = myPosition["geopoint"];
    double lat = myGeoPoint.latitude;
    double lon = myGeoPoint.longitude;

    if (filterLocation != null) {
      lat = filterLocation.getDouble(LATITUDE);
      lon = filterLocation.getDouble(LONGITUDE);
    }

    GeoFirePoint center = geo.point(latitude: lat, longitude: lon);
    final collectionReference =
        FirebaseFirestore.instance.collection(USER_BASE);

    // double radius = 10000;
    double radius = appSettingsModel.getDouble(NEARBY_RADIUS);
    double limit = appSettingsModel.getDouble(MILES_LIMIT);
    bool useLimit = appSettingsModel.getBoolean(USE_RADIUS_LIMIT);
    bool useNearby = appSettingsModel.getBoolean(USE_NEARBY_LIMIT);

    stateNameFromLat(myLat, myLong);

    /*loadingSub = */ geo
        .collection(collectionRef: collectionReference)
        .within(center: center, radius: radius, field: POSITION)
        .first
        .then((event) async {
      for (DocumentSnapshot doc in event) {
        BaseModel model = BaseModel(doc: doc);
        if (model.myItem()) continue;
        if (!model.signUpCompleted) continue;
        if (userModel.getList(BLOCKED).contains(model.getObjectId())) continue;
        if (genderType != -1) {
          if ((genderType == 0 || genderType == 1) &&
              model.getInt(GENDER) != genderType) continue;
        }
        // else {
        //   if (model.getInt(GENDER) != userModel.getInt(PREFERENCE)) continue;
        // }
        //if (model.getInt(ETHNICITY) != userModel.getInt(ETHNICITY)) continue;
        if (onlineType > 0) {
          if (onlineType == 1) if (!isOnline(model)) continue;
          int now = DateTime.now().millisecondsSinceEpoch;
          if (onlineType == 2) if ((now - (model.getInt(TIME))) >
              Duration.millisecondsPerSecond) continue;
        }

        if (minAge != -1) {
          int age = getAge(DateTime.parse(model.getString(BIRTH_DATE)));
          if (minAge > age) continue;
        }
        if (maxAge != -1) {
          int age = getAge(DateTime.parse(model.getString(BIRTH_DATE)));
          if (maxAge < age) continue;
        }
        if (interestType != -1) {
          if (model.getInt(RELATIONSHIP) != interestType) continue;
        }

        //if (model.getMap(POSITION).isEmpty) continue;

        final geoPoint = model.getModel(POSITION).get("geopoint") as GeoPoint;
        final distance =
            (await calculateDistanceTo(geoPoint, myGeoPoint) * 0.001);
        model.put(DISTANCE, distance);

        int index = peopleList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (index == -1) peopleList.add(model);
      }
      //loadingSub.cancel();
      setup = true;
      if (mounted) setState(() {});
    });
  }

  Future<String> stateNameFromLat(double lat, double long) async {
    final String apiKey = "AIzaSyAdBH8Jt-ZIHs9iZdgBe5czWIK4lwRZRak";

    print("@hererrrr");

    String locationState = "";
    try {
      var endP =
          "https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&key=$apiKey";
      //"https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=$apiKey";
      var response =
          await http.get(endP, headers: {"Access-Control-Allow-Origin": "*"});

      print("response ${response.statusCode}");

      if (response.statusCode != 200) return "";

      Map<String, dynamic> results = jsonDecode(response.body)['results'][0];

      if (null == results || results.isEmpty) return "";
      print("results $results");
      List addressComponent = results['address_components'];

      if (addressComponent.isEmpty) return "";

      List state = addressComponent
          .where((e) => BaseModel(items: e)
              .getList("types")
              .contains("administrative_area_level_1"))
          .toList();
      /*List country = addressComponent
        .where((e) => BaseModel(items: e).getList("types").contains("country"))
        .toList();*/
      if (state.isEmpty) return "";
      String stateComponent = state[0]["long_name"];
      // if (country.isEmpty) return "";
      // String countryComponent = country[0]["long_name"];
      locationState = stateComponent;
      //String locationCountry = countryComponent;
      print("locationState $locationState");
      locationState = locationState == null ? "" : locationState;
      return locationState.toLowerCase();
    } catch (e) {
      print("eeeeeeeeee $e");
      return "";
    }
  }

  @override
  void dispose() {
    for (var sub in subs) sub?.cancel();
    loadingSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  String get headerTitle {
    if (currentPage == 0)
      return widget.fromStrock ? "Quick Strock" : "Nearby Search";

    if (currentPage == 1) return "Matches";
    if (currentPage == 2) return "Chat";
    return "Profile";
  }

  page() {
    return Column(
      children: [
        //addSpace(30),
        pages(),
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: List.generate(4, (p) {
              if (p == 0) {
                return btmTab(p, Icons.home, "Explore", isAsset: false);
              }
              if (p == 1) {
                return btmTab(p, Icons.favorite, "Matches", isAsset: false);
              }
              if (p == 2) {
                return btmTab(p, Icons.message_outlined, "Chat",
                    isAsset: false);
              }
              return btmTab(p, Icons.person_outline, "Profile", isAsset: false);
            }))
      ],
    );
  }

  pages() {
    return Expanded(
      child: PageView(
        controller: pc,
        onPageChanged: (p) {
          setState(() {
            currentPage = p;
          });
        },
        children: [
          page1(),
          ShowMatches(
            showBar: false,
          ),
          MessagesPage(
            showBar: false,
          ),
          MyProfile(
            showBar: false,
          )
        ],
      ),
    );
  }

  page1() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
                child: Row(
              children: [
                FlatButton(
                    onPressed: () {
                      setup = false;
                      peopleList = [];
                      filterLocation = null;
                      genderType = -1;
                      onlineType = -1;
                      interestType = -1;
                      minAge = 18;
                      maxAge = 80;
                      filtering = false;
                      Navigator.of(context).pop();
                    },
                    minWidth: 50,
                    height: 50,
                    child: Container(
                      width: 50,
                      height: 50,
                      child: Center(
                          child: Icon(
                        Icons.keyboard_backspace,
                        color: black,
                        size: 25,
                      )),
                    )),
                Flexible(
                  child: Text(
                    headerTitle,
                    style: textStyle(true, 25, black),
                  ),
                ),
              ],
            )),
            IconButton(
                icon: Icon(Icons.sort),
                onPressed: () {
                  pushAndResult(
                      context,
                      FilterDialog(
                          filtering,
                          filterLocation,
                          genderType,
                          onlineType,
                          minAge,
                          maxAge,
                          interestType), result: (_) {
                    List items = _;
                    filterLocation = items[0];
                    genderType = items[1];
                    onlineType = items[2];
                    minAge = items[3];
                    maxAge = items[4];
                    interestType = items[5];
                    filtering = items[6];
                    loadItems();
                  }, depend: false);
                })
          ],
        ),
//

        Flexible(
          child: Builder(builder: (ctx) {
            if (!setup) return loadingLayout();

            if (peopleList.isEmpty)
              return Center(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
//                      Image.asset(
//                        "assets/icons/gender.png",
//                        width: 50,
//                        height: 50,
//                        color: AppConfig.appColor,
//                      ),
                      Icon(
                        Icons.location_off,
                        color: AppConfig.appColor,
                        size: 50,
                      ),
                      Text(
                        "No One Nearby",
                        style: textStyle(true, 20, black),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              );

            return StaggeredGridView.countBuilder(
              key: const ValueKey('nbList'),
              physics: AlwaysScrollableScrollPhysics(),
              crossAxisCount: 3,
              itemCount: peopleList.length,
              itemBuilder: (BuildContext context, int p) {
                BaseModel model = peopleList[p];

//            String image =
//                model.profilePhotos[model.getInt(DEF_PROFILE_PHOTO)].imageUrl;
                bool isVideo = false;
                int defPos = model.getInt(
                    widget.fromStrock ? DEF_STROCK_PHOTO : DEF_PROFILE_PHOTO);
                isVideo = model.profilePhotos[defPos].isVideo;
                String image = model.profilePhotos[defPos]
                    .getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);

                if (widget.fromStrock) {
                  image = model.hookUpPhotos[defPos].imageUrl;
                  isVideo = model.hookUpPhotos[defPos].isVideo;
                  image = model.hookUpPhotos[defPos]
                      .getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);
                }
                return Container(
                  margin: EdgeInsets.only(top: p == 1 ? 50 : 0),
                  width: double.infinity,
                  //alignment: Alignment.center,
                  child: Stack(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          GestureDetector(
                            onTap: () {
                              //clickChat(context, model, false);
                              pushAndResult(
                                  context,
                                  ShowProfile(
                                    theUser: model,
                                    fromMeetMe: widget.fromStrock,
                                  ));
                            },
                            child: Container(
                              height: 110,
                              width: double.infinity,
                              alignment: Alignment.center,
                              child: Stack(
                                children: [
                                  Align(
                                    alignment: Alignment.topCenter,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Stack(
                                        children: [
                                          Container(
                                            width: 100,
                                            height: 100,
                                            color: black.withOpacity(.05),
                                          ),
                                          if (image.contains("http"))
                                            Image.network(
                                              image,
                                              width: 100,
                                              height: 100,
                                              fit: BoxFit.cover,
                                            ),
                                          if (isOnline(model))
                                            Container(
                                              height: 15,
                                              width: 15,
                                              padding: EdgeInsets.all(2),
                                              // child: Text(
                                              //   "Online",
                                              //   style:
                                              //       textStyle(false, 10, white),
                                              // ),
                                              decoration: BoxDecoration(
                                                color: green,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(10),
                                                  bottomRight:
                                                      Radius.circular(10),
                                                ),

                                                //shape: BoxShape.circle
                                              ),
                                            ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Container(
                                        //alignment: Alignment.bottomCenter,
                                        padding:
                                            EdgeInsets.fromLTRB(5, 3, 5, 3),
                                        decoration: BoxDecoration(
                                            color: AppConfig.appColor,
                                            borderRadius:
                                                BorderRadius.circular(25),
                                            border: Border.all(
                                                color: white, width: 2)),
                                        child: Text(
                                          "${(model.getDouble(DISTANCE)).roundToDouble()} KM",
                                          style: textStyle(false, 12, white),
                                        )),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Text(
                            model.getString(NAME),
                            style: textStyle(true, 12, black),
                            maxLines: 1,
                            textAlign: TextAlign.center,
                          )
                        ],
                      ),
                      if (isVideo)
                        Center(
                          child: Container(
                            height: 25,
                            width: 25,
                            child: Icon(
                              Icons.play_arrow,
                              color: Colors.white,
                              size: 14,
                            ),
                            decoration: BoxDecoration(
                                color: Colors.black.withOpacity(0.8),
                                border:
                                    Border.all(color: Colors.white, width: 1.5),
                                shape: BoxShape.circle),
                          ),
                        ),
                    ],
                  ),
                );
              },
              padding: EdgeInsets.all(0),
              staggeredTileBuilder: (int index) =>
                  new StaggeredTile.extent(1, (index == 1) ? 180 : 130),
              shrinkWrap: true,
              mainAxisSpacing: 4.0,
              crossAxisSpacing: 4.0,
            );
          }),
        ),
      ],
    );
  }

  btmTab(int p, icon, String title, {bool isAsset = false}) {
    bool active = currentPage == p;
    final width = MediaQuery.of(context).size.width / 4;
    return Flexible(
      child: GestureDetector(
          onTap: () {
            setState(() {
              pc.jumpToPage(p);
            });
          },
          child: Container(
            width: width,
            height: 64,
            alignment: Alignment.center,
            color: transparent,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Builder(
                  builder: (ctx) {
                    if (isAsset)
                      return Image.asset(
                        icon,
                        height: 20,
                        width: 20,
                        color: black.withOpacity(active ? 1 : 0.5),
                      );
                    return Icon(
                      icon,
                      size: 20,
                      color: black.withOpacity(active ? 1 : 0.5),
                    );
                  },
                ),
                Text(
                  title,
                  style: textStyle(active, active ? 13 : 12,
                      black.withOpacity(active ? 1 : 0.5)),
                )
              ],
            ),
          )),
    );
  }
}
