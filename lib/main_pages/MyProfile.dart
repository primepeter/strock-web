import 'dart:html';
import 'dart:ui';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/AppSettings.dart';
import 'package:Strokes/admin/AppAdmin.dart';
import 'package:Strokes/app/ImageUtils.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:Strokes/dialogs/inputDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker_web_redux/image_picker_web_redux.dart';
import 'package:intl/intl.dart';

import '../MainWebHome.dart';

class MyProfile extends StatefulWidget {
  final bool showBar;

  const MyProfile({Key key, this.showBar = true}) : super(key: key);

  @override
  _MyProfileState createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  TextEditingController aboutController = TextEditingController();
  int myPreference = -1;
  // bool refresh = false;
  List<BaseModel> profilePhotos = userModel.profilePhotos;
  List<BaseModel> hookUpPhotos = userModel.hookUpPhotos;
  int defProfilePhoto = userModel.getInt(DEF_PROFILE_PHOTO);
  int defStrockPhoto = userModel.getInt(DEF_STROCK_PHOTO);

  @override
  initState() {
    super.initState();
    overlayController.add(false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: page(),
    );
  }

  String get formatBirthDate {
    if (userModel.birthDate.isEmpty) return "Add Birthday!";

    final date = DateTime.parse(userModel.birthDate);
    return new DateFormat("MMMM d").format(date);
  }

  double pixels = 0;
  page() {
    double blur = (pixels / 10);
    double imageHeight =
        (getScreenHeight(context) * 0.5) - (pixels > 0 ? 0 : (pixels));

    int pos = 0;
    bool isVideo = false;
    String imageUrl = '';

    if (userModel.profilePhotos.isNotEmpty) {
      pos = userModel.getInt(DEF_PROFILE_PHOTO) >
                  userModel.profilePhotos.length - 1 ||
              (userModel.getInt(DEF_PROFILE_PHOTO).isNegative)
          ? 0
          : userModel.getInt(DEF_PROFILE_PHOTO);
      imageUrl = userModel.profilePhotos[pos].imageUrl;
    }

    return scrollPageFix();
  }

  scrollPageFix() {
    double blur = (pixels / 10);
    double imageHeight =
        (getScreenHeight(context) * 0.5) - (pixels > 0 ? 0 : (pixels));

    int pos = 0;
    bool isVideo = false;
    String imageUrl = '';

    if (userModel.profilePhotos.isNotEmpty) {
      pos = userModel.getInt(DEF_PROFILE_PHOTO) >
                  userModel.profilePhotos.length - 1 ||
              (userModel.getInt(DEF_PROFILE_PHOTO).isNegative)
          ? 0
          : userModel.getInt(DEF_PROFILE_PHOTO);
      imageUrl = userModel.profilePhotos[pos].imageUrl;
    }

    return Stack(
      children: [
        ListView(
          key: const ValueKey('profileList'),
          padding: EdgeInsets.only(bottom: 200),
          // crossAxisAlignment: CrossAxisAlignment.start,
          // mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              height: imageHeight,
              width: double.infinity,
              child: Stack(
                children: [
                  placeHolder(imageHeight, width: double.infinity),
                  Image.network(
                    imageUrl,
                    fit: BoxFit.cover,
                    height: imageHeight,
                    width: double.infinity,
                  )
                ],
              ),
            ),
            Container(
                padding: EdgeInsets.fromLTRB(20, 30, 20, 20),
                child: Row(
                  children: [
                    Flexible(
                      fit: FlexFit.tight,
                      child: Text(
                        userModel.getString(NAME),
                        style: textStyle(false, 22, black),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    addSpaceWidth(5),
                    IconButton(
                      icon: Image.asset(
                        ic_edit_profile,
                        color: black,
                        height: 20,
                      ),
                      onPressed: () {
                        pushAndResult(
                            context,
                            inputDialog(
                              "Your Name",
                              message: userModel.getString(NAME),
                              hint: "What's your name?",
                            ), result: (_) {
                          if (null == _) return;
                          userModel
                            ..put(NAME, _)
                            ..updateItems();

                          setState(() {});
                        }, depend: false);
                      },
                    ),
                    addSpaceWidth(5),
                  ],
                )),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.only(left: 25),
                  child: GestureDetector(
                    onTap: () {
                      if (userModel.isPremium) return;
                      openSubscriptionPage(context);
                    },
                    child: Column(
                      children: [
                        Text(
                          "Your\nStatus",
                          textAlign: TextAlign.center,
                          style: textStyle(false, 14, black.withOpacity(.5)),
                        ),
                        addSpace(5),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              userModel.isPremium ? "PREMIUM" : "REGULAR",
                              style: textStyle(false, 18, black),
                            ),
                            addSpaceWidth(5),
                            Icon(
                              Icons.add_circle_outline,
                              size: 18,
                              color: blue0,
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(20, 30, 20, 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        "Profile Photos",
                        style: textStyle(true, 22, black),
                      ),
                      Spacer(),
                      IconButton(
                        onPressed: () async {
                          File mediaInfo = await ImagePickerWeb.getImage(
                              outputType: ImageType.file);
                          if (null == mediaInfo) return;
                          BaseModel model = BaseModel();
                          model.put(
                              OBJECT_ID, mediaInfo.name.hashCode.toString());
                          model.put(IMAGE_URL, '');
                          model.put(FILE_NAME, mediaInfo.name);
                          profilePhotos.add(model);
                          userModel
                            ..put(PROFILE_PHOTOS,
                                profilePhotos.map((e) => e.items).toList())
                            ..updateItems();
                          setState(() {});
                          showProgress(true, context,
                              msg: 'Uploading Photo...');

                          ImageUtils.uploadImageToFirebaseStoreFile(mediaInfo,
                              (res, error) {
                            showProgress(false, context);

                            if (error != null) {
                              profilePhotos.removeWhere((e) =>
                                  e.getObjectId() == model.getObjectId());
                              setState(() {});
                              print('error');
                              return;
                            }

                            int p = profilePhotos.indexWhere(
                                (e) => e.getObjectId() == model.getObjectId());
                            BaseModel bm = BaseModel();
                            bm.put(IMAGE_URL, res);
                            profilePhotos[p] = bm;
                            userModel
                              ..put(PROFILE_PHOTOS,
                                  profilePhotos.map((e) => e.items).toList())
                              ..updateItems();
                            setState(() {});
                          });
                        },
                        icon: Icon(Icons.add_a_photo),
                      )
                    ],
                  ),
                  photoBox(),
                  addSpace(20),

                  Row(
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            "Wow Factor!!!",
                            style: textStyle(true, 22, black),
                          ),
                          addSpaceWidth(5),
                          GestureDetector(
                              onTap: () {
                                showMessage(
                                    context,
                                    Icons.info,
                                    black,
                                    "Wow Factor!!!",
                                    "What will make you go on a spontaneous meet/date?");
                              },
                              child: Icon(Icons.info)),
                        ],
                      ),
                      Spacer(),
                      IconButton(
                        icon: Image.asset(
                          ic_edit_profile,
                          color: black,
                          height: 20,
                        ),
                        onPressed: () {
                          pushAndResult(
                              context,
                              inputDialog(
                                "Wow Factor!!!",
                                hint: "Write about your wow factor?",
                                message: userModel.getString(WOW_FACTOR),
                              ), result: (_) {
                            if (null == _) return;
                            userModel
                              ..put(WOW_FACTOR, _)
                              ..updateItems();

                            setState(() {});
                          }, depend: false);
                        },
                      ),
                    ],
                  ),
                  addSpace(15),
                  Container(
                      child: Text(
                          userModel.getString(WOW_FACTOR).isEmpty
                              ? "Write about your Wow Factor?"
                              : userModel.getString(WOW_FACTOR),
                          style: textStyle(false, 18, black.withOpacity(.6)))),
                  addLine(1, black.withOpacity(.1), 0, 10, 0, 10),

                  Row(
                    children: [
                      Text(
                        "About you",
                        style: textStyle(true, 22, black),
                      ),
                      Spacer(),
                      IconButton(
                        icon: Image.asset(
                          ic_edit_profile,
                          color: black,
                          height: 20,
                        ),
                        onPressed: () {
                          pushAndResult(
                              context,
                              inputDialog(
                                "About you",
                                hint: "Write a bit about yourself",
                                maxLength: 80,
                                message: userModel.getString(ABOUT_ME),
                              ), result: (_) {
                            if (null == _) return;
                            userModel
                              ..put(ABOUT_ME, _)
                              ..updateItems();

                            setState(() {});
                          }, depend: false);
                        },
                      ),
                    ],
                  ),
                  addSpace(15),
                  Container(
                      child: Text(
                          userModel.getString(ABOUT_ME).isEmpty
                              ? "Write a bit about yourself"
                              : userModel.getString(ABOUT_ME),
                          style: textStyle(false, 18, black.withOpacity(.6)))),
                  addLine(1, black.withOpacity(.1), 0, 10, 0, 10),
                  Row(
                    children: [
                      Text(
                        "Your Birthday?",
                        style: textStyle(true, 22, black),
                      ),
                      /*Spacer(),
                      IconButton(
                        icon: Image.asset(
                          ic_edit_profile,
                          color: black,
                          height: 20,
                        ),
                        onPressed: () {
                          pushAndResult(
                              context,
                              inputDialog(
                                "Wow Factor!!!",
                                hint: "Write about your wow factor?",
                                message:
                                    userModel.getString(WOW_FACTOR),
                              ), result: (_) {
                            if (null == _) return;
                            userModel
                              ..put(WOW_FACTOR, _)
                              ..updateItems();

                            setState(() {});
                          }, depend: false);
                        },
                      ),*/
                    ],
                  ),
                  addSpace(15),
                  Text(
                    formatBirthDate,
                    style: textStyle(false, 16, black.withOpacity(.7)),
                  ),
                  addSpace(15),
                  addLine(1, black.withOpacity(.1), 0, 10, 0, 10),
                  Text(
                    "Your Ethnicity preference?",
                    style: textStyle(true, 22, black),
                  ),
                  addSpace(15),
                  groupedButtons(
                      ethnicityType,
                      userModel.get(ETHNICITY) == null
                          ? ""
                          : ethnicityType[userModel.getInt(ETHNICITY)],
                      (text, p) {
                    userModel
                      ..put(ETHNICITY, p)
                      ..updateItems();
                    preferenceController.add(true);
                    setState(() {
                      //myPreference = p;
                    });
                  },
                      selectedColor: AppConfig.appColor,
                      normalColor: black.withOpacity(.6),
                      selectedTextColor: white,
                      normalTextColor: black),
                  addSpace(15),
                  addLine(1, black.withOpacity(.1), 0, 10, 0, 10),
                  //addSpace(15),
                  Text(
                    "What's your Gender preference?",
                    style: textStyle(true, 22, black),
                  ),
                  addSpace(15),
                  groupedButtons(
                      preferenceType,
                      userModel.get(PREFERENCE) == null
                          ? ""
                          : preferenceType[userModel.getInt(PREFERENCE)],
                      (text, p) {
                    userModel
                      ..put(PREFERENCE, p)
                      ..updateItems();
                    preferenceController.add(true);
                    setState(() {
                      //myPreference = p;
                    });
                  },
                      selectedColor: AppConfig.appColor,
                      normalColor: black.withOpacity(.6),
                      selectedTextColor: white,
                      normalTextColor: black),
                  addSpace(15),
                  addLine(1, black.withOpacity(.1), 0, 10, 0, 10),
                  Text(
                    "Your Relationship preference?",
                    style: textStyle(true, 22, black),
                  ),
                  addSpace(15),
                  groupedButtons(
                      relationshipType,
                      userModel.get(RELATIONSHIP) == null
                          ? ""
                          : relationshipType[userModel.getInt(RELATIONSHIP)],
                      (text, p) {
                    userModel
                      ..put(RELATIONSHIP, p)
                      ..updateItems();
                    preferenceController.add(true);

                    setState(() {
                      //myPreference = p;
                    });
                  },
                      selectedColor: AppConfig.appColor,
                      normalColor: black.withOpacity(.6),
                      selectedTextColor: white,
                      normalTextColor: black),
                  /* addSpace(15),
                  addLine(1, black.withOpacity(.1), 0, 10, 0, 10),
                  Text(
                    "Where do you live in?",
                    style: textStyle(true, 22, black),
                  ),
                  addSpace(15),
                  GestureDetector(
                    onTap: () {
                      pushAndResult(context, SearchPlace(),
                          result: (BaseModel res) {
                        String yourCity = res.getString(PLACE_NAME);
                        userModel
                          ..put(CITY, yourCity)
                          ..updateItems();
                        setState(() {});
                      }, depend: false);
                    },
                    child: Container(
                        height: 40,
                        width: double.infinity,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: Text(
                                userModel.city.isEmpty
                                    ? "Search for your city"
                                    : userModel.city,
                                style: textStyle(
                                    false,
                                    18,
                                    black.withOpacity(
                                        userModel.city.isEmpty ? 0.7 : 1)),
                              ),
                            ),
                            Icon(
                              Icons.search,
                              color: black.withOpacity(.7),
                            )
                          ],
                        )),
                  ),*/
                  addSpace(15),
//                            addLine(1, black.withOpacity(.1), 0, 10, 0, 10),
//                            Text(
//                              "Location",
//                              style: textStyle(true, 22, black),
//                            ),
//                            addSpace(15),
//                            Container(
//                                height: 40,
//                                width: double.infinity,
//                                child: Row(children: [
//                                  Flexible(
//                                      fit: FlexFit.tight,
//                                      child: Text(
//                                        userModel.getString(MY_LOCATION),
//                                        style: textStyle(false, 18, black),
//                                      )),
//                                  Text(userModel.getString(COUNTRY),
//                                      style: textStyle(
//                                          false, 14, black.withOpacity(.5)))
//                                ])),
//                            addSpace(15),
                  addLine(1, black.withOpacity(.1), 0, 10, 0, 10),
                  InkWell(
                    onTap: () {
                      pushAndResult(context, AppSettings(), depend: false);
                    },
                    child: Container(
                        height: 50,
                        width: double.infinity,
                        child: Row(children: [
                          Flexible(
                              fit: FlexFit.tight,
                              child: Text(
                                "General Settings",
                                style: textStyle(true, 18, black),
                              )),
                          Icon(
                            Icons.navigate_next,
                            size: 18,
                            color: black,
                          )
                        ])),
                  ),
//                            addLine(1, black.withOpacity(.1), 0, 0, 0, 10),
//                            InkWell(
//                              onTap: () {
//                                pushAndResult(context, AdsPage());
//                              },
//                              child: Container(
//                                  height: 50,
//                                  width: double.infinity,
//                                  child: Row(children: [
//                                    Flexible(
//                                        fit: FlexFit.tight,
//                                        child: Text(
//                                          "Advertize With Us",
//                                          style: textStyle(true, 18, black),
//                                        )),
//                                    Icon(
//                                      Icons.navigate_next,
//                                      size: 18,
//                                      color: black,
//                                    )
//                                  ])),
//                            ),
                  addLine(1, black.withOpacity(.1), 0, 10, 0, 0),
//                            if (userModel.getInt(ACCOUNT_TYPE) == 1)
//                              InkWell(
//                                onTap: () {
//                                  showMessage(
//                                      context,
//                                      Icons.error,
//                                      red0,
//                                      "Cancel?",
//                                      "Are you sure you want to cancel your Subscription? This would revert you back to a regular user.",
//                                      textSize: 14,
//                                      clickYesText: "UnSubscribe",
//                                      onClicked: (_) {
//                                    if (_) {
//                                      userModel
//                                        ..put(ACCOUNT_TYPE, 0)
//                                        ..updateItems();
//                                      setState(() {});
//                                    }
//                                  }, clickNoText: "Cancel");
//                                },
//                                child: Container(
//                                    //height: 50,
//                                    color: red,
//                                    padding: EdgeInsets.all(10),
//                                    width: double.infinity,
//                                    child: Row(children: [
//                                      Flexible(
//                                          fit: FlexFit.tight,
//                                          child: Text(
//                                            "Cancel Subscription",
//                                            style: textStyle(true, 16, white),
//                                          )),
//                                      Icon(
//                                        Icons.navigate_next,
//                                        size: 18,
//                                        color: white,
//                                      )
//                                    ])),
//                              ),
//                            addLine(1, black.withOpacity(.1), 0, 0, 0, 10),

                  addLine(1, black.withOpacity(.1), 0, 0, 0, 10),
                ],
              ),
            ),
            if (isAdmin) ...[
              InkWell(
                onTap: () {
                  pushAndResult(context, AppAdmin(), depend: false);
                },
                child: Container(
                    //height: 50,
                    color: red,
                    padding: EdgeInsets.all(15),
                    width: double.infinity,
                    child: Row(children: [
                      Flexible(
                          fit: FlexFit.tight,
                          child: Text(
                            "Admin Portal",
                            style: textStyle(true, 18, white),
                          )),
                      Icon(
                        Icons.navigate_next,
                        size: 18,
                        color: white,
                      )
                    ])),
              ),
            ],
//              addSpace(150),
          ],
        ),
        if (widget.showBar)
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: RaisedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                elevation: 10,
                shape: CircleBorder(),
                child: Icon(
                  Icons.cancel,
                  size: 30,
                  color: red,
                ),
              ),
            ),
          ),
      ],
    );
  }

  photoBox() {
    String key = DEF_PROFILE_PHOTO;

    if (profilePhotos.isEmpty) return Container();

    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      key: const ValueKey('profilePhotoList'),
      child: Row(
        children: List.generate(profilePhotos.length, (p) {
          BaseModel photo = profilePhotos[p];
          bool isVideo = photo.isVideo;
          String imageUrl =
              photo.getString(isVideo ? THUMBNAIL_URL : IMAGE_URL);
          bool isOnline = imageUrl.startsWith('http');
          bool isDef = defProfilePhoto == p;
          bool isPublic = photo.getInt(VISIBILITY) == 0;

          return GestureDetector(
            onLongPress: () {
              showMessage(context, Icons.error, red0, "Default?",
                  "Make the photo default?", textSize: 14, clickYesText: "Yes",
                  onClicked: (_) {
                if (_) {
                  defProfilePhoto = p;
                  userModel
                    ..put(DEF_PROFILE_PHOTO, defProfilePhoto)
                    ..updateItems();
                  homeRefreshController.add(true);
                  setState(() {});
                }
              }, clickNoText: "Cancel");
            },
            child: Container(
              height: 200,
              width: 160,
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Opacity(
                    opacity: isPublic ? 1 : 0.15,
                    child: Container(
                      margin: EdgeInsets.all(8),
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Stack(
                            children: [
                              placeHolder(200, width: 160),
                              if (isOnline)
                                Image.network(
                                  imageUrl,
                                  height: 200,
                                  width: 160,
                                  fit: BoxFit.cover,
                                )
                              // else
                              //   Image.memory(
                              //     photo.get(IMAGE_URL),
                              //     height: 200,
                              //     width: 160,
                              //     fit: BoxFit.cover,
                              //   )
                            ],
                          )),
                    ),
                  ),
                  if (isVideo)
                    Center(
                      child: Container(
                        height: 50,
                        width: 50,
                        child: Icon(
                          Icons.play_arrow,
                          color: Colors.white,
                        ),
                        decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.8),
                            border: Border.all(color: Colors.white, width: 1.5),
                            shape: BoxShape.circle),
                      ),
                    ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      margin: EdgeInsets.all(4),
                      width: 30,
                      height: 30,
                      child: new RaisedButton(
                          padding: EdgeInsets.all(0),
                          elevation: 2,
                          shape: CircleBorder(),
                          color: red0,
                          child: Icon(
                            Icons.close,
                            color: white,
                            size: 13,
                          ),
                          onPressed: () {
                            if (defProfilePhoto == p) {
                              showMessage(
                                context,
                                Icons.error,
                                red0,
                                "Opps Sorry!",
                                "Sorry you cant' delete photo set as default until you set another as default",
                                textSize: 14,
                              );
                              return;
                            }
                            profilePhotos.removeAt(p);
                            userModel
                              ..put(PROFILE_PHOTOS,
                                  profilePhotos.map((e) => e.items).toList())
                              ..updateItems();
                            setState(() {});
                          }),
                    ),
                  ),
                  if (defProfilePhoto == p)
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        padding: const EdgeInsets.all(4.0),
                        decoration: BoxDecoration(
                            border: Border.all(color: white, width: 2),
                            color: appColor,
                            borderRadius: BorderRadius.circular(10)),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Icon(
                              Icons.check,
                              size: 12,
                            ),
                            addSpaceWidth(3),
                            Text("Default"),
                          ],
                        ),
                      ),
                    ),
                ],
              ),
            ),
          );
        }),
      ),
    );
  }

  void uploadPhotos(BaseModel model, String s, String id) {
    uploadFileString(model.getString(IMAGE_URL), (res, err) {
      if (null != err) return;
      final photos = userModel.profilePhotos;
      int pos = photos.indexWhere((e) => e.getObjectId() == id);
      if (pos != -1) {
        model.put(IMAGE_URL, res);
        photos[pos] = model;
        userModel
          ..put(PROFILE_PHOTOS, photos.map((e) => e.items).toList())
          ..updateItems();
      }

      if (mounted) setState(() {});
    });
  }
}
