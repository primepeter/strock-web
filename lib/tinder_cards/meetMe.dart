import 'dart:async';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainWebHome.dart';
import 'package:Strokes/PaymentSubPage.dart';
import 'package:Strokes/WebPusher.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:Strokes/main_pages/ShowMatches.dart';
import 'package:Strokes/main_pages/ShowProfileLikes.dart';
import 'package:Strokes/main_pages/ShowProfileSuperLikes.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'card_stack.dart';
import 'matches.dart';
import 'profiles.dart';
import 'round_icon_button.dart';

class MeetMe extends StatefulWidget {
  final bool webMode;

  const MeetMe({Key key, this.webMode = false}) : super(key: key);
  @override
  _MeetMeState createState() => _MeetMeState();
}

class _MeetMeState extends State<MeetMe>
    with AutomaticKeepAliveClientMixin<MeetMe> {
  List<Profile> demoProfiles;
  Profile previousProfile;
  bool setup = false;
  List otherPeople = [];
  List<MatchEngine> matchEngines = [];
  bool showOverlay = true;
  List<StreamSubscription> subs = List();
  List<String> displayIds = [];
  bool handling = false;

  @override
  initState() {
    super.initState();
    var overlaySub = overlayController.stream.listen((b) {
      setState(() {
        showOverlay = b;
      });
    });
    subs.add(overlaySub);
    //loadOtherPeople();
    otherPeople = meetMeList;
    if (meetMeList.isNotEmpty) loadMatches(false);
  }

  loadMatches(bool isNew) {
    otherPeople.shuffle();

    /*for (var model in otherPeople) {
      int p = demoProfiles
          .indexWhere((e) => e.objectId ?? "" == model.getObjectId());
      final profile = Profile(
          objectId: model.getObjectId(),
          age: getAge(DateTime.parse(model.getString(BIRTH_DATE))),
          name: model.getString(NAME),
          user: model,
          isAds: false,
          photos: List<String>.from(
              model.profilePhotos.map((e) => e.imageUrl).toList()),
          bio: model.getString(ABOUT_ME),
          location: model.city,
          distance: "${(model.getDouble(DISTANCE)).roundToDouble()} KM");

      if (p == -1)
        demoProfiles.add(profile);
      else
        demoProfiles[p] = profile;
    }
*/
    /*   for (var pf in demoProfiles){
      int p = matchEngines.indexWhere((e) => e.currentMatch.profile.user.getObjectId()==pf.user.getObjectId());
if(p==-1)matchEngines.add(value)
    }*/

    demoProfiles = otherPeople
        .map((e) => Profile(
            objectId: e.getObjectId(),
            age: getAge(DateTime.parse(e.getString(BIRTH_DATE))),
            name: e.getString(NAME),
            user: e,
            isAds: false,
            photos: List<String>.from(
                e.profilePhotos.map((e) => e.imageUrl).toList()),
            bio: e.getString(ABOUT_ME),
            location: e.city,
            distance: "${(e.getDouble(DISTANCE)).roundToDouble()} KM"))
        .toList();

    MatchEngine matchEngine = new MatchEngine(
      matches: demoProfiles.map((Profile profile) {
        return new DateMatch(profile: profile);
      }).toList(),
    );

    matchEngines.add(matchEngine);
    setup = matchEngines.isNotEmpty;
    setState(() {});
  }

  @override
  void dispose() {
    for (var s in subs) s?.cancel();
    super.dispose();
  }

  updateHandling(bool handled) {
    setState(() {
      handling = handled;
    });
  }

  loadOtherPeople() async {
    double limit = appSettingsModel.getDouble(MILES_LIMIT);
    bool useLimit = appSettingsModel.getBoolean(USE_RADIUS_LIMIT);
    QuerySnapshot shots = await Firestore.instance
        .collection(USER_BASE)
        .where(GENDER, isEqualTo: userModel.getInt(PREFERENCE))
        .getDocuments();

    for (DocumentSnapshot doc in shots.documents) {
      BaseModel model = BaseModel(doc: doc);
      if (model.myItem()) continue;
      if (!model.signUpCompleted) continue;
      if (model.getList(MATCHED_LIST).contains(userModel.getUserId())) continue;

      if (model.getMap(POSITION).isEmpty) continue;
      final geoPoint = model.getModel(POSITION).get("geopoint") as GeoPoint;
      // final distance = (await calculateDistanceTo(geoPoint) / 1000);
      // if ((distance > limit)) continue;
      // print("Distance $distance limit $limit");
      // model.put(DISTANCE, distance);

      int index = otherPeople
          .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
      if (index == -1) {
        otherPeople.add(model);
      } else {
        otherPeople.add(model);
      }
    }

    otherPeople.shuffle();
    List<BaseModel> allItems = [];
    allItems.addAll(List.from(otherPeople));
    //allItems.addAll(List.from(hookupList));
    demoProfiles = allItems
        .map((e) => Profile(
            objectId: e.getObjectId(),
            age: getAge(DateTime.parse(e.getString(BIRTH_DATE))),
            name: e.getString(NAME),
            user: e,
            isAds: false,
            photos: List<String>.from(
                e.profilePhotos.map((e) => e.imageUrl).toList()),
            bio: e.getString(ABOUT_ME),
            location: e.city,
            distance: "${(e.getDouble(DISTANCE)).roundToDouble()} KM"))
        .toList();

    MatchEngine matchEngine = new MatchEngine(
      matches: demoProfiles.map((Profile profile) {
        return new DateMatch(profile: profile);
      }).toList(),
    );

    matchEngines.add(matchEngine);
    setup = true;
    if (mounted) setState(() {});

    //injectAds();
  }

  addToDisplay(String id, bool add) {
    if (add) {
      int p = displayIds.indexWhere((e) => e == id);
      if (p != -1)
        displayIds[p] = id;
      else
        displayIds.add(id);
    } else {
      displayIds.removeWhere((e) => e == id);
    }
    setState(() {});
  }

  MatchEngine getMatchEngine() {
    logToScreen();
    return matchEngines[matchEngines.length - 1];
  }

  logToScreen() {
    int idSize = displayIds.length;
    int eSize = matchEngines.length;
  }

  injectAds() {
    int myPlan = userModel.getInt(ACCOUNT_TYPE);
    String key = myPlan == 0 ? FEATURES_REGULAR : FEATURES_PREMIUM;
    BaseModel package = appSettingsModel.getModel(key);
    int adsSpacing = package.getInt(ADS_SPACING);
    adsList.shuffle();

    List<Profile> newList = [];
    for (int p = 0; p < demoProfiles.length; p++) {
      //if (!adsSetup) continue;
      //if (adsList.isEmpty) continue;
      if (p % adsSpacing != 0) {
        final ads = getAdsAt(p, adsSpacing);
        if (null == ads) continue;
        Profile adsProfile = Profile(
            objectId: ads.getObjectId(),
            isAds: true,
            photos: [ads.getString(ADS_IMAGE)],
            name: ads.getString(TITLE),
            urlLink: ads.getString(ADS_URL),
            user: ads);
        newList.add(adsProfile);
      }
      newList.add(demoProfiles[p]);
    }
    demoProfiles.clear();
    demoProfiles.addAll(newList);

    MatchEngine matchEngine = new MatchEngine(
      matches: demoProfiles.map((Profile profile) {
        return new DateMatch(profile: profile);
      }).toList(),
    );
    matchEngines[matchEngines.length - 1] = matchEngine;
    if (mounted) setState(() {});
  }

  BaseModel getAdsAt(int p, int adsSpacing) {
    if (adsList.isEmpty) return null;
    int index = p ~/ adsSpacing;
    index = index - 1;

    if (index < 0) return null;
    if (index > adsList.length - 1) {
      BaseModel model = findAds(true);
      if (model == null) model = findAds(false);
      if (model == null) return null;
    }

    if (index > adsList.length - 1) return null;

    BaseModel ad = adsList[index];
    if (ad.getInt(STATUS) != APPROVED) return null;
    List<String> myHiddenPosts = List.from(userModel.getList(HIDDEN));
    if (myHiddenPosts.contains(ad.getObjectId())) return null;
    adsList.shuffle();
    return ad;
  }

  final List loadedAds = [];

  BaseModel findAds(bool skipShown) {
    BaseModel model;
    for (BaseModel bm in adsList) {
      if (loadedAds.contains(bm.getObjectId())) continue;
      if (skipShown && bm.getList(SEEN_BY).contains(userModel.getUserId()))
        continue;
      //partnersList.add(bm);
      loadedAds.add(bm.getObjectId());
      model = bm;
      break;
    }
    adsList.shuffle();
    return model;
  }

  previousMatch() async {
    if (!userModel.isPremium) {
      overlayController.add(false);
      showMessage(context, Icons.error, red0, "Opps Sorry!",
          "You cannot rewind to previous prospect until you become a Premium User",
          textSize: 14,
          //cancellable: false,
          clickYesText: "Subscribe", onClicked: (_) {
        if (_) {
          // openSubscriptionPage(context);
          pushAndResult(context, WebPusher(child: PaymentSubPage()),
              depend: false);
        } else
          overlayController.add(true);
      }, clickNoText: "Close");
      return;
    }

    if (null == previousProfile) return;
    demoProfiles.removeWhere((e) => e.objectId == previousProfile.objectId);
//    demoProfiles.insert(0,getMatchEngine().currentMatch.profile);
    demoProfiles.insert(0, previousProfile);
    addToDisplay(previousProfile.objectId, false);
//    getMatchEngine().dispose();
    setup = false;
    setState(() {});

    Future.delayed(Duration(milliseconds: 500), () {
      previousProfile = null;
      MatchEngine matchEngine = new MatchEngine(
        matches: demoProfiles.map((Profile profile) {
          return new DateMatch(profile: profile);
        }).toList(),
      );
      matchEngines.add(matchEngine);
      setup = true;
      setState(() {});
    });
  }

  Widget _buildBottomBar() {
    return BottomAppBar(
      color: Colors.transparent,
      elevation: 0.0,
      child: new Padding(
        padding: const EdgeInsets.all(16.0),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            RoundIconButton.small(
              icon: Icons.settings_backup_restore,
              iconColor: Colors.orange,
              onPressed: () {
                if (!setup || otherPeople.isEmpty) return;
                if (previousProfile == null) return;

                previousMatch();
                setState(() {});
                return;
              },
            ),
            new RoundIconButton.large(
              icon: Icons.clear,
              iconColor: Colors.red,
              size: 65,
              onPressed: () {
                if (!setup || otherPeople.isEmpty) return;
                setState(() {
                  previousProfile = getMatchEngine().currentMatch.profile;
                  addToDisplay(previousProfile.objectId, true);
                  if (previousProfile.isAds) previousProfile = null;
                });
                getMatchEngine().currentMatch.nope();
              },
            ),
            new RoundIconButton.large(
              icon: Icons.favorite,
              iconColor: Colors.green,
              size: 65,
              onPressed: () {
                if (!setup || otherPeople.isEmpty) return;
                setState(() {
                  previousProfile = null;
                });
                final profile = getMatchEngine().currentMatch.profile;
                pushNotificationToUser(profile.user, NOTIFY_LIKED);
                handleMatch(profile.user, isAds: profile.isAds, onHandled: () {
                  addToDisplay(profile.objectId, true);
                  getMatchEngine().currentMatch.like();
                });
                return;
              },
            ),
            new RoundIconButton.small(
              icon: Icons.star,
              iconColor: Colors.blue,
              onPressed: () {
                if (!setup || otherPeople.isEmpty) return;
                setState(() {
                  previousProfile = null;
                });

                final profile = getMatchEngine().currentMatch.profile;

                pushNotificationToUser(
                  profile.user,
                  NOTIFY_SUPER_LIKED,
                );
                addToDisplay(profile.objectId, true);
                if (profile.isAds) handleAds(profile.user, 2);
                getMatchEngine().currentMatch.superLike();
                return;
              },
            ),
          ],
        ),
      ),
    );
  }

  controlButton({
    @required IconData icon,
    @required Color color,
    @required int count,
    @required onTap,
  }) {
    return FlatButton(
      onPressed: onTap,
      minWidth: 50,
      height: 30,
      child: Container(
        height: 30,
        width: 50,
        color: transparent,
        child: Stack(
          children: [
            new Container(
                padding: EdgeInsets.all(5),
                child: Center(
                    child: Icon(
                  icon,
                  size: 30,
                  color: color,
                ))),
            if (count > 0)
              Container(
                  decoration: BoxDecoration(
                      color: AppConfig.appColor,
                      shape: BoxShape.circle,
                      border: Border.all(color: white, width: 2)),
                  padding: EdgeInsets.all(6),
                  child: Text(
                    count.toString(),
                    style: textStyle(false, 12, black),
                  ))
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return WillPopScope(
      onWillPop: () async {
        overlayController.add(false);
        Future.delayed(Duration(milliseconds: 20), () {
          Navigator.of(context).pop();
        });
        return false;
      },
      child: Scaffold(
        body: Column(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(0, 5, 0, 10),
              color: white,
              child: Row(
                children: <Widget>[
                  if (!widget.webMode)
                    IconButton(
                        onPressed: () {
                          overlayController.add(false);
                          Future.delayed(Duration(milliseconds: 20), () {
                            Navigator.of(context).pop();
                          });
                        },
                        icon: Icon(
                          Icons.keyboard_backspace,
                          color: black,
                          size: 20,
                        )),
                  Center(
                      child: Text(
                    "Meet Me",
                    //style: textStyle(true, 20, black)
                    style: textStyle(true, 25, black),
                  )),
                  addSpaceWidth(10),
                  Spacer(),
                  Row(
                    children: [
                      controlButton(
                          icon: Icons.thumb_up,
                          color: green,
                          count: likesList.length,
                          onTap: () {
                            if (isAdmin || userModel.isPremium) {
                              pushAndResult(
                                  context,
                                  ShowProfileLikes(
                                    webMode: widget.webMode,
                                  ),
                                  depend: false);
                              return;
                            }
                            overlayController.add(false);
                            showMessage(
                                context,
                                Icons.error,
                                red0,
                                "Opps Sorry!",
                                "You cannot view persons who liked your profile until you become a Premium User",
                                textSize: 14,
                                cancellable: false,
                                clickYesText: "Subscribe", onClicked: (_) {
                              if (_) {
                                // openSubscriptionPage(context);
                                pushAndResult(
                                    context, WebPusher(child: PaymentSubPage()),
                                    depend: false);
                              } else
                                overlayController.add(true);
                            }, clickNoText: "Cancel");
                          }),
                      controlButton(
                          icon: Icons.star,
                          color: Colors.blue,
                          count: superLikesList.length,
                          onTap: () {
                            if (isAdmin || userModel.isPremium) {
                              pushAndResult(
                                  context,
                                  ShowProfileSuperLikes(
                                    webMode: widget.webMode,
                                  ),
                                  depend: false);
                              return;
                            }
                            overlayController.add(false);
                            showMessage(
                                context,
                                Icons.error,
                                red0,
                                "Opps Sorry!",
                                "You cannot view persons who Super liked your profile until you become a Premium User",
                                textSize: 14,
                                cancellable: false,
                                clickYesText: "Subscribe", onClicked: (_) {
                              if (_) {
                                // openSubscriptionPage(context);
                                pushAndResult(
                                    context, WebPusher(child: PaymentSubPage()),
                                    depend: false);
                              } else
                                overlayController.add(true);
                            }, clickNoText: "Cancel");
                          }),
                      controlButton(
                          icon: Icons.favorite,
                          color: red,
                          count: matchesList.length,
                          onTap: () {
                            pushAndResult(
                                context,
                                ShowMatches(
                                  fromHome: true,
                                  webMode: widget.webMode,
                                ),
                                depend: false);
                          })
                    ],
                  ),
                ],
              ),
            ),
            Flexible(
                child: !setup
                    ? loadingLayout()
                    : Builder(builder: (ctx) {
                        //if (!strockSetup) return loadingLayout();
                        if (otherPeople.isEmpty)
                          return Center(
                            child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Image.asset(
                                    "assets/icons/gender.png",
                                    width: 50,
                                    height: 50,
                                    color: AppConfig.appColor,
                                  ),
//                                  Icon(
//                                    Icons.location_on,
//                                    color: AppConfig.appColor,
//                                  ),
                                  Text(
                                    "No Views Yet",
                                    style: textStyle(true, 20, black),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                            ),
                          );

                        final matchEngine = getMatchEngine();
                        bool allLoaded =
                            demoProfiles.length == displayIds.length;

                        if (allLoaded &&
                            displayIds.contains(
                                matchEngine.currentMatch.profile.objectId))
                          return Center(
                            child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Image.asset(
                                    "assets/icons/gender.png",
                                    width: 50,
                                    height: 50,
                                    color: white,
                                  ),
                                  Text(
                                    "Out of Matches",
                                    style: textStyle(true, 20, black),
                                    textAlign: TextAlign.center,
                                  ),
                                  Text(
                                    "Check back later!",
                                    style: textStyle(
                                        true, 14, black.withOpacity(.6)),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                            ),
                          );

//                        return Tind.TinderSwapCard(
//                          title: "",
//                          demoProfiles: demoProfiles,
//                          myCallback: (d) {},
//                        );

                        return new CardStack(
                          matchEngine: matchEngine,
                          showOverlay: showOverlay,
                          callback: (currentMatch, direction) {
                            final match = currentMatch.profile;
                            bool isAds = match.isAds;
                            BaseModel user = match.user;
                            String id = match.objectId;

                            if (isAds) handleAds(user, direction);

                            if (direction != 0) {
                              previousProfile = null;
                              //loadMaxChecker(direction);
                              if (direction == 1) {
                                handleMatch(user, isAds: isAds);
                              }
                              String key =
                                  direction == 1 ? LIKE_LIST : SUPER_LIKE_LIST;
                              List list = userModel.getList(key);
                              /*if (!list.contains(id)) {
                                list.add(id);
                                userModel.put(key, list);
                                user..put(key, userModel.getObjectId())..updateItems();
                              }*/
                              userModel
                                ..put(key, user.getObjectId())
                                ..updateItems();
                              // user
                              //   ..put(key, userModel.getObjectId())
                              //   ..updateItems();

                              pushNotificationToUser(
                                user,
                                direction == 1
                                    ? NOTIFY_LIKED
                                    : NOTIFY_SUPER_LIKED,
                              );
                            } else {
                              previousProfile = match;
                            }
                            List viewedList = userModel.getList(SEEN_BY);
                            if (!viewedList.contains(id)) {
                              viewedList.add(id);
                              userModel.put(SEEN_BY, viewedList);
                            }
                            userModel.updateItems();
                            setState(() {});
                          },
                        );
                      })),
          ],
        ),
        bottomNavigationBar: _buildBottomBar(),
      ),
    );
  }

  handleAds(BaseModel user, int p) {
    String key = p == 0
        ? SEEN_BY
        : p == 1
            ? LIKE_LIST
            : SUPER_LIKE_LIST;
    user
      ..putInList(key, userModel.getObjectId(), true)
      ..updateItems();
  }

  handleMatch(BaseModel user, {bool isAds = false, onHandled}) async {
    if (isAds) {
      handleAds(user, 1);
      if (null != onHandled) onHandled();
      return;
    }

    final doc = await FirebaseFirestore.instance
        .collection(USER_BASE)
        .doc(user.getObjectId())
        .get();
    if (doc == null || !doc.exists) {
      if (null != onHandled) onHandled();
      return;
    }
    user = BaseModel(doc: doc);
    final likes = user.getList(LIKE_LIST);
    userModel
      ..putInList(LIKE_LIST, user.getObjectId(), true)
      ..putInList(SEEN_BY, user.getObjectId(), true)
      ..updateItems();
    onHandled();
    if (likes.contains(userModel.getUserId())) {
      userModel
        ..putInList(MATCHED_LIST, user.getObjectId())
        ..updateItems();
      user
        ..putInList(MATCHED_LIST, userModel.getObjectId())
        ..updateItems();
      matchedController.add(user);
      return;
    }
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => false;
}
