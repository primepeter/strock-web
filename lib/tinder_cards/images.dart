const String myImage = "https://goo.gl/X36nXi";
const String app_icon = "assets/tinder/app_icon.png";
const String ava6 = "assets/tinder/ava6.png";

const String img1 = "assets/tinder/img1.jpg";
const String img2 = "assets/tinder/img2.jpg";
const String img3 = "assets/tinder/img3.jpg";
const String img4 = "assets/tinder/img4.jpg";
const String hookUp = "assets/tinder/hookup.png";
const String swipe = "assets/tinder/swipe.png";
const String match = "assets/tinder/match.png";
const String avatar = "assets/tinder/avatar.jpg";
const String chat = "assets/tinder/chat2.png";
const String ic_chat = "assets/tinder/ic_chat.png";
const String hookup_icon = "assets/tinder/hookup_icon.png";
const String hookup_icon_small = "assets/tinder/hookup_icon_small.png";
const String lightening = "assets/tinder/lightening.png";
